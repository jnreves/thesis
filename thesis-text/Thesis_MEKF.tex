\subsection{Extended Kalman Filter applied to Spacecraft Attitude Estimation}
%This thesis' objective is to develop an attitude estimation, so naturally the EKF concepts must be applied to this problem. 
%\vspace{0.4cm}
%\par \textbf{Attitude representation for Kalman filtering}
\par In Chapter 2, multiple attitude representations were presented. Although all are useful in some ways throughout this work, some are more suitable for filtering applications than others.
The Rotation Matrix is composed by 
9 parameters and subject to 6 constraints, meaning that it can be described by three degrees of freedom locally, as seen in \ref{chapter:background}.
With this in mind, a lower parameterization is more useful for filtering. 
\par Since all three-component
representations of the rotation group have discontinuities or singularities, a filter must provide a guarantee to avoid them.
Although early Kalman Filters used Euler angles, which are intuitive for small angular displacements,
they are not usually used due to requiring a fair number of trigonometric function evaluations,
which increases computing power demands and the presence of singularities.
The rotation vector $\boldsymbol{\vartheta}$ has singularities for $360^{\circ}$ rotations. This is not useful for a global attitude representation due to the freedom of heading of the satellite.
\par The quaternion becomes the representation of choice since it is the lowest-dimensional 
parameterization that is free from singularities. On the other hand, 
the quaternion creates the challenge of maintaining the normalization constraint when, for example, the
update equation is considered.
To tackle this problem, distinct solutions are proposed.
\vspace{0.4cm}
\par \textbf{Additive quaternion representation}
\par The Additive Kalman Filter handles each of the quaternion components as independent
parameters and spoils the normalization. The quaternion estimate is given by
\begin{equation}
    \hat{ \mathbf{q}} = E \{  \mathbf{q} \}
\end{equation}
and the error of estimation is given by
\begin{equation}
    \Delta  \mathbf{q} =  \mathbf{q}  - \hat{\mathbf{q}} 
\end{equation}
Assuming that the true quaternion $\mathbf{q}$ has unit norm,  this leads to 
\begin{equation}
    E \{ \vert \vert \hat{\mathbf{q}} \vert \vert \} =  E \{ \vert \vert \mathbf{q} \vert \vert \} + E \{ \vert \vert \Delta \mathbf{q} \vert \vert \} = 1 + E \{ \vert \vert \Delta \mathbf{q} \vert \vert \}
\end{equation}
must violate the unit norm constraint for $\hat{\mathbf{q}}$.
\par Several methods have been proposed to deal with this problem. One method
uses brute force to normalize the estimate but is only a second-order correction to
the quaternion error. Other method gives up enforcing the quaternion norm and defines 
an attitude matrix whilst introducing one unobservable degree of freedom, but has 
been used successfully \cite{psiakiAttitudeEstimationFlexible2002}.
\vspace{0.4cm}
\par \textbf{Multiplicative quaternion representation}
\par The basic idea of the multiplicative EKF (MEKF) is to use the quaternion as the "global" attitude representation and use a three-component state vector 
$\delta  \boldsymbol{\vartheta}$ for the "local" representation of attitude errors. The true quaternion can be obtained based on the error quaternion and the estimated quaternion
such as
\begin{equation}
    \mathbf{q} = \delta \mathbf{q} (\delta  \boldsymbol{\vartheta}) \otimes \hat{\mathbf{q}}
    \label{eq:mekf-q-true}
\end{equation}
The representations $\mathbf{q}$, $\delta \mathbf{q}$ and $\hat{\mathbf{q}}$ are properly normalized quaternions.
The attitude error $\delta \mathbf{q}$ is defined in the body reference frame. The three-component attitude error vector $\boldsymbol{\delta \vartheta}$ follows the definition in \ref{eq:bck-delta-v}. The MEKF error state 
is then defined as 
\begin{equation}
    \Delta \mathbf{x} = \begin{bmatrix}
        \delta  \boldsymbol{\vartheta} \\
        \Delta \boldsymbol{\xi}
        \end{bmatrix}
    \label{eq:mekf-state}
\end{equation}
where $ \boldsymbol{\xi} $ is a vector of other variables to be estimated and follows $ \boldsymbol{\xi} = \hat{ \boldsymbol{\xi} } + \Delta \boldsymbol{\xi} $. Thus, the conventions of the EKF can be applied 
to compute an estimate of $ \Delta \mathbf{x}$. With this structure, the quaternion estimate $ \hat{\mathbf{q}}$ is not part of the EKF and a reset operation transports the
uncertainty to the global variable, maintaining a small error in $\delta \boldsymbol{\vartheta}$, thus avoiding singularities. 
\par In comparison with the Additive Kalman Filter, the main advantage is faster computation due to having a smaller local variable, resulting in smaller covariance matrices.
On the other hand, the dimensional reduction more accurately depicts the actual degrees of freedom of the system \cite{markleyAttitudeErrorRepresentations2003} \cite{markleyAttitudeEstimationQuaternion2004}, as well as has a 
transparent physical interpretation for the error covariance.
%\cite{[5, 23, 24, 31, 36, 37]} from Fundamentals. chose 23 and 24

\vspace{0.4cm}
\par \textbf{Attitude Kalman filter formulation}
\par The MEKF follows the same structure of \ref{alg:ekf}, with the most significant steps being initialization, measurement update, state vector reset and propagation to
the next iteration, where the latter three are iterated. 
\par The propagation step works in the global attitude representation, the quaternion, to propagate the state estimate and the covariance of the state. 
By differentiating the expression in \eqref{eq:mekf-q-true}, we obtain
\begin{equation}
   \dot{\mathbf{q}} = \delta \dot{\mathbf{q}} \otimes \hat{\mathbf{q}}  +  (\delta  \boldsymbol{\vartheta}) \otimes \dot{\hat{\mathbf{q}}}
    \label{eq:mekf-q-true-dot}
\end{equation}
The true and the estimate quaternion have the following kinematic equations
\begin{equation}
    \dot{\mathbf{q}} = \frac{1}{2} \begin{bmatrix}
        \boldsymbol{\omega} \\
       0
       \end{bmatrix} \otimes \mathbf{q}
     \label{eq:mekf-q-true-kinematics}
 \end{equation}
 \begin{equation}
    \dot{\hat{\mathbf{q}}} = \frac{1}{2} \begin{bmatrix}
        \hat{\boldsymbol{\omega}} \\
       0
       \end{bmatrix} \otimes \hat{\mathbf{q}}
     \label{eq:mekf-q-hat-kinematics}
 \end{equation}

where $\boldsymbol{\omega}$ and $\hat{\boldsymbol{\omega}}$ are the true and estimated angular velocities, respectively, and $\boldsymbol{\omega} = \hat{\boldsymbol{\omega}} + \delta \boldsymbol{\omega} $ 
Substituting \eqref{eq:mekf-q-true}, \eqref{eq:mekf-q-true-kinematics} and \eqref{eq:mekf-q-hat-kinematics} into \eqref{eq:mekf-q-true-dot} gives
\begin{equation}
    \delta \dot{\mathbf{q}} = \frac{1}{2} \left(  \begin{bmatrix}
        \boldsymbol{\omega} \\
       0
       \end{bmatrix}
 \otimes \delta \mathbf{q}
-\delta \mathbf{q} \otimes \begin{bmatrix}
        \hat{\boldsymbol{\omega}} \\
       0
       \end{bmatrix}
 \right)
\label{eq:mekf-dot-delta-q-1}
\end{equation}
Assuming that $ \delta \boldsymbol{\omega}$ and $ \delta \mathbf{q} - I_q$ are small, it leads to 
\begin{equation}
\delta \dot{\mathbf{q}} = - \left(  \begin{bmatrix}
    \hat{\boldsymbol{\omega}} \times \delta \mathbf{q}_{1:3}  \\
   0
   \end{bmatrix}
+ \frac{1}{2}
\begin{bmatrix}
    \delta {\boldsymbol{\omega}} \\
   0
   \end{bmatrix}
\right)
\label{eq:mekf-dot-delta-q-2}
\end{equation}

An alternative representation for $ \delta \mathbf{q} (\boldsymbol{ \delta  \vartheta})$ is
\begin{equation}
    \delta \mathbf{q} \simeq 
    \begin{bmatrix}
        \delta  \boldsymbol{\vartheta}/2 \\
       1
       \end{bmatrix} =
       I_q + \frac{1}{2}
       \begin{bmatrix}
        \delta  \boldsymbol{\vartheta} \\
          0
          \end{bmatrix}
\label{eq:mekf-delta-q-alt}
\end{equation}
By using \eqref{eq:mekf-delta-q-alt} with \eqref{eq:mekf-dot-delta-q-2} we obtain the value for the first three components, such as
\begin{equation}
\delta \dot{\boldsymbol{\vartheta}} = - \hat{\boldsymbol{\omega}} \times \delta  \boldsymbol{\vartheta} + \delta \boldsymbol{\omega}
\label{eq:mekf-v-dot}
\end{equation}
whilst the fourth component is $\delta \dot{q}_4 = 0$. The previous equation will be used to propagate the error-angle covariance. Its expected value expression is
\begin{equation}
\delta \dot{\hat{\boldsymbol{\vartheta}}} = - \hat{\boldsymbol{\omega}} \times \delta \hat{\boldsymbol{\vartheta}}
\label{eq:mekf-v-hat-dot}
\end{equation}

\par The measurement update step works within the error-state and partially in discrete-time. 
\par The observation model follows the definition in \eqref{eq:measurement-ekf}, such that
\begin{equation}
    \mathbf{y} = h(\mathbf{q}, \boldsymbol{\xi}) + \boldsymbol{\vartheta}, \boldsymbol{\vartheta} \sim N(\mathbf{0}, R )
    \label{eq:mekf-measurement2}
\end{equation}
that gives the following sensitivity matrix
\begin{equation}
    H(\mathbf{q}, \boldsymbol{\xi}) = \frac{\partial h}{\partial (\Delta \mathbf{x})} = \begin{bmatrix}
        \frac{\partial h}{\partial (\delta \boldsymbol{\vartheta} )} & \frac{\partial h}{\partial \boldsymbol{\xi}} 
       \end{bmatrix} = \begin{bmatrix}
       H_{\boldsymbol{\vartheta}} & H_{\boldsymbol{\xi}} 
       \end{bmatrix}
    \label{eq:mekf-measurement}
\end{equation}
By substituting \eqref{eq:mekf-delta-q-alt} and the quaternion operations in \eqref{eq:mekf-q-true-dot} it gives 
\begin{equation}
    \mathbf{q} = \left( I_q + \frac{1}{2} \begin{bmatrix}
        \boldsymbol{ \delta \vartheta} \\  
        0
       \end{bmatrix} \right) \otimes \hat{\mathbf{q}} = \hat{\mathbf{q}} + \frac{1}{2} \Xi(\hat{\mathbf{q}}) \boldsymbol{ \delta \vartheta}
\label{eq:mekf-q-true-alt}
\end{equation}
From the previous equation, it follows that the attitude part of the measurement sensitivity matrix can be evaluated by using the chain rule 
\begin{equation}
H_v = \frac{\partial h}{\partial \mathbf{q}} \frac{\partial \mathbf{q} }{\partial (\boldsymbol{ \delta \vartheta})} = \frac{1}{2} \frac{\partial h}{\partial \mathbf{q}} \Xi(\hat{\mathbf{q}})
\label{eq:mekf-H-v}
\end{equation}

In order to avoid recalculating the nonlinear function $h(\hat{\mathbf{q}}, \hat{\boldsymbol{\xi}}) $ if batch measurements are processed, the first-order Taylor series is used to compute the
expectation
\begin{equation}
E \{\ h(\mathbf{q}, \boldsymbol{\xi}) \} \simeq h(\hat{\mathbf{q}}, \hat{\boldsymbol{\xi}}) +  H(\hat{\mathbf{q}}, \hat{\boldsymbol{\xi}}) \begin{bmatrix}
  \boldsymbol{ \delta \vartheta}  \\  \Delta \hat{\boldsymbol{\xi}}
   \end{bmatrix} 
\label{eq:mekf-expected-h}
\end{equation}

The state update for the $k$-th measurement is then 
\begin{equation}
    \begin{bmatrix}
    \boldsymbol{ \delta \vartheta}_k^+ \\ \Delta \hat{\boldsymbol{\xi}}_k^+  
    \end{bmatrix}
    =
    \begin{bmatrix}
    \boldsymbol{ \delta \vartheta}_k^- \\ \Delta \hat{\boldsymbol{\xi}}_k^-  
    \end{bmatrix}
    +
    K_k \left\{ \mathbf{y}_k - h(\hat{\mathbf{q}}_k^- , \hat{\boldsymbol{\xi}}_k^- ) -  H(\hat{\mathbf{q}}_k^- , \hat{\boldsymbol{\xi}}_k^- ) \begin{bmatrix}
        \boldsymbol{ \delta \vartheta}_k^-  \\  \Delta \hat{\boldsymbol{\xi}}_k^-
         \end{bmatrix} 
    \right\}
\label{eq:mekf-state-update}
\end{equation}
where $K_k$ is computed as defined in the previous section. 
\par The reset step is responsible for converting the local $\Delta \mathbf{x}^+ $ representation into the global representation $\mathbf{x}^-$ and resets the error state to zero, such as
\begin{equation}
\hat{\mathbf{q}}^+ = \mathbf{q} ( \hat{\boldsymbol{\delta  \vartheta} }^+ ) \otimes \hat{\mathbf{q}}^- = \frac{1}{\sqrt{1 + || \hat{\boldsymbol{ \delta \vartheta} }^+ /2 ||^2 }} \begin{bmatrix}
     \hat{\boldsymbol{ \delta\vartheta} }^+ /2 \\ 1
\end{bmatrix} \otimes \hat{\mathbf{q}}^-
\label{eq:mekf-reset-v}
\end{equation}
\begin{equation}
    \hat{\boldsymbol{\xi}}_k^+ =  \hat{\boldsymbol{\xi}}_k^- + K_{\xi k} [ \mathbf{y}_k - h_k (\hat{\mathbf{y}}_k^- , \hat{\boldsymbol{\xi}}_k^-) ]
\label{eq:mekf-reset-xi}
\end{equation}

\vspace{0.4cm}
\par \textbf{Mission mode Kalman filter}
\par The mission mode EKF estimates the attitude and the gyroscope bias with a 6-state EKF and was first introduced in \cite{leffertsKalmanFilteringSpacecraft1982}.
Precise angular rate information is vital to ensure precise attitude knowledge, filter noisy attitude data as well as to perform precise manoeuvres.
In a pursuit of better accuracy, the gyroscope for dynamic model replacement method is employed. Instead of using the dynamic equations defined in \ref{chapter:background}
for the propagation step of the filter, information from modern accurate gyroscope is part of the dynamic model. The main advantages of this method are increased accuracy and 
decreased computational load. The rotational dynamic models are often inaccurate and torque measurements often unavailable, and would corrupt the information from the gyroscope. 
The computation of the dynamics equations is also much more cumbersome than the gyroscope alternative.
\par The filter assumes that misalignment, as well as scale factors, have already been determined. An update is performed at each time step.

% states
\par The global state of the filter is given as
\begin{equation}
\mathbf{x} = \begin{bmatrix}
    \mathbf{q} \\
    \boldsymbol{\beta}
\end{bmatrix}
\label{eq:mission-state}
\end{equation}
whilst the local error state is given as
\begin{equation}
    \Delta \mathbf{x} = \begin{bmatrix}
        \delta \mathbf{q} \\
        \Delta \boldsymbol{\beta}
    \end{bmatrix}
\label{eq:mission-error-state}
\end{equation}
where $ \Delta \boldsymbol{\beta} = \boldsymbol{\beta} - \hat{\boldsymbol{\beta}}$. From the gyroscope model presented in Section \ref{subsection:sensors-model}, both $ \dot{\boldsymbol{\beta}} $ and $\dot{\hat{\boldsymbol{\beta}}}$ are $0$.

% propagation 
The propagation step propagates the global variables, the quaternion and bias estimates. The discrete-time quaternion propagation follows 

\begin{equation}
    \hat{\mathbf{q}}_{k+1}^{-}=\exp [(\Delta \boldsymbol{\theta} / 2) \otimes] \hat{\mathbf{q}}_{k}^{+} \approx \bar{\Theta}\left(\hat{\boldsymbol{\omega}}_{k}^{+}\right) \hat{\mathbf{q}}_{k}^{+}
\end{equation}
with 
\begin{equation}
    \bar{\Theta}( \hat{\omega}_{k}^{+} ) \equiv \begin{bmatrix}
        \cos \left(\frac{1}{2}\left\|\hat{\omega}_{k}^{+}\right\| \Delta t\right) I_{3}-\left[\hat{\psi}_{k}^{+} \times\right] & \hat{\psi}_{k}^{+} \\
        -\hat{\psi}_{k}^{+T} & \cos \left(\frac{1}{2}\left\|\hat{\omega}_{k}^{+}\right\| \Delta t\right)
    \end{bmatrix}
\end{equation}
where 
\begin{equation}
    \hat{\boldsymbol{\psi}}_{k}^{+} \equiv \frac{\sin \left(\frac{1}{2}\left\|\hat{\boldsymbol{\omega}}_{k}^{+}\right\| \Delta t\right) \hat{\boldsymbol{\omega}}_{k}^{+}}{\left\|\hat{\boldsymbol{\omega}}_{k}^{+}\right\|}
\end{equation}
    

In order to define the covariance propagation matrix, the error-state vector dynamics must be defined, such that
\begin{equation}
\Delta \dot{\mathbf{x}}(t) = F(t) \Delta \mathbf{x} (t) + G(t) \mathbf{w}(t)
\label{eq:mission-error-dynamics}
\end{equation}
where 
\begin{equation}
    \mathbf{w}(t) = \begin{bmatrix} \sigma_v (t) \\ \sigma_u (t) \end{bmatrix}
    \label{eq:}
\end{equation}
and the matrices $F(t)$ and $G(t)$ follow 
\begin{equation}
    F(t) = \begin{bmatrix}
        -[\hat{\boldsymbol{\omega}}(t) \times] & -I_3 \\
        0_{3 \times 3} & 0_{3 \times 3}
    \end{bmatrix}
\label{eq:mission-F}
\end{equation}
\begin{equation}
    G(t) = \begin{bmatrix}
        -I_3 & 0_{3 \times 3} \\
        0_{3 \times 3} & I_3
    \end{bmatrix}
\label{eq:mission-G}
\end{equation}

\begin{equation}
    Q= \begin{bmatrix}
        \sigma_v^2 I_3 & 0_{3 \times 3} \\
        0_{3 \times 3} & \sigma_u^2 I_3
    \end{bmatrix}
\label{eq:mission-Q}
\end{equation}

The discrete-time error state transition matrices for this case  is given by 

\begin{equation}
    \Phi = \begin{bmatrix}
        \Phi_{11} & \Phi_{12} \\
        \Phi_{21} & \Phi_{22}
    \end{bmatrix}
\label{eq:mission-phi-mat}
\end{equation}
with 
\begin{subequations}
    \begin{gather}
        \Phi_{11} =  I_3 - [\hat{\boldsymbol{\omega}} \times] \frac{sin(||\hat{\boldsymbol{\omega}}|| \Delta t)}{||\hat{\boldsymbol{\omega}}||}
        + [\hat{\boldsymbol{\omega}} \times]^2 \frac{ \{ 1 - cos(||\hat{\boldsymbol{\omega}}|| \Delta t) \}  }{||\hat{\boldsymbol{\omega}} ||^2}\\
        \Phi_{12} = [\hat{\boldsymbol{\omega}} \times] \frac{ \{ 1 - cos(||\hat{\boldsymbol{\omega}}|| \Delta t) \}  }{||\hat{\boldsymbol{\omega}} ||^2} 
        - I_3 \Delta t 
        - [\hat{\boldsymbol{\omega}} \times]^2 \frac{  \{ || \hat{\boldsymbol{\omega}} || \Delta t - sin(|| \hat{\boldsymbol{\omega}} || \Delta t)  \} }{ || \hat{\boldsymbol{\omega}} ||^3 } \\
        \Phi_{21} =  0_{3 \times 3} \\
        \Phi_{22} = I_3
    \end{gather}
\end{subequations}
The formula for the covariance matrix $Q$ in discrete-time is formulated in \cite{crassidisOptimalEstimationDynamic2012a}. % Optimal Estimation of Dynamic Systems
It assumes that $ \hat{\boldsymbol{\omega}} $ is constant
throughout the sampling time and that the sampling rate is below Nyquist's limit (for example, with a safety factor of 10, it is required that $ ||\hat{\boldsymbol{\omega}}|| \Delta t < \pi /10 $ ).

\begin{equation}
Q_k \approx  \begin{bmatrix}
    (\sigma_v^2 \Delta t + \frac{1}{3} \sigma_u^2 \Delta t^3 ) I_3 & -( \frac{1}{2} \sigma_u^2 \Delta t^2) I_3 \\
    -(\frac{1}{2} \sigma_u^2 \Delta t^2 ) I_3 & (\sigma_u^2 \Delta t) I_3
\end{bmatrix}
\label{eq:mission-q-k}
\end{equation}


The discrete-time covariance propagation follows the definition from Section \ref{section:lkf} with 
\begin{equation}
\Upsilon=\left[\begin{array}{cc}
    -{I}_{3} & 0_{3 \times 3} \\
    0_{3 \times 3} & {I}_{3}
\end{array}\right]
\end{equation}

The bias propagation is given by 
\begin{equation}
    \hat{\boldsymbol{\beta}}_{k+1}^{-} = \hat{\boldsymbol{\beta}}_{k}^{+}
    \label{eq:bias-porp}
\end{equation}
    


% update step
\par The measurement step receives observations that can be derived from \eqref{eq:mekf-measurement}, for $N$ vector observation at time $t_k$, given as 
\begin{equation}
    \mathbf{y}_{k}=\left.\left[\begin{array}{c}
        A\left(\mathbf{q} \right) \mathbf{r}_{1} \\
        A\left(\mathbf{q} \right) \mathbf{r}_{2} \\
        \vdots \\
        A\left(\mathbf{q} \right) \mathbf{r}_{N}
        \end{array}\right]\right|_{t_{k}}+\left.\left[\begin{array}{c}
        \boldsymbol{v}_{1} \\
        \boldsymbol{v}_{2} \\
        \vdots \\
        \boldsymbol{v}_{N}
        \end{array}\right]\right|_{t_{k}} \equiv \mathbf{h}_{k}\left(\mathbf{x}_{k} \right)+\mathbf{v}_{k}
\label{eq:mission-measurement}
\end{equation}


\begin{equation}
    R = diag([R_1 R_2 \dots R_N])
    \label{eq:misison-covariance}
\end{equation}

where $R_i$ is the covariance of $\boldsymbol{\vartheta}_i$. The covariance can either be simplified to $R_i = \sigma_i^2 I_3$ under the assumption that 
the measurement errors are isotropic or follow the QUEST measurement model \cite{markleyFundamentalsSpacecraftAttitude2014}, where the measurements are not isotropic
but lead to computational disadvantages. 

The process that leads to the observation sensitivity matrix $H_k (\hat{\mathbf{x}}_k^-)$ is presented. The true attitude matrix $A( \mathbf{q} )$
is related to the \textit{a priori} attitude matrix $A( \hat{\mathbf{q}}^- )$ through 
\begin{equation}
A( \mathbf{q} ) = A( \delta \mathbf{q} ) A( \hat{\mathbf{q}}^- )
\label{eq:mission-obs1}
\end{equation}

The first-order approximation of the error-attitude matrix $A( \delta \mathbf{q} ) $ is given by 

\begin{equation}
    A( \delta \mathbf{q} ) \simeq I_3 - [\delta \boldsymbol{\vartheta} \times ]
    \label{eq:mission-obs2}
\end{equation}


For each sensor, the true $\mathbf{r}$, and estimated body vectors $\mathbf{b}$, are given by 

\begin{equation}
    \mathbf{b} = A( \mathbf{q} ) \mathbf{r}
    \label{eq:mission-obs3}
\end{equation}
\begin{equation}
    \hat{\mathbf{b}} = A( \hat{\mathbf{q}}^- ) \mathbf{r}
    \label{eq:mission-obs4}
\end{equation}
\begin{equation}
    \Delta \mathbf{b} \equiv \mathbf{b} -\hat{\mathbf{b}}^{-}=-[\delta \boldsymbol{\vartheta} \times] A\left(\hat{\mathbf{q}}^{-}\right) \mathbf{r}=\left[\hat{\mathbf{b}}^{-} \times\right] \delta \boldsymbol{\vartheta}
    \label{eq:mission-obs5}
\end{equation}

Therefore the sensitivity matrix for all measurements is given by 
\begin{equation}
H_{k}\left(\hat{\mathbf{x}}_{k}^{-}\right)=\left.\left[\begin{array}{cc}
    {\left[\hat{\mathbf{b}}_{1}^{-} \times\right]} & 0_{3 \times 3} \\
    {\left[\hat{\mathbf{b}}_{2}^{-} \times\right]} & 0_{3 \times 3} \\
    \vdots & \vdots \\
    {\left[\hat{\mathbf{b}}_{N}^{-} \times\right]} & 0_{3 \times 3}
    \end{array}\right]\right|_{t_{k}}
    \label{eq:mission-sensitivity-matrix}
\end{equation}

The filter is reset before every measurement update implicitly. 

The mission mode attitude estimation algorithm is summarized in Tab. \ref{tab:mekf-eqs}. The filter is
first initialized with a fixed state and an error covariance matrix. The first three diagonal elements of the error
covariance matrix correspond to attitude errors and the second three correspond to the bias. 
The estimated corrected angular velocity from the gyroscope is used to propagate the quaternion kinematic
model and error covariance in the EKF.
Then, the Kalman gain is computed
using the measurement-error covariance $R_k$ and sensitivity matrix in \eqref{eq:mission-sensitivity-matrix}
The state error covariance and state error follow the standard EKF update. The quaternion is reset follows \eqref{eq:mekf-reset-v} and the bias
follows \eqref{eq:mekf-reset-xi}.

% PICTURE 3
\begin{comment}[H]
    \centering
    \includegraphics[width=\textwidth]{Figures/temp-mission-equations.png}
    \caption[misison mode filter]{EKF equations}
    \label{temp3}
  \end{comment}
  \begin{table}[H]
    \centering
      \begin{tabular}{lll}
          \hline
          \multirow[t]{3}{*}{Initial condition} & $\hat{\mathbf{q}}(t_0) = \hat{\mathbf{q}}_0$ \\
          & $\hat{\boldsymbol{\beta}}(t_0) = \hat{\boldsymbol{\beta}}_0$ \\
          & $P(t_0) = P_0$ \\
          \hline
          \multirow[t]{3}{*}{Gain} & $K_{k}=P_{k}^{-} H_{k}^{T}\left(\hat{\mathbf{x}}_{k}^{-}\right)\left[H_{k}\left(\hat{\mathbf{x}}_{k}^{-}\right) P_{k}^{-} H_{k}^{T}\left(\hat{\mathbf{x}}_{k}^{-}\right)+H_{k}\right]^{-1}$ \\
          & $H_{k}\left(\hat{\mathbf{x}}_{k}^{-}\right)=\left.\left[\begin{array}{cc}
            {\left[\hat{\mathbf{b}}_{1}^{-} \times\right]} & 0_{3 \times 3} \\
            {\left[\hat{\mathbf{b}}_{2}^{-} \times\right]} & 0_{3 \times 3} \\
            \vdots & \vdots \\
            {\left[\hat{\mathbf{b}}_{N}^{-} \times\right]} & 0_{3 \times 3}
            \end{array}\right]\right|_{t_{k}}$ \\
          \hline
          \multirow[t]{3}{*}{Update} & $ P_{k}^{+}=\left[I-K_{k} H_{k}\left(\hat{\mathbf{x}}_{k}^{-}\right)\right] P_{k}^{-}$ \\
          & $\Delta \hat{\mathbf{x}}_{k}^{+}=K_{k}\left[\mathbf{y}_{k}-\mathbf{h}_{k}\left(\hat{\mathbf{x}}_{k}^{-}\right)\right]$ \\
          & $\Delta \hat{\mathbf{x}}_{k}^{+} \equiv\left[\begin{array}{ll}
            \hat{\boldsymbol{ \delta \vartheta}}_{k}^{+T} & \Delta \hat{\boldsymbol{\beta}}_{k}^{+T}
            \end{array}\right]^{T}$ \\
          & $\mathbf{h}_{k}\left(\hat{\mathbf{x}}_{k}^{-}\right)=\left.\left[\begin{array}{c}
            A\left(\hat{\mathbf{q}}^{-}\right) \mathbf{r}_{1} \\
            A\left(\hat{\mathbf{q}}^{-}\right) \mathbf{r}_{2} \\
            \vdots \\
            A\left(\hat{\mathbf{q}}^{-}\right) \mathbf{r}_{N}
            \end{array}\right]\right|_{t_{k}}$ \\
          & $\hat{\mathbf{q}}^{*}=\hat{\mathbf{q}}_{k}^{-}+\frac{1}{2} \Xi\left(\hat{\mathbf{q}}_{k}^{-}\right) \boldsymbol{\delta} \hat{\vartheta}_{k}^{+}$ \\
          & $\hat{\mathbf{q}}_{k}^{+}=\mathbf{q}^{*} /\left\|\mathbf{q}^{*}\right\|$ \\
          & $\hat{\boldsymbol{\beta}}_{k}^{+}=\hat{\boldsymbol{\beta}}_{k}^{-}+\Delta \hat{\boldsymbol{\beta}}_{k}^{+}$ \\
          \hline
          \multirow[t]{3}{*}{Propagation} & $\hat{\boldsymbol{\omega}}(t)=\boldsymbol{\omega}_g(t)-\hat{\boldsymbol{\beta}}(t) $\\
          & $\dot{\hat{\mathbf{q}}}(t)=\frac{1}{2} \Xi(\hat{\mathbf{q}}(t)) \hat{\boldsymbol{\omega}}(t)$ \\
          & $\dot{P}(t)=F(t) P(t)+P(t) F^{T}(t)+G(t) Q(t) G^{T}(t)$ \\
          \hline
      \end{tabular}%
      \caption[MEKF for mission mode.]{MEKF for mission mode - adapted from \cite{markleyFundamentalsSpacecraftAttitude2014}.}
    \label{tab:mekf-eqs}%
  \end{table}