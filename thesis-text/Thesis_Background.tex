%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
%     File: Thesis_Background.tex                                      %
%     Tex Master: Thesis.tex                                           %
%                                                                      %
%     Author: Andre C. Marta                                           %
%     Last modified :  2 Jul 2015                                      %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Background}
\label{chapter:background}

In this chapter, the concepts needed to develop the estimation and control algorithms are presented. 
In Section \ref{section:reference-frames}, the frames of reference for used to describe the relation between the satellite and the Earth are presented.
In Section \ref{section:attitude-representations}, the various attitude representations are presented and their benefits and shortcomings analyzed.
The attitude error representations are also derived.
In Section \ref{section:attitude-kinematics-dynamics}, the laws of motion that describe the behaviour of the satellite are presented. 
The kinematics, i.e., the variations of orientation when only the satellite is considered, are studied. 
The attitude dynamics relate torques with the inertia of the satellite, which generates angular accelerations.
In Section \ref{section:orbit-dynamics}, the orbital motion of the satellite is described, along with the parameters to describe it.
Finally, in Section \ref{section:env-model}, the influence of external torques and forces by the space environment on the satellite are studied. 

\section{Reference Frames}
\label{section:reference-frames}
Different three-dimensional reference frames are useful when exploring an attitude analysis problem. 
These are specified by the origin's location and the orientation of their coordinate axis. 
Different frames are used to model different behaviours of the satellite, as well as the environment that encloses it.
This section introduces these useful frames and 
is based on \cite{noureldinFundamentalsInertialNavigation2013}. %Fundamentals of Inertial Navigation, Satellite-based Positioning and their Integration

\subsection{Body Frame}
\label{subsection:body-frame}
The body frame of the satellite, designated as $\mathcal{B}$, has its origin in the estimated centre of mass and its axis rigidly
connected to the satellite. It is composed by three unit vectors $ \mathcal{B} = \{b_1,b_2, b_3\}$, such that $b_3$
is aligned with the telescope of the payload, $b_1$ is aligned with 3U length of the satellite and
$b_2$ completes the right-hand coordinate system. 
\subsection{Inertial Reference Frame}
\label{subsection:inertial-frame}
In an inertial frame, all Newton's laws of motion are valid. Any frame moving at constant
velocity and without rotation with respect to an inertial frame is also inertial. 
The frame of choice for near-Earth environments is the Earth-centered Inertial (ECI) frame, designated as $\mathcal{I} = \{i_1,i_2, i_3\}$. 
It has as origin the centre of mass of the Earth, with $i_3$ aligned with the Earth's rotation axis
through the conventional terrestrial pole (defined as an average of the poles from 1900 to
1905), $i_1$ aligned with the equatorial plane, pointing towards the vernal equinox and $i_2$ completes the right-hand coordinate system.
The ECI frame is represented in Fig. \ref{fig:eci-ecef}.

\subsection{Earth-Centered/Earth-Fixed Frame}
\label{subsection:ecef}
The Earth-Centered/Earth-Fixed frame (ECEF) is defined as $\mathcal{E} = \{e_1,e_2, e_3\}$. The axis $e_3$
is aligned with $i_3$, however $e_3$ points in the direction of Earth's prime meridian, $e_2$
completes the right-hand coordinate system. The rotation angle between $\mathcal{E}$ and $\mathcal{I}$
is known as Greenwich Mean Sidereal Time (GMST) angle, represented as $\theta_{GMST}$, and
follows the Earth's rotation. It is calculated based on that instant time a number of Julian 
centuries elapsed from the J2000 epoch. The ECEF frame is represented in Fig. \ref{fig:eci-ecef}.

The transformation of a position vector $\mathbf{r}$ from its ECI representation $\mathbf{r}_{\mathcal{I}}$ to its ECEF 
representation $\mathbf{r}_{\mathcal{E}}$ follows:

\begin{equation}
    \mathbf{r}_{\mathcal{E}} =  \begin{bmatrix}
	 \cos \theta_{GMST}& \sin\theta_{GMST}  & 0 \\
	 -\sin\theta_{GMST} & \cos\theta_{GMST}  & 0 \\
	 0  & 0 & 1
    \end{bmatrix}\mathbf{r}_{\mathcal{I}} 
    \label{eq:eci2ecef}
\end{equation}
The comparison of both frames can also be seen in Fig. \ref{fig:eci-ecef}.
\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{Figures/ECI-ECEF.png}
    \caption[ECI and ECEF frames.]{ECI (black) and ECEF (blue) frames, adapted from \cite{noureldinFundamentalsInertialNavigation2013}}
    \label{fig:eci-ecef}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Attitude Representations}
\label{section:attitude-representations}
The attitude of a satellite is defined as the orientation of the satellite's frame $\mathcal{B}$ with respect to the
reference system $\mathcal{I}$. There are a few different ways of representing the orientation, each
with its advantages and shortcomings. The most significant ones for this work will be analyzed.
This section represents the useful representations for this work and is based on \cite{markleyFundamentalsSpacecraftAttitude2014}. 
%\textcolor{red}{Find one more reference}

%Spacecraft attitude determination is the study of methods for estimating the proper orthogonal matrix that 
%transforms vectors from a reference frame to a frame fixed to the satellite body. 

% rotm
\subsection{Rotation Matrix Representation}
\label{subsection:attitude-matrix}
The rotation (or attitude) matrix, denoted by the letter $A$, is the fundamental representation to express a rotation between two axes. 
 %Thus, it is the study of proper orthogonal $3 \times 3$ matrices, or matrices in the SO(3) group. 
The attitude matrix $A_{GF}$
represents the transformation from a reference frame $F$ to a reference frame $G$ and is given by
\begin{equation}
A_{GF} \mathbf{x}_F = \mathbf{x}_G
\label{eq:bck-rotm}
\end{equation}
The matrix $A_{GF}$ is a proper orthogonal $3 \times 3$ matrix, as all matrices in the special orthogonal group of dimension $3$ ($SO(3)$) group, and is often designated as a 
Direction Cosine Matrix (DCM). Although it does not suffer from singularities and does not require large computation efforts, 
it is composed of 9 parameters, whereas in reality it has 6 scalar constraints. 
This is due to the orthogonality constraint $A A^T = I$ and symmetry requirement. 


% euler axis
\subsection{Euler Axis/Angle Representation}
\label{subsection:euler-axis-agle}
\par Euler's Theorem states that every rotation matrix consists of a rotation around a fixed axis, the
$ \mathbf{e}$ unit vector, that has the same representation in both frames. 
 This statement is equivalent to substituting $\mathbf{x}_F$ and $\mathbf{x}_G$ by $\mathbf{e}$ in \eqref{eq:bck-rotm}. 
 It can be proven that the rotation axis $\mathbf{e}$ is the eigenvector, with a corresponding eigenvalue of $1$.
 \par To completely define the matrix, the angle of rotation about the rotation axis, $\vartheta$, should be specified. 
 The corresponding attitude matrix is as follows:

\begin{equation}
    A(\mathbf{e}, \vartheta )= \textit{I} - \sin\vartheta [\mathbf{e}\times] + (1- \cos\vartheta)[\mathbf{e}\times]^2 
\label{eq:euler-axis2attitude}
\end{equation}

where $[\square \times]$ is the skew-symmetric matrix, defined in Appendix \ref{section:appendix-cross}.

This representation seems to be dependent on four parameters ($\mathbf{e}$ and $\vartheta$) but when the constraint $ || \mathbf{e} || = 1$ is imposed
there are only three independent parameters. From \eqref{eq:euler-axis2attitude}, it can be said that the representation 
is a periodic function of the rotation angle over an unlimited range, with a period of $2 \pi$.

\subsection{Rotation vector representation}
A three-component representation can be computed from the combination of the Euler axis and angle, with $\boldsymbol{\vartheta} \equiv \vartheta \mathbf{e}$, such that 
\begin{equation}
    A(\mathbf{e}, \vartheta) = \exp ( - [ \boldsymbol{\vartheta} \times ] )
\end{equation}
where $\exp$ is the matrix exponential form. Although this representation is useful to analyze small rotations, when it comes to 
large displacements it obscures the periodicity of the attitude matrix as a function of $\vartheta$. 


%euler angles
\subsection{Euler Angles}
An Euler angle representation expresses a rotation by successive rotations around an initial frame, with constant
column vectors and variable rotation angles. The vectors chosen in the classical Euler angle representation are the 
unit vectors for the orthogonal axis, such that 
\begin{equation}
    A_{zyx} = A(\mathbf{e}_z , \psi ) A(\mathbf{e}_y , \theta ) A(\mathbf{e}_x , \phi)
\end{equation}
where 
\begin{subequations}
    \begin{equation}
        A(\mathbf{e}_z , \psi ) = \begin{bmatrix}
           \cos \psi & -\sin \psi & 0 \\
           \sin \psi & \cos \psi & 0 \\
           0 & 0 & 1
        \end{bmatrix}
    \end{equation}
    \begin{equation}
        A(\mathbf{e}_y , \theta ) = \begin{bmatrix}
           \cos \theta & 0 & \sin \theta \\
           0 & 1 & 0 \\
           -\in \theta & 0 & \cos \theta
        \end{bmatrix}
    \end{equation}
    \begin{equation}
        A(\mathbf{e}_x , \phi) = \begin{bmatrix}
           1 & 0 & 0 \\
           0 & \cos \theta & -\sin \theta \\
           0 & \sin \theta & \cos \theta
        \end{bmatrix}
    \end{equation}
\end{subequations}

Euler angles offer a transparent attitude representation due to their straightforward physical interpretation, in addition to not having redundant parameters.
Due to the use of trigonometric functions and singularities for extreme values, their usefulness is limited. 

% quat
\subsection{Quaternion Representation}
\label{subsection:quaternion}

The quaternion concept was introduced as an extension to complex numbers, defined as
\begin{equation}
    \mathbf{q}(\mathbf{e}, \vartheta) = \begin{bmatrix} \mathbf{e}sin(\vartheta / 2) \\ cos(\vartheta / 2) \end{bmatrix}
\label{eq:quat-def}
\end{equation}
and obeys the parameterization and operations defined in Appendix \ref{section:appendix-quat}.

Unit quaternions, i.e. quaternions with unit norm, are used to parameterize rotations. The
quaternion representation of the attitude matrix is defined as:
\begin{equation}
    \mathbf{A}(\mathbf{q}) = (q_4^2 - ||{\mathbf{q}_{1:3}}||^2) I_3 -
    2q_{4}[\mathbf{q}_{1:3} \times] + \mathbf{q}_{1:3}\mathbf{q}_{1:3}^T
    \label{eq:quat2rotm}
\end{equation}

% \par The rotation of a three-component vector, implemented by quaternion multiplication, is also shown in Annex TODO. 


% advj
In comparison to other attitude representations, the quaternion has fewer scalars (only 4), as well
as less computational intense functions for the useful operations. 
When compared to the attitude matrix, they have fewer parameters and only follow the unit norm constraint,
as opposed to the six constraints. 
On the other hand, the quaternions representation's greatest disadvantage is that $\mathbf{q}$ and $-\mathbf{q}$ give the same
attitude matrix. This can be avoided by restricting the quaternions to one hemisphere of the $S^3$, which is 
usually taken to be the $q_4 \geq 0$ hemisphere. However, this leads to the same type of discontinuities present in 
other attitude representations since it effectively diminishes the number of useful parameters.  



% other
Other methods to represent attitude, such as the (Modified) Rodrigues parameters, are not discussed in this work. 

\subsection{Attitude error representation}
In order to evaluate estimation and control algorithms for the attitude, it is relevant to introduce attitude errors for the considered representations.
% rotm
\par When it comes to the attitude matrix, the rotation between a reference frame $\mathcal{I}$ and a satellite body frame $\mathcal{B}$ can be represented as a small rotation $A_{\mathcal{B}\hat{\mathcal{B}}}$ between
$\mathcal{B}$ and an estimated body frame $\hat{\mathcal{B}}$, such as 
\begin{equation}
    A_{\mathcal{B}\mathcal{I}} = A_{\mathcal{B}\hat{\mathcal{B}}} A_{\hat{\mathcal{B}}\mathcal{I}}
\end{equation}
In the case of absence of errors, the matrix $A_{\mathcal{B}\mathcal{I}}$ is an identity matrix. 

% rot vec
\par An attitude error represented by the rotation vector, with the small-angle approximation,  is given by 

\begin{equation}
    A(\boldsymbol{\delta \vartheta}) = \exp ( - [ \boldsymbol{\delta \vartheta} \times ] ) \approx I_3 - [ \boldsymbol{\delta \vartheta} \times ] + \frac{1}{2}[ \boldsymbol{\delta \vartheta} \times ]^2
\end{equation}

% quat
\par The attitude error in terms of quaternions is given by 
\begin{equation}
    A(\boldsymbol{\delta} \mathbf{q}) \approx I_3 - 2 [\boldsymbol{\delta} \mathbf{q}_{1:3} \times ] + [\boldsymbol{\delta} \mathbf{q}_{1:3} \times ]^2
\end{equation}

\par It is relevant to note that these expressions are equivalent through second-order considering
\begin{equation}
    \boldsymbol{\delta \vartheta} = 2 \boldsymbol{\delta} \mathbf{q}_{1:3}
    \label{eq:bck-delta-v}
\end{equation}


\section{Attitude Kinematics and Dynamics}
\label{section:attitude-kinematics-dynamics}
In order to characterize the rotation of the satellite, the kinematics and dynamics must be defined. 
The kinematics show the rotational motion of the satellite body frame $\mathcal{B}$ with respect to the inertial frame $\mathcal{I}$.
For this study, the point mass model is not satisfactory and a rigid body model is used instead.  
Again, this section is based on \cite{markleyFundamentalsSpacecraftAttitude2014}. 

\par To describe attitude variations, the angular velocity $\boldsymbol{\omega}_\mathcal{B}^{\mathcal{B}\mathcal{I}}$ will be defined as:

\begin{equation}
    \boldsymbol{\omega}_\mathcal{B}^{\mathcal{B}\mathcal{I}}(t) = \lim_{\Delta t \rightarrow 0} \frac{\Delta \boldsymbol{\vartheta}_\mathcal{B}^{\mathcal{B}\mathcal{I}}}{\Delta t}
\label{eq:angular-velocity}
\end{equation}
The superscript $\mathcal{B}\mathcal{I}$ means that it relates to the rotation from frame $\mathcal{I}$ to frame $\mathcal{B}$, and the subscript $\mathcal{B}$
means that the rotation vector is represented in frame $\mathcal{B}$. 
The attitude matrix is the fundamental representation of a rotation. The rotation fundamental
equation of attitude kinematics, from the frame $\mathcal{I}$ to frame $\mathcal{B}$, is given by
\begin{equation}
    \dot{\mathbf{A}}_{\mathcal{B}\mathcal{I}} = -\mathbf{A}_{\mathcal{B}\mathcal{I}} \left[ \boldsymbol{\omega}_{\mathcal{I}}^{\mathcal{B}\mathcal{I}} \times \right] = \mathbf{A}_{\mathcal{B}\mathcal{I}} \left[ \omega_{\mathcal{I}}^{\mathcal{I}\mathcal{B}} \times \right] 
\label{eq:attitude-kinematics}
\end{equation}

The quaternion representation for attitude equivalent to $\mathbf{A}_{\mathcal{B}\mathcal{I}}$ has the following equation for the kinematic motion:
\begin{equation}
    \dot{\mathbf{q}}_{\mathcal{B}\mathcal{I}} = \frac{1}{2} \boldsymbol{\omega}_{\mathcal{I}}^{\mathcal{I}\mathcal{B}} \otimes  \mathbf{q}_{\mathcal{B}\mathcal{I}}=
    \frac{1}{2}\Omega(\boldsymbol{\omega}_{\mathcal{I}}^{\mathcal{I}\mathcal{B}})\mathbf{q}_{\mathcal{B}\mathcal{I}} = \frac{1}{2} \Xi(\mathbf{q}) \omega_{\mathcal{I}}^{\mathcal{I}\mathcal{B}} 
\label{eq:quaternion-kinematics}
\end{equation}
where $\Xi(\mathbf{q})$ is computed according to Appendix \ref{section:appendix-quat} and 
\begin{equation}
    \Omega(\boldsymbol{\omega}) = \begin{bmatrix}
      [\boldsymbol{\omega} \times] & -\boldsymbol{\omega} \\
      \boldsymbol{\omega} & 0
    \end{bmatrix}
  \end{equation}
%$\Omega(\omega_{\mathcal{I}}^{\mathcal{I}\mathcal{B}})$
%% \section{Attitude Dynamics}
%% \label{section:attitude-dynamics}
\par In order to study how forces and torques are applied to the satellite and how they interact with
its kinematics, the angular momentum must be taken into account. We will consider a satellite to be 
made of $n$ point masses. The angular momentum with respect to the origin $0$ of an inertial 
coordinate frame is defined in terms of the different masses $m_i$, positions 
$\mathbf{r}^{i0}$, and velocities $\mathbf{v}^{i0} = \dot{\mathbf{r}}^{i0}$
of the points relative to 0 by:
\begin{equation}
H^{0} = \sum_{i=1}^{n} \mathbf{r}^{i0} \times m_i \mathbf{v}^{i0}
\label{eq:momentum}
\end{equation}
    
Newton's second law of motion states that 
$m_i \dot{\mathbf{v}}_{\mathcal{I}}^{i0} = F_{\mathcal{I}}^{i}$ in an inertial reference frame, 
and $\mathbf{v}^{i0} \times \mathbf{v}^{i0} = 0$, so the angular momentum obeys the
equation:
\begin{equation}
\dot{H}_{\mathcal{I}}^{0} = \sum_{i=1}^{n} \mathbf{r}^{i0} \times F_{\mathcal{I}}^{iext} \equiv L_\mathcal{I}^0
\label{eq:momentum-dot}
\end{equation}
where  $F^{iext}$ is the force exerted on $i$ by everything external to the system of mass points and $L_{\mathcal{I}}^0$ is the net torque about 0 
assuming that the force exerted by a mass point on itself is zero and the strong law of action and reaction
\footnote{the force between two mass points acts along the line between them}.

By defining the centre of mass $c$ as a collection of mass points and considering that the motion of the centre 
of mass and the motion of the mass points about their centre of mass are uncoupled, the fundamental equation of
attitude dynamics is given as:

\begin{equation}
\dot{H}_{\mathcal{I}}^{c} \equiv L_I^c
\label{eq:momentum-dot-fundamental}
\end{equation}

By defining a rigid body, a body frame where the distance of any point mass to the centre of the frame, $r^{ic}$
remains constant in time regardless of external forces or moments exerted on it, the moment of inertia (MOI) can be 
defined as:
\begin{equation}
J^c_{\mathcal{B}} = - \sum_{i = 1}^n m_i [r^{ic}_{\mathcal{B}} \times]^2
\label{eq:moi}
\end{equation}

and can be used to describe the angular momentum in a body frame:
\begin{equation}
H_{\mathcal{B}}^{c} = J^c_{\mathcal{B}} \omega^{\mathcal{B}\mathcal{I}}_{\mathcal{B}}
\label{eq:angular-momentum-moi}
\end{equation}

External torques $L_\mathcal{B}^c$ are often more easily computed in the body frame, so \eqref{eq:momentum-dot-fundamental} can be replaced by:
\begin{equation}
\dot{H}_{\mathcal{B}}^{c} =  L_\mathcal{B}^c - \omega^{\mathcal{B}\mathcal{I}}_{\mathcal{B}} \times H_{\mathcal{B}}^{c}
\label{eq:momentum-dot-body}
\end{equation}

%%%%%%%%% Orbital motion

\section{Orbit Dynamics}
\label{section:orbit-dynamics}
Explaining the motion of celestial bodies has been subject of study for many centuries. One of the biggest breakthroughs was accomplished by 
Johannes Kepler, to be later refined by Isaac Newton. Kepler proposed three laws of planetary motion in the $17^{th}$ century based on observation by Tycho Brahe.

\par Newton's law of gravitation states that any two bodies attract each other with a force proportional to the product of their masses and inversely proportional to the square of the distance between them \cite{wertzSpaceMissionAnalysis1999}. % Wert and Larzon 1999
The equation for the force caused by gravity is 
\begin{equation}
    F_G = - \frac{G M m}{r^3} \mathbf{r} = - \frac{\mu m}{r^3} \mathbf{r}
    \label{eq:orb1}
\end{equation}
where $G$ is the universal constant of gravitation, $M$ is the mass of the Earth, $m$ is the mass of the satellite, $\mathbf{r}$ is the vector between them (where $r = ||\mathbf{r} ||$) and $\mu$ is the Earth's gravitational constant.
\par Combining Newton's second law with his law of gravitation, it gives the equation for the acceleration of the satellite, such as
\begin{equation}
    \ddot{\mathbf{r}} + (\mu r^{-3}) \mathbf{r} = \mathbf{0}
    \label{eq:orb2}
\end{equation}
The previous equation is the two-body equation of motion and gives the satellite's orbit. 
It assumes that gravity is the only force applied and that the Earth is spherically symmetric, as well as that the Earth and the satellite are the only two bodies in the system and can be treated as a point-mass.
These concerns will be considered in Section \ref{section:env-model}, whilst \eqref{eq:orb2} can be modified to account for perturbations, such as 
\begin{equation}
    \ddot{\mathbf{r}} =  -(\mu r^{-3}) \mathbf{r} + \mathbf{a}_p
    \label{eq:orb3}
\end{equation}

\par The motion of a satellite could therefore be characterized by a fixed elliptical orbit in space with the Earth being at one of the foci. 
This orbit can be specified by the six elements of the satellite's position and velocity vectors at a specific epoch.
However, an alternative representation that uses the six Keplerian elements was developed to provide a physical characterization of the orbit \cite{noureldinFundamentalsInertialNavigation2013} %Fundamentals of Inertual Navigation
and are the following:
\begin{itemize}
    \item \textbf{Semimajor}: Longest dimension of the orbit;
    \item \textbf{Eccentricity}: Ovalness of the ellipse;
    \item \textbf{Inclination}: Angle of the orbital plane relative to the Earth's equatorial plane;
    \item \textbf{Right ascension of the ascending node (RAAN)}: Angle in the equatorial plane between the ascending node of the satellite's orbit and the vertical equinox;
    \item \textbf{Argument of perigee or periapsis}: Orientation of the elipse in orbital plane;
    \item \textbf{True anomaly}: Position of the satellite in the orbit.
\end{itemize}

\section{Spacecraft perturbations}
\label{section:env-model}
Spacecraft perturbations can be divided into perturbation forces, which affects accelerations, and perturbation torques.
The forces have an impact in the orbital motion of the satellite and deviate it from the Keplerial moment, contributing to the $\mathbf{a}_p$ in \eqref{eq:orb3}.
On the other hand, the torques cause changes in the angular rate of the satellite, depicted in the $L_\mathcal{B}^c$ in \eqref{eq:momentum-dot-body}. This section models these perturbations, based on \cite{markleyFundamentalsSpacecraftAttitude2014} \cite{wertzSpaceMissionAnalysis1999} \cite{computersciencescorporationSpacecraftAttitudeDetermination1978}.
% Wertz 1999, 1978 - Hughes 2004, crassidis 2014

\subsection{Perturbative forces}
Perturbative forces are connected to \eqref{eq:orb3} and impose unwanted accelerations that alter the orbital motion of the satellite. 
For satellite in LEO, the most significant forces are aerodynamic drag, non-spherical Earth perturbations and the gravitational influence from the Sun and Moon. 
These forces are depicted through
\begin{equation}
    \mathbf{a}_p = \mathbf{a}_d + \mathbf{a}_{sph} + \mathbf{a}_{sun} + \mathbf{a}_{moon}
\end{equation}
\vspace{0.4cm}
\par \textbf{Aerodynamic drag}
\par The atmospheric drag arises from the friction between the atmosphere and the satellite, causing a hinder force of the latter.
This perturbation has the biggest impact for low-Earth orbits since the density of the atmosphere decreases exponentially with an increase in altitude.
\par In order to model the drag (and torque), the satellite is modelled as a collection of $N$ flat plates of area $S_i$ and outward normal unit normal vector 
$n_{\mathcal{B}}^i$ expressed in the satellite body fixed coordinate system. 
The drag and torque are dependent on velocity of the satellite concerning the atmosphere in the inertial frame $\mathbf{v_{rel \mathcal{I}}}$ since the atmosphere is not 
stationary in that frame.
\par The inclination of the $i$th plate in relation to the relative velocity is given as 
\begin{equation}
    \cos \theta^i_d = (A^T n_{\mathcal{B}}^i )^T (\frac{ \mathbf{v_{rel \mathcal{I}}} }{ \mathbf{v_{rel \mathcal{I}}} })
\end{equation}
where $A$ is the attitude matrix that rotates the environment to the body frame.


The drag force is modelled through the following fluid mechanics equation
\begin{equation}
    \mathbf{a}_d = -\frac{1}{2} \rho || \mathbf{v_{rel \mathcal{I}}}||  \mathbf{v_{rel \mathcal{B}}} C_d \sum^N_{i=1} S_i \cos \theta^i_d
\end{equation}
where $\rho$ is the air density and $C_d$ is the drag coefficient of the satellite. Only plates with $\cos \theta^i_d > 0 $ are included in the summation.
\par There are various models to determine the air density but the simplest one in the Exponential decaying model atmosphere, given by 
\begin{equation}
    \rho = \rho_0 \exp (-\frac{h-h_0}{H})
\end{equation}
where $\rho_0$ and $h_0$ are reference density and height respectively, $h$ is the height and $H$ is the scalar height, which is established according to the height.
This model is fully static (independent of time) and doesn't take into account temperature changes. % meh 
\vspace{0.4cm}
\par \textbf{Non-spherical Earth perturbation}
\par The Earth is not a perfect sphere and mass is not distributed uniformly throughout it, ergo the gravity field will also be nonuniform. 
In order to translate the point-mass approach of Newton's law of universal gravitation to a more realistic approach models were created based on the 
spherical harmonic expansion, such as the Earth Gravitation Model 2008 (EGM2008). 

\vspace{0.4cm}
\par \textbf{Sun and Moon influence}
\par Even though the Earth is the celestial body that the satellite orbits, every body with a mass has a gravitational field.
Namely, in the Moon and Sun gravitational forces are classified as perturbations, though they are not subject to heterogeneous constitution since they are too distant to be noticeable.
They can be modelled with Newton's universal gravitational law, such as 
\begin{equation}
    \mathbf{a}_{sun} = \frac{\mu_{sun} M_{sun}}{ ||\mathbf{r}_{sun}||^2} \frac{\mathbf{r}_{sun}}{|\mathbf{r}_{sun}|}
\end{equation}
\begin{equation}
    \mathbf{a}_{moon} = \frac{\mu_{moon} M_{moon}}{ ||\mathbf{r}_{moon}||^2} \frac{\mathbf{r}_{moon}}{|\mathbf{r}_{moon}|}
\end{equation}
where $\mu$ are the gravitational constants of the celestial bodies, $M$ the mass and $\mathbf{r}$ the distance between the centre of the body and the satellite point of mass.



%\par \textcolor{red}{Verify if solar pressure}


\subsection{Perturbative torques}
Perturbative torques can be divided into internal or external. Internal perturbative torques include deployment of payload elements, propellant slosh and reaction wheel imbalances. 
These are often negligible in comparison to external torques and are not considered in this work. The external torques result from the interaction with the environment and include aerodynamic, gravitational and magnetic torque.
These torques are depicted through
\begin{equation}
    L_p = L_p^c + L_d + L_m
\end{equation}
\vspace{0.4cm}
\par \textbf{Gravitational torque}
\par The non-symmetry of a rigid body leads to nonuniform gravitational field over it, causing the appearance of a torque about its centre of mass. 
This torque can be computed as 
\begin{equation}
    L_g^c = 3(\frac{\mu}{r^3}) \mathbf{o} \times J^c \cdot \mathbf{o}
\end{equation}
where $\mathbf{r}$ is the distance between the Earth and the satellite (where $r = ||\mathbf{r} ||$), $\mathbf{o}$ is the representation of a nadir-pointing unit vector in the body frame and
$J^c$ is the moment of inertia tensor about the centre of mass. 

\par \textbf{Aerodynamic torque}
%Aerodynamic torques are created by the residual atmosphere and other particules present in space.
% og 257 huges

\par The aerodynamic torque translates how the aerodynamic forces instigate angular motion. 
\par  The inclination of the $i$th plate to the relative velocity is given by
\begin{equation}
    \cos \theta^i_d = \frac{n_{\mathcal{B}}^i \cdot \mathbf{v_{rel \mathcal{B}}}}{|| \mathbf{v_{rel \mathcal{I}}} || }
\end{equation}

The aerodynamic force on the $i$th plane is
\begin{equation}
    \mathbf{a}_d^i  = -\frac{1}{2} \rho || \mathbf{v_{rel \mathcal{I}}}||  \mathbf{v_{rel \mathcal{B}}} C_d \text{max}(\cos \theta^i_d, 0)
\end{equation}
The aerodynamic torque on the satellite is then
\begin{equation}
    L_d = \sum^N_{i=1} \mathbf{r}^i \times \mathbf{a}_d^i 
\end{equation}
where $\mathbf{r}^i $ is the vector from the satellite centre of mass to the centre of pressure of the $i$th plate.
\vspace{0.4cm}
\par \textbf{Magnetic torque}
\par When a satellite is immersed in a magnetic field $\mathbf{B}$ and has a magnetic dipole $\textbf{m}$, a torque is created. 
Permanent magnets and current loop contribute to the magnetic dipole, whilst it is possible to balance these elements in order to reduce their influence.
The existence of a nonzero magnetic field in the neighbourhood of the Earth is what allows for magnetic torque control. 
This phenomenon follows
\begin{equation}
    L_m = \mathbf{m} \times \mathbf{B}
\end{equation} 

%\par \textcolor{red}{Verify if solar pressure is implemented}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{comment}
\section{Theoretical Model 1}
\label{section:theory1}

The research should be supported with a comprehensive list of references.
These should appear whenever necessary, in the limit, from the first to the last chapter.

A reference can be cited in any of the following ways:
%
\begin{itemize}
  \item Citation mode \#1 - \quad \cite{jameson:adjointns}
  \item Citation mode \#2 - \quad \citet{jameson:adjointns}
  \item Citation mode \#3 - \quad \citep{jameson:adjointns}
  \item Citation mode \#4 - \quad \citet*{jameson:adjointns}
  \item Citation mode \#5 - \quad \citep*{jameson:adjointns}
  \item Citation mode \#6 - \quad \citealt{jameson:adjointns}
  \item Citation mode \#7 - \quad \citealp{jameson:adjointns}
  \item Citation mode \#8 - \quad \citeauthor{jameson:adjointns}
  \item Citation mode \#9 - \quad \citeyear{jameson:adjointns}
  \item Citation mode \#10 - \quad \citeyearpar{jameson:adjointns}
\end{itemize}
%
Several citations can be made simultaneously as \citep{nocedal:opt,marta:ijcfd}. \\

This is often the default bibliography style adopted (numbers following the citation order), accordi
ng to the options:\\
{\tt \textbackslash usepackage\{natbib\}} in file {\tt Thesis\_Preamble.tex},\\
{\tt \textbackslash bibliographystyle\{abbrvnat\}} in file {\tt Thesis.tex}.\\

Notice however that this style can be changed from numerical citation order to authors' last name wi
th the options: \\
{\tt \textbackslash usepackage[numbers]\{natbib\}} in file {\tt Thesis\_Preamble.tex},\\
{\tt \textbackslash bibliographystyle\{abbrvunsrtnat\}} in file {\tt Thesis.tex}. \\

Multiple citations are compressed when using the {\tt sort\&compress} option when loading the {\tt n
atbib} package as {\tt \textbackslash usepackage[numbers,sort\&compress]\{natbib\}} in file {\tt The
sis\_Preamble.tex}, resulting in citations like \citep{marta:ijcfd1,marta:ijcfd2,marta:ijcfd3,marta:
ijcfd4}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Theoretical Model 2}
\label{section:theory2}

Other models...
\paragraph{Rotations and Euler Theorem}


\subsection{Direction Cosine Matrix}
\label{subsection:dcm}

\subsection{Euler Axis/Angle Representation}
\label{subsection:euler-axis}


\subsection{Rotation Vector Representation}
\label{subsection:rotation-vector}
\subsection{Quaternion Representation}
\label{subsection:rotation-representation}
\subsection{Euler Angles Representation}
\label{subsection:euler-angles}
\subsection{Rodrigues Parameters Representation}
\label{subsection:rotation-representation}



\subsection{Rotation Vector Representation}
\label{subsection:rotation-vector}
In order to have a easier representation for analysis, the Euler axis and angle can be combined
into a three-component rotation vector:
\begin{equation}
    \vartheta = \vartheta \mathbf{e}
\label{eq:rot-vec}
\end{equation}

which  is used to describe small rotations.

FIX ME - nu should be bold

MAYBE DELETE SECTION? put it above


\end{comment}

