{"rule":"PASSIVE_VOICE","sentence":"^\\QControl algorithms are researched in to Actuators.\\E$"}
{"rule":"PASSIVE_VOICE","sentence":"^\\QIn this chapter, the available actuators are presented, as well as reasons for their choice and model.\\E$"}
{"rule":"DASH_RULE","sentence":"^\\QReaction wheels can be either used to generate torque through the variation of the motor's angular velocity or ran at a constant high speed, generating a gyro stiffness across the wheel's axis - called the momentum wheel.\\E$"}
