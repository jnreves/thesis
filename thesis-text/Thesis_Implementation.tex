\chapter{Implementation}
\label{chapter:implementation}
The simulation environment was developed in Matlab 2020b and Simulink and can be seen in Fig. \ref{fig:imp-full-model}. 
The model is divided into intuitive sections, similarly to this chapter. From left to right, the outputs of a block 
are used as the inputs to the following block. Each section has the modelling blocks, as well as separated data treatment blocks. 
This is done to expedite the swap of each separate block and reduce the amount of \textit{Scopes}, \textit{To Workspace} and other computational heavy blocks. 
Further data processing is done in separate scripts. 
\par Individual models have been developed for pre-computation of the satellite, Sun and GS position and magnetic field. 
Although the simulation in usually ran with a $0.025$ time-step, the previous data is calculated with a $0.5$ second interval since it is enough to encompass the slow variations. 
A setup script also allows for quick changes into the simulation environment, such as sensor/actuators parameters, simulation time and initialization parameters for the real and estimate quaternion.
A more detailed look into each of the subsystems is provided in Appendix \ref{chapter:annex-sim-models}, that follows the same order as this section. 

% blocos facilmente trocaveis, scripts de pre processamento, tempo de simulacao, delta t de simulacao, beaseada em que, subdivisao de cada seccao
% mais programas feitos para visualizacao de resultados
\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.9\textwidth]{Figures/FullModel.png}
    \caption[Full simulation model.]{Full simulation model.}
    \label{fig:imp-full-model}
\end{figure}

% environment
\section{Environment}
\label{sec:imp-env}
\par This section is responsible for the orbital motion, disturbances calculation and rotational dynamics.

\par The position motion of the satellite, Earth, Sun and Moon are pre-calculated in order to reduce computation time. The \textit{planetEphemeris} function from the Aerospace toolbox gives
the position and velocity of the Earth and Moon. These values are then propagated through Runge-Kutta $4^th$ order integration and the Sun position is calculated. The celestial bodies
positions are used to calculated and propagate the satellite's position with resort to Runge-Kutta $8^th$ order integration. 
The magnetic field true model used is $12^{th}$ degree WMM2020 model, provided by Matlab.
\par The keplerian parameters for the orbit follow the Sun synchronous orbit and can be seen in Tab. \ref{tab:imp-iss-kepler}. They were chosen to maximize the time that the satellite has LOS to the GS. 
%The keplerian parameters for the orbit follow the 
%International Space Station values, since the satellite will be launched from it and can be seen in table \ref{tab:imp-iss-kepler}. 
 
\begin{table}[H]
    \centering
    \begin{tabular}{|l|l|}
        \hline
        Semi-Major axis & $6871 km$ \\ \hline
        Eccentricity  & $ 0 $ \\ \hline
        Inclination  & $ 97.86^{\circ} $ \\ \hline
        Right Ascending Node  & $ 22.5^{\circ} $ \\ \hline
    \end{tabular}
    \caption[Kepler parameters for Sun synchronous orbit.]{Kepler parameters for Sun synchronous orbit.}
\label{tab:imp-iss-kepler}
\end{table}

\par The GS location is defined as has having coordinates $38^{\circ}N$ $150^{\circ}W$. 
Aiming to use them in a simulation environment, the coordinates are converted to ECI and propagated.

%In order to be used in the simulation environment, the
%coordinates are converted to ECI and propagated. 
% \par \textcolor{red}{disturbances, according to background}
\par The rotational dynamics of the satellite follow \eqref{eq:momentum-dot-body}, where the external torques are the sum of the disturbances.

% sensors
\section{Sensors}
\label{sec:imp-sensors}
\par Since this project's goal is to provide an updated and improved approach to the ISTSat-1 ADCS's, the sensors same sensors are used. 
The simulation environment is designed to allow for testing of different sensors through a configuration file for further optimization and experimentation.
\par The satellite is fitted with and Inertial Measurement Unit (IMU), the TDK InvenSense MPU-9250, that integrates a gyroscope with the parameters described in Tab. \ref{tab:imp-gyro}.
\begin{table}[H]
    \centering
    \begin{tabular}{|l|l|}
        \hline
        ADC & $16$ \textit{bits}             \\ \hline
        Sampling frequency  & $4-8000 Hz$  \\ \hline
        Low-pass filter           & $5-250 Hz$ \\ \hline
        Noise RMS    & $0.01^{\circ} /s$           \\ \hline
    \end{tabular}
    \caption[Gyroscope parameters.]{Gyroscope parameters - Appendix \ref{section:annex-gyroscope}.}
\label{tab:imp-gyro}
\end{table}
This leads to the a Gaussian white-noise $\boldsymbol{\eta}_{arw}$ with the following properties
\begin{equation}
    E [ \boldsymbol{\eta}_{arw} \boldsymbol{\eta}_{arw}^T ] = \sigma^2_{arw} I_3
\end{equation}
where $\sigma_{arw}$ is the Allen Variation. It is assumed that the alignments and scaling factors have been calculated through ground tests. 

\par The Sun sensor used is the Solar MEMS ISS-D25, one per face of the satellite. Some of its most relevant parameters are shown in table \ref{tab:imp-sun-sensor}.
It is modelled according to \eqref{eq:sun-sensor-model}, where the values for the noise attitude matrices are derived from the datasheet (Appendix \ref{section:annex-sun-sensor}) so that they have the same statistical properties of \textit{Angle x} and \textit{Angle y}.

\begin{table}[H]
    \centering
    \begin{tabular}{|l|l|}
        \hline
        Field of View (FOV) & $50^\circ$             \\ \hline
        Accuracy (3 $\sigma$)  & \textless $0,3^\circ$  \\ \hline
        Precision           & \textless $0,04^\circ$ \\ \hline
        Angle resolution    & $0,001$           \\ \hline
    \end{tabular}
    \caption[Sun sensor parameters.]{Sun sensor parameters - Appendix \ref{section:annex-sun-sensor}.}
\label{tab:imp-sun-sensor}
\end{table}

\par Two magnetometers are available, the already mentioned MPU-9250 and the Honeywell HMC5983, in Appendix \ref{section:annex-mag}. The latter is used due to its superior performance, 
whilst the former one can be used as a backup in future implementations. The model follows \eqref{eq:magnetometer}, where the noise component follows
\begin{equation}
    E [ \boldsymbol{\eta}_{mag} \boldsymbol{\eta}_{mag}^T ] = \sigma_{mag}^2 I_3
\end{equation}
which has standard deviation $\sigma_{mag}$ specified as $200 nT$. 

\par The beacon noise description follows the statistical characteristics from \cite{nguyenDevelopmentPointingAcquisition2015}, that leads to a mean attitude 
error of $29.7 \mu rad$. The GPS is modelled as having an uncertainty of $10 m$ around the true satellite position, according to \cite{noureldinFundamentalsInertialNavigation2013}.

% estimation
\section{Estimation}
\label{sec:imp-est}
The Earth magnetic field is estimated through the $5^{th}$ degree WMM2020, self-developed.
The Sun vector true model and estimation use the same algorithm since it is suitable for onboard use.
\par The GS vector is calculated from the estimated position from the GPS and the known position of the GS. 
When the satellite is above the GS, the mission requires the payload to be pointed at it; otherwise, the payload should be pointed at nadir. 
A smooth transition was implemented between both cases, as can be seen in Fig. \ref{fig:3d-orbit}.

\par In order to determine the desired quaternion and angular velocity a rotation matrix was calculated by adding constraints to the Rodrigues's rotation formula.
This is done by restricting the desired attitude to have the $y$ axis aligned with the orbit's axis of rotation and $-z$ axis aligned with the GS vector, whilst the GS is starting to be visible.
The quaternion is recovered from the attitude matrix and the angular velocity is determined through derivation. 
To extract the quaternion from the attitude matrix the Modified Shepperd's algorithm is used \cite{markleyUnitQuaternionRotation2008}. 
This algorithm doesn't recover a continuos quaternion and may lead to issues in the calculation of the desired frame. 
To avoid it, a selection of only one of the estimates was selected which leads to a non-optimal estimate.
The alternatives found \cite{wuOptimalContinuousUnit2019} require a high computational effort, that is not suitable for onboard use.



\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{Figures/3d-orbit.jpeg}
    \caption[Orbit of the satellite.]{Orbit of the satellite in relation to the Ground Station.}
    \label{fig:3d-orbit}
\end{figure}

\par For the MEKF, the initial parameters are shown in table \ref{tab:imp-mekf-init}. The quaternion initial estimation is varied to test for different situation. 
\begin{table}[H]
    \centering
    \begin{tabular}{|l|l|}
        \hline
       \multirow{3}{*}{$\mathbf{q}_{0}$} & $[0.2608, -0.4776, 0.3602, 0.7577]$ ($5.3^{\circ}$ of initial error) \\ \cline{2-2}
        & $[0.2951, -0.4104, 0.3900, 0.7697]$ ($9.8^{\circ}$ of initial error) \\ \cline{2-2}
        & value from TRIAD \\ \hline
        $\mathbf{b}_0 $                               & $[0, 0, 0]  $                   \\ \hline
        $P_0 $                                        & $0.000001 \times I_{6 \times 6}$  \\ \hline
        propagation frequency                       & $40 Hz$                          \\ \hline
        update frequency                            & $10 Hz$                           \\ \hline
        \end{tabular}
    \caption[MEKF filter initialization parameters.]{MEKF filter initialization parameters.}
\label{tab:imp-mekf-init}
\end{table}

Depending on the availability of the sensors the matrix $R_k$ changes, such that
\begin{table}[H]
    \centering
    \begin{tabular}{|l|l|}
        \hline
        \rule{0pt}{25pt}Not in eclipse              & $\begin{bmatrix}R_{mag} &  0\\ 0 & R_{sun}\end{bmatrix}$            \\ \hline
        Eclipse, beacon not in LOS  & $R_{mag}$          \\ \hline
        \rule{0pt}{25pt}Eclipse and beacon in LOS   & $\begin{bmatrix}R_{mag} &  0\\ 0 & R_{beacon}\end{bmatrix}$        \\ \hline
    \end{tabular}
    \caption[MEKF measurement covariance matrix.]{MEKF measurement covariance matrix.}
\label{tab:imp-mekf-measurement-matrix}
\end{table}
The values for each measurement covariance matrix were initially set to the sensors specifications and, by trial and error, were tuned to 
\begin{table}[H]
    \centering
    \begin{tabular}{|l|l|}
        \hline
        $R_{mag}$             & $0.0055 \times I_{3} $         \\ \hline
        $R_{sun}$   & $0.01 \times diag(0.16, 0.16, 0.45)  $        \\ \hline
        $R_{beacon}$    & $10^{(-5)} \times diag(0.1, 0.1, 0.3)$       \\ \hline
    \end{tabular}
    \caption[Measurement covariance matrix values.]{Measurement covariance matrix values.}
\label{tab:imp-measurement-matrix}
\end{table}

\section{Control and Actuators}
\label{sec:imp-cont-act}
The reaction wheels used are the Cubespace Cubewheel Small, one per axis. 
% why
The parameters for the reaction wheel can be seen in Tab. \ref{tab:imp-rw}.
\begin{table}[H]
    \centering
    \begin{tabular}{|l|l|}
        \hline
        Max RPM & $8000$             \\ \hline
        Max. Torque  & $0.23 mT$  \\ \hline
        Max. stored Momentum           & $1.77 mT/s$ \\ \hline
        Inertia   & $3.12 \times 10^{-6}$           \\ \hline
    \end{tabular}
    \caption[Reaction wheel parameters.]{Reaction wheel parameters - Appendix \ref{section:annex-rw}.}
\label{tab:imp-rw}
\end{table}

\par The first-order system that models the reaction wheel is tuned to have the response according to the manufacturer's parameters, with saturation
for the stored momentum/RPM (revolutions per minute). The output torque is controlled by a reference torque provided by the controller. The step response for the maximum torque in the time domain can be seen in Fig. \ref{fig:step-response}.
It can be seen that the output torque is zero when the reaction wheel reaches its maximum stored momentum.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Figures/step-response.jpeg}
    \caption[Stored momentum and torque response for a step of the maximum torque.]{Stored momentum and torque response for a step of the maximum torque.}
    \label{fig:step-response}
\end{figure}

\par The second-order system was tuned by analyzing the manufacturer's properties and matching it to the general flywheel equations \cite{gamboaFlywheelEnergyStorage2008} 
\begin{subequations}
    \begin{equation}
        L_{fw} = J_{fw} \dot{\boldsymbol{\omega}}_{fw} = k m r^2 \dot{\boldsymbol{\omega}}_{fw}
    \end{equation}
    \begin{equation}
        H_{fw} = \frac{1}{2} J_{fw} \boldsymbol{\omega}_{fw}^2
    \end{equation}
\end{subequations}
where $L_{fw}$ is the produced torque and $H_{fw}$ the momentum stored by the flywheel, $J_{fw}$ is the moment of inertia of the wheel, $\boldsymbol{\omega}_{fw}$ the angular velocity of the wheel, $m$ the mass, $r$ the radius and $k$ the inertia constant \footnote{the parameters $m$, $r$ and $k$ where the main parameters used to tune the model and were extrapolated from the datasheet}.
\par The satellite will be powered by the EuduroSat solar panels that have an optional feature which includes a printed coil in the back of the
panel to work as magnetorquers. Only 3 solar panels are fitted with the optional coil, one per axis. These magnetorquers provide a magnetic dipole
of $131 mAm^2$ and are controlled through PWM.
\par The sliding controller uses the parameters in Tab. \ref{tab:imp-sliding-control}.
\begin{table}[H]
    \centering
    \begin{tabular}{|l|l|}
        \hline
        Frequency & $1 Hz$ \\ \hline
        K & $0.026$ \\ \hline
        $\epsilon$ & $8 \times 10^{-3}$\\ \hline
        G & $5 \times 10^{-4} I_3$\\ \hline
        $\hat{J}$ & $J_{sat}$\\ \hline
    \end{tabular}
    \caption[Sliding Controller parameters.]{Sliding Controller parameters.}
\label{tab:imp-sliding-control}
\end{table}

\par The GFTAC controller uses the parameters in Tab. \ref{tab:imp-GFTAC-control}.
\begin{table}[H]
    \centering
    \begin{tabular}{|l|l|}
        \hline
        Frequency & $1 Hz$ \\ \hline
        $k_1$ & $8.5 \times 10^{-4}$ \\ \hline
        $k_2$ & $5 \times 10^{-4} $ \\ \hline    
        $\alpha_1$ & $0.6$ \\ \hline
        $\alpha_2$ & $0.75$ \\ \hline
        $\sigma$ & $0.4$ \\ \hline     
    \end{tabular}
    \caption[Sliding Controller parameters.]{Sliding Controller parameters.}
\label{tab:imp-GFTAC-control}
\end{table}

The PD controller uses the parameters in Tab. \ref{tab:imp-pd-control}.
\begin{table}[H]
    \centering
    \begin{tabular}{|l|l|}
        \hline
        $k_d$ & $2.3 \times 10^{-5}$ \\ \hline
        $k_p$ & $8.4 \times 10^{-5} $ \\ \hline     
    \end{tabular}
    \caption[PD Controller parameters.]{PD Controller parameters.}
\label{tab:imp-pd-control}
\end{table}


