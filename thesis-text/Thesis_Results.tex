\chapter{Testing}
\label{chapter:results}

\par In order to select the appropriate combination of sensors, actuators and algorithms, different scenarios must be run. 
The simulation uses the parameters discussed in Chapter \ref{chapter:implementation}, with different angular rate and quaternion initializations.
% Most of the sensors and the controllers to be used are already defined since they have been extensively studied already in other works from the ISTSat-1 team \cite{nevesControlAlgorithmISTsat12019}.  
The detumbling controller can be tested separately since is the first maneuver to be performed and it does not use bias estimation to correct the gyroscope measurements and the reaction wheels are not used.
\par Since the beacon is not a regular sensor used in a CubeSats, a verification of its usability must be done before integrating into the mission implementation.
The MEKF is used to compare the performance with and without the beacon due to the convenience of tuning it for both options.

\par A comparison between the different methods must be done in order to verify if the computational complexity that the filters add does in fact lead to better attitude and bias estimation. 
The estimation is evaluated separately with identical simulation environments for the different methods. 
The different control algorithms must also be tested without the influence of the estimators. The convergence time, the angular velocity and pointing error are evaluated.
After the preferable method for estimation and control are chosen a simulation is made to verify wether the pointing requirements are met. 



% Detumbling

\section{Detumbling}
In order to test the detumbling maneuver a simulation is started with a $10^{\circ} / s$ angular rate, the expected tumbling rate after leaving the launcher. The results are presented in Fig. \ref{fig:res-det}, detailing the angular velocity in the different axes. 
After $1.3474$ orbits the angular relate crosses the detumbled threshold of $5^{\circ} / s$ and, with the use of the controllers, is slowed down further. 
Although the satellite is slowed down to the required threshold, it does so in more than one orbit. 
%In order to comply with the latter requirement more solar panels could be fitted with the magnetorquers.

\begin{figure}[!htb]
    \centering
    \includegraphics[width=\textwidth]{Figures/detumbling.png}
    \caption[Detumbling controller.]{Detumbling controller with a $10^{\circ} / s$ angular rate.}
    \label{fig:res-det}
\end{figure}


% beacon vs no beacon

\section{Beacon effectiveness verification}
\label{section:res-beacon}
In order to verify if the estimation accuracy is improved with the use of the beacon, a comparison of the two cases (with and without the beacon) is done using the MEKF.
Two distinct scenarios were used for the initial error estimation, with the results presented in Tab. \ref{tab:res-mekf-w-wo} and in Fig. \ref{fig:res-beacon}.
It is assumed that when the beacon is in the LOS it is also within the FOV of the telescope.
\par The addition of the beacon decreases the attitude estimation error drastically, especially when it is only considered the period where the GS is in the LOS of the satellite and can be used to improve estimation. 
A decline in the error estimation can be observed in Fig. \ref{fig:res-beacon} as soon as the measurement is available.
The estimation performance decreases in eclipse due to the unavailability of the Sun sensors and without the integration of the beacon in the filter it never meets the required accuracy of $0.1^{\circ}$.
The bias estimation without the use of the beacon results in a higher accuracy and a smoother conversion to the real value. 
\par The initial estimate for the quaternion has a significant effect in the attitude estimation error and results in less accurate estimates in LOS due to a longer convergence period. It has little effect on the bias estimation.
If the filter is run long enough, both of the scenarios will perform similarly.
\par It can be concluded that the beacon has a positive impact in the estimation and should be used in the attitude estimation. However, the availability is limited due to only being available in eclipse and in LOS.

\begin{figure}[!htb]
    \centering
    \includegraphics[width=\textwidth]{Figures/beaconVSnoBeaconComplete.png}
    \caption[Attitude and bias estimation with and without beacon.]{Attitude (top) and bias (bottom) estimation with (left) and without (right) beacon.}
    \label{fig:res-beacon}
\end{figure}

\begin{table}[!htb]
    \centering
    \begin{tabular}{|c|cc|cc|}
        \hline
        \textbf{Mean} & \multicolumn{2}{c|}{\textbf{With beacon}} & \multicolumn{2}{c|}{\textbf{Without beacon}} \\ \hline
        \textbf{Initial error ($^{\circ}$)} & \multicolumn{1}{c|}{$5.3$} & $9.8$ & \multicolumn{1}{c|}{$5.3$} & $9.8$ \\ \hline
        \textbf{Attitude estimation error ($^{\circ}$)} & \multicolumn{1}{c|}{$0.7128$} & $0.7974$ & \multicolumn{1}{c|}{$1.3985$} & $1.1611$ \\ \hline
        \textbf{Attitude estimation error in LOS ($^{\circ}$)} & \multicolumn{1}{c|}{$0.1884$} & $0.4429$ & \multicolumn{1}{c|}{$1.4710$} & $1.7852$ \\ \hline
        \textbf{Bias estimation error ($^{\circ} / s $)} & \multicolumn{1}{c|}{$3 \times 10^{-4}$} & $2.6433 \times 10^{-4}$ & \multicolumn{1}{c|}{$2.9433 \times 10^{-3}$} & $2.6012 \times 10^{-4}$ \\ \hline
    \end{tabular}
    \caption[Attitude and bias estimation performace with and without beacon.]{Attitude and bias estimation performace with and without beacon.}
    \label{tab:res-mekf-w-wo}
\end{table}



% General estimation

\section{Comparison of estimation methods}
\label{section:res-estimation}
In order to compare the attitude estimation methods scenario is simulated using the same initial values for quaternion and velocity. For the MEKF and CF, the initial quaternion estimate used illustrates $5.3^{\circ}$ of initial error. 
The results of the simulation can be seen Fig. \ref{fig:res-est} and Tab. \ref{fig:res-est}. The TRIAD method doesn't estimate the bias.
As done in Section \ref{section:res-beacon}, it is assumed that when the beacon is in the LOS it is also within the FOV of the telescope. Without it, the CF wouldn't be comparable since it has an attitude error greater than the FOV of the telescope in eclipse. 
\par A comparison of the advantages and disadvantages of the estimation methods can be seen in Tab. \ref{tab:res-estimation-adv}.
Although the CF has a high mean attitude estimation error, it mainly results from the eclipse phase. 
When only the magnetometer and gyroscope are available the attitude error increases drastically. 
Its accuracy is not high enough in the mission area since it doesn't reach a $0.1^{\circ}$ accuracy consistently. The bias estimate is also far from the real value.
\par The TRIAD method has a consistent estimate, that is only slightly improved with the addition of the beacon. Since it only uses 2 measurements, it doesn't provide an estimate in eclipse.
As it is a deterministic method it doesn't need to converge, which is useful to provide an initial estimate for other methods considering it has a low computational demand.
\par The MEKF demonstrate an acceptable accuracy when the beacon is used, although it is not throughout the full duration of the LOS. This is due to the fact that it has a convergence period when the beacon available.
In eclipse the estimation is also deteriorated, but not as much as the CF. 
This results in a better bias estimation, that is used to correct the angular velocity measurements in the propagation phase.  
Tests were done with only the propagation step in eclipse, but it was found that the update with only one measurement was still beneficial for the attitude estimation.
Hence, his method is the one chosen for the mission implementation.

\begin{table}[!htb]
    \centering
    \begin{tabular}{|c|c|c|c|}
    \hline
    \textbf{Mean} & \textbf{MEKF} & \textbf{TRIAD} & \textbf{CF} \\ \hline
    \textbf{Attitude estimation error ($^{\circ}$)} & $0.9429$ & $0.6775$ & $25.1388$ \\ \hline
    \textbf{Attitude estimation error in LOS ($^{\circ}$)} & $0.3780$ & $0.5224$ & $2.3361$ \\ \hline
    \textbf{Bias estimation error ($^{\circ} / s $)} & $2.7 \times 10^{-4}$ & $N/A$ & $0.4529$ \\ \hline
    \end{tabular}
    \caption[Comparison of the attitude estimation methods.]{Comparison of the attitude estimation methods.}
    \label{tab:res-est}
\end{table}

\begin{figure}[!htb]
    \centering
    \includegraphics[width=\textwidth]{Figures/estAllComplete.png}
    \caption[Comparison of the attitude estimation methods.]{Comparison of the attitude estimation methods.}
    \label{fig:res-est}
\end{figure}

\begin{table}[!htb]
    \centering
    \begin{tabular}{|l|l|l|}
    \hline
     & Advantages & Disadvantages \\ \hline
    TRIAD & \begin{tabular}[c]{@{}l@{}}- Consistent and low computational effort\\ - Provides an estimation with only two\\ samples, without the need to converge\end{tabular} & \begin{tabular}[c]{@{}l@{}}- Does not use past information to refine estimation\\ - Does not estimate gyroscope bias\\ - Is limited by the sensors' performance\end{tabular} \\ \hline
    CF controller & \begin{tabular}[c]{@{}l@{}}- Low computational effort (in comparison\\ to state estimation algorithms)\\ - Good estimation when the beacon is\\ available\end{tabular} & \begin{tabular}[c]{@{}l@{}}- Bad estimation in eclipse\\ - Accuracy bellow expectations\end{tabular} \\ \hline
    MEKF & \begin{tabular}[c]{@{}l@{}}- Very good attitude estimation after the\\ filter converges, especially\\ with the incorporation of the beacon \end{tabular} & \begin{tabular}[c]{@{}l@{}}- High computational efforts\\ - Unaccurate representation of the gyroscope bias\end{tabular} \\ \hline
    \end{tabular}
    \caption[Advantages and disadvantages of estimation algorithms.]{Advantages and disadvantages of estimation algorithms.}
    \label{tab:res-estimation-adv}
\end{table}

% Control
\section{Comparison of control methods}

To test the different control algorithms, a scenario was simulated where there was a large initial error and a non-zero angular rate.
This allowed for the controllers to be tested in how they converged to the desired frame. 
The controllers were tuned to don't surpass the detumbling threshold for the angular rate whilst converging or following the desired frame. 
To only evaluate the controllers' performance the real quaternion was used to calculate $\boldsymbol{\delta} q$ instead of the estimated quaternion. 
The results are presented in Tab. \ref{tab:res-controllers} and in Fig. \ref{tab:res-controllers}.

\par A comparison of the advantages and disadvantages of the control methods can be seen in Tab. \ref{tab:res-controllers-adv}. As expected, the PD controller has a high angular velocity and angular error and is incapable to react to the fast attitude variations whilst overflying the GS.
It also has a larger steady-state error, but converges to it quicker than the sliding controller. 
\par The sliding controller has the longest convergence time but when the desired frame is reached it has a much better performance than the PD controller.
Although the angular velocity error is comparable to the GFTAC controller, the angular error while overflying the GS is larger. 
\par The GFTAC controller achieves an angular error below $0.1^{\circ}$ throughout the simulation, after convergence. 
Due to having the fastest convergence times, it has a large angular rate in this period. Considering its performance isn't deteriorated while overflying the GS it is chosen for the mission implementation. 


\begin{table}[]
    \centering
    \begin{tabular}{|c|c|c|c|}
    \hline
    \textbf{Mean} & \textbf{GFTAC} & \textbf{Sliding} & \textbf{PD} \\ \hline
    \textbf{Attitude control error ($^{\circ}$)} & $0.2244$ & $1.1888$ & $3.0028$ \\ \hline
    \textbf{Angular velocity error ($^{\circ} / s$)} & $0.0689$ & $0.0637$ & $0.073$ \\ \hline
    \end{tabular}
    \caption[Comparison of controllers.]{Comparison of controllers.}
    \label{tab:res-controllers}
\end{table}


\begin{figure}[!htb]
    \centering
    \includegraphics[width=\textwidth]{Figures/allControlComplete.png}
    \caption[GFTAC, sliding and PD controllers angular velocity and pointing error.]{GFTAC (left), sliding (middle) and PD (right) controllers angular velocity (top) and pointing error (bottom) for the same initial attitude error.}
    \label{fig:res-controllers}
\end{figure}

\begin{table}[!htb]
    \centering
    \begin{tabular}{|l|l|l|}
    \hline
     & Advantages & Disadvantages \\ \hline
    PD Controller & - Simple implementation & -  Unable to cope with fast attitude changes \\ \hline
    Sliding controller & \begin{tabular}[c]{@{}l@{}}- Very good angular velocity tracking\\ - Good angular tracking\end{tabular} & \begin{tabular}[c]{@{}l@{}}- Very complicated implementation\\ - Fast-switching control law\end{tabular} \\ \hline
    GFTAC controller & \begin{tabular}[c]{@{}l@{}}- Good angular velocity tracking\\ - Very good angular tracking\end{tabular} & \begin{tabular}[c]{@{}l@{}}- Complicated implementation\\ - Hard to have a fast initial conversion without\\ surpassing the detumbling threshold for the\\ angular velocity\end{tabular} \\ \hline
    \end{tabular}
    \caption[Advantages and disadvantages of controllers.]{Advantages and disadvantages of controllers.}
    \label{tab:res-controllers-adv}
\end{table}

% Full sim
%Complete Control and Estimation, using TRIAD for initial estimation, MEKF + GFTAC
\section{Mission mode control and estimation}

For the mission, the MEKF is selected as the estimation algorithm. 
In this scenario, the beacon is only available if the GS is in the LOS of the satellite and if it is within the FOV of the telescope. 
Namely, if the pointing error rises above $10^{\circ}$ the sensor is not available. 
The TRIAD algorithm is run once for the initialization of the filter, out of eclipse so that it can use both the magnetometer and sun sensors.
When it comes to the controller, the GFTAC was chosen for its consistency in pointing accuracy and low angular velocity error. 
The quaternion estimate was used to calculate $\boldsymbol{\delta} q$ and the bias used to correct the angular velocity measurements from the gyroscope.
\par Fig. \ref{fig:res-Complete} shows the pointing error of the complete system. As observed in Section \ref{section:res-beacon} and Section \ref{section:res-beacon}, the highest values for the pointing error are observed in eclipse and are improved whilst the beacon is LOS.
It can be said that the estimation is predominantly responsible for the errors then.
The initialization of the filter with the TRIAD method resulted in lower overall errors throughout the simulation. 
A pointing error lower than $0.1^{\circ}$ was accomplished for $28.29 \%$ of the time in LOS.



\begin{figure}[!htb]
    \centering
    \includegraphics[width=\textwidth]{Figures/Complete.png}
    \caption[Complete mission mode simulation.]{Complete mission mode simulation with MEFK estimation and GFTAC control.}
    \label{fig:res-Complete}
\end{figure}
