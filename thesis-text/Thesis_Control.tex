\chapter{Control}
\label{chapter:control}
Attitude control is essential to meet mission pointing requirements, as well as other satellite functions,
such as detumbling and maintaining radio communications. 
\par The actuators can be selected separately from the control algorithms, based on the mission's pointing accuracy requirements, whilst taking into account the SWaP and the required manoeuvres.
The state-of-the-art actuators are presented and selected for mission use in Section \ref{section:actuators}.
Although the maintenance of radio communications is a separate manoeuvre from off/nadir pointing, the control algorithm can be designed with the goal of performing both of the tasks, presented in Section \ref{section:pointing}.
Lastly, the controller for the detumbling manoeuvre is presented in Section \ref{section:detumbling}.
\section{Actuators}
\label{section:actuators}
Actuators are responsible for providing all necessary control actions. Since orbit correction equipment is not necessary for the 
considered mission, orbital actuators are not analyzed. Therefore, only attitude actuators are considered. Also, no passive 
solutions are considered due to the mission's high pointing precision and reduced SWaP capacity for redundancy. This section is based 
on \cite{markleyFundamentalsSpacecraftAttitude2014} \cite{ovchinnikovAttitudeDeterminationControl2021}. 
Fluid dynamic actuators are not considered due to the increased technological complexity.

\subsection{Reaction wheels}
\label{subsection:reaction-wheels}
Reaction wheels accelerate flywheels with motors in order to achieve a reaction of the satellite in the opposite direction,
following Newton's third law. A reaction wheel is usually defined by two attributes, torque ($Nm$) and stored Momentum ($Nm/s$).
The first variable represents the amount of force that the wheel can apply on its axis and the latter represents the maximum
energy that can be stored in the inertia of the wheel.
\par Reaction wheels can be either used to generate torque through the variation of the motor's angular velocity or ran at a 
constant high speed, generating a gyro stiffness across the wheel's axis - called the momentum wheel. These applications are 
usually employed as redundancy in order to stabilize an axis or to store energy \cite{tsiotrasSatelliteAttitudeControl}.

\subsection{Control momentum gyro}
\label{subsection:cmg}
If a momentum wheel is mounted on a pivot, then when the pivot is driven to change the axis of the spinning wheel, a very 
high torque is generated on the satellite to resist the gyro stiffness of the wheel. This is the basic operating method of a 
Control Momentum gyro. Although these actuators are widely used in satellites \cite{gurrisiSpaceStationControl}, they do not scale to small sizes well and are less
applicable to CubeSat applications, especially when there are tight volumetric requirements. Also, the control algorithms are complicated
as there are certain positions where no torque is generated. 

\subsection{Magnetorquers}
\label{subsection:magnetorquers}
Magnetorquers are electromagnets that generate a large magnetic field ($A\text{m}^2$) when energized. This interacts with the local magnetic
field and generates a torque across the satellite when their field is offset from the local Earth's magnetic vector. A wide variety of materials can 
be employed to generate the field, from coils of copper wire to rods. When not being used, they generate a small residual magnetic field that 
causes a disturbance in the satellite. Some rods fabricated from soft magnetic materials are often used as passive dampers.

\subsection{Actuators comparision and selection}
\label{subsection:actuators-selection}
To select the necessary actuators, some analysis of the necessary manoeuvres' requirements must be conducted. 
For the pointing maneuver, considering both coarse and medium modes, the strictest requirement for accuracy is $0.1^{\circ}$. 
In table \ref{tab:actuators_review} literature is compiled. 
\par Due to the stark requirements of the mission, the suggested pointing actuators are 
reaction wheels or control momentum gyro. As previously discussed, the latter is not adequate for CubeSats. 
The required slewing rate of the aforementioned desired frame in relation to the inertial frame is $ 2^\circ / s $, which in \cite{wertzSpaceMissionAnalysis1999}
also suggests the use of the control momentum gyros. However, as previously stated, this actuator is not applicable. 

\begin{table}[H]
    \centering
    \begin{tabular}{|l|l|}
    \hline
    \textbf{Required accuracy $^{\circ}$} & \textbf{Effect on ADCS}  \\\hline
    $>$ 5 & Magnetorquers \\\hline
    1-5 &  Reaction wheels + magnetorquers (for momentum dumping)\\\hline
    0.1 - 1 &   Reaction wheels + magnetorquers (for momentum dumping) \\\hline
    $<$ 0.1 &  Reaction wheels + magnetorquers (for momentum dumping) but better class \\\hline
  \end{tabular}
  \caption[Attitude actuators comparison]{Comparison of attitude actuators for 3-axis stabilization, adapted from
  \cite{wertzSpaceMissionAnalysis1999} }
  \label{tab:actuators_review}
\end{table}

For the detumbling maneuver, the ADCS is responsible for reducing the spinning rate of the satellite until it gets lower than $5^\circ / s$. Such cannot be accomplished with the use of
reaction wheels and are usually accomplished by magnetorquers. The momentum dumping maneuver, which involves reducing the reaction wheel's speed, thus creating torque, also needs this 
additional sensor in order to oppose these forces. 


\subsection{Actuators model}
\label{subsection:actuators-model}

The selected actuators must be modelled in order to be incorporated in the simulation environments. The model for the reaction wheel and magnetorquers is now presented.
\vspace{0.4cm}
\par \textbf{Reaction wheels}

\par The torque generated through a reaction wheel can be modelled through a second-order system of a DC-motor coupled to a flywheel \cite{carraraTorqueSpeedControl}. This approach requires knowledge
of the reaction wheel's motor parameters, such as armature inductance and resistance, as well as the flywheels parameters. None of these specifications are available in COTS reaction 
wheels, so a model was assumed based on a first-order system for the motor, maximum torque values and physical parameter, as discussed in \ref{sec:imp-cont-act}. 
The highest angular velocity possible, positive or negative, that corresponds to maximum stored momentum, serves as saturation for the output torque. 
Jitter and control speed disturbances are also added to the wheel's angular velocity.  
\vspace{0.4cm}
\par \textbf{Magnetorquer}
\par The torque generated by a magnetorquer is given by \cite{avanziniMagneticDetumblingRigid2012}
\begin{equation}
    L_{mtq} = \mathbf{m} \times \mathbf{B}
\label{eq:c-magnetorquer-model}
\end{equation}
where the $\mathbf{m}$ is the commanded magnetic dipole moment vector generated by the coils and $\mathbf{B}$ is the 
local geomagnetic field vector expressed in terms of the body-frame components. 

\section{Pointing}
\label{section:pointing}
The mission requires the satellite to point to a specific location on the Earth, the GS. 
Whilst the satellite is not engaged in quantum communications, it still has to ensure radio communications.
This will be handled by choosing between reference attitudes in different locations of the orbit for the pointing controller.
Three different control architectures are proposed to accomplish this task.

% spin-axis vs three-axis
Early missions designed spin-stabilized satellites due to the limited control actuation and lack of computing
power to implement sophisticated control laws. Although these satellites are very stable, spin-stabilization
is very difficult to achieve, since it imposes rigid constraints on component placement and design, which is an 
arduous task, although some spinners are still used to this day \cite{westfallDesignAttitudeControl2015}.

In the modern days, sensors, actuators and computer processors have advanced to allow for three-axis
stabilization. These methods are supported as being effective for the mission's requirements, off-nadir pointing,
as discussed in \cite{wertzSpaceMissionAnalysis1999}. 
% open loop vs closed-loop
The control of satellite slews can be accomplished by either closed-loop or open-loop schemes. The latter usually requires
pre-determined pointing manoeuvres that are typically determined using optimal control techniques \cite{scrivenerSurveyTimeoptimalAttitude1994}. These
schemes are sensitive to satellite parameter uncertainties and unexpected disturbances. Closed-loop schemes are more 
robust since they can compensate for these uncertainties and disturbances. 
% quaternion and full state feedback
Although some control schemes have been derived for various attitude representations \cite{tsiotrasStabilizationOptimalityResults1996}, the quaternion is the chosen
representation since it can provide effortless orientation feedback from the estimation, as discussed in Section \ref{section:est-algo}.
The angular rate is also available through the gyroscope, completing full-state feedback.

% feedback controllers overview -> desired trajectory
\par The desired attitude motion is generated by a frame $\mathcal{D}$ with its attitude and angular velocity denoted by 
$q_d$ and $w_d$, creating a time-varying trajectory. 
It is defined in such a way that the payload is facing the GS when it is in LOS of the GS. 
The error quaternion and angular velocity error are defined as such
% deltas \boldsymbol{\delta}
\begin{equation}
    \boldsymbol{\delta} \mathbf{q} =
\begin{bmatrix}
    \boldsymbol{\delta} \mathbf{q}_{1:3}\\
\delta q_{4}
\end{bmatrix}
= \mathbf{q} \otimes \mathbf{q}_d^{-1}
\label{eq:c-delta-q}
\end{equation}
\begin{equation}
    \boldsymbol{\delta} \boldsymbol{\omega}= \boldsymbol{\omega} - A(\boldsymbol{\delta} \mathbf{q})\boldsymbol{\omega}_d = \boldsymbol{\omega} - \bar{\boldsymbol{\omega}}_d
\label{eq:c-delta-w}
\end{equation}
where the operation $ \otimes $ follows the definition in Appendix \ref{chapter:appendix-quat} and $ A(\delta \mathbf{q})$
is the transformation matrix from $\mathcal{D}$ to $\mathcal{B}$. 
The goal of an attitude controller is to drive $\boldsymbol{\omega}$ to $\boldsymbol{\omega}_d$ and $\delta \mathbf{q}$ to 
the identity quaternion $\mathbf{I}_q = [0 0 0 1]^T$. 
\par The attitude kinematics and dynamics of a rigid body follow the definition in Section \ref{section:attitude-kinematics-dynamics} (removing the superscripts for brevity):
\begin{equation}
\label{eq:c-kinematics}
\dot{\mathbf{q} } = \frac{1}{2} \Omega ( \boldsymbol{\omega})\mathbf{q} 
\end{equation}
\begin{equation}
    J \dot{\boldsymbol{\omega}} = - [ \boldsymbol{\omega} \times ] J \boldsymbol{\omega} + L
\label{eq:c-dynamics}
\end{equation}
\par Since reaction wheels are used to control the spacecraft, it is useful to define $\bar{J}$ as the moment of inertia tensor of the satellite and reaction wheels, such that 
\begin{equation}
    \bar{J} = J + J_{wheels}   
\label{eq:l-bar}
\end{equation}
% Error dynamics
\par The equations of attitude motion relative to $\mathcal{D}$ can be written based on \eqref{eq:c-kinematics} and \eqref{eq:c-dynamics} as 
\begin{equation}
    \boldsymbol{\delta}  \dot{\mathbf{q} } = \frac{1}{2} \Omega (\boldsymbol{\delta} \boldsymbol{\omega})\mathbf{q} 
\label{eq:c-q-error-dynamics}
\end{equation}
\begin{equation}
    \bar{J}  \boldsymbol{\delta} \dot{\boldsymbol{\omega}} = (\bar{J} (\boldsymbol{\delta} \boldsymbol{\omega} + \bar{\boldsymbol{\omega}}_d))^{\times} 
    - \bar{\boldsymbol{\omega}}_d^{\times}  \bar{J}  - \bar{J}  \bar{\boldsymbol{\omega}}_d^{\times}  - \bar{\boldsymbol{\omega}}_d^{\times}  \bar{J}  \bar{\boldsymbol{\omega}}_d 
    - \bar{J}  A(\boldsymbol{\delta} \mathbf{q}) \dot{\boldsymbol{\omega}}_d + L
\label{eq:c-euler-error}
\end{equation}




\subsection{Global finite-time attitude tracking controller}
\label{subsection:GFTAC}
This section follows the implementation in \cite{guiGlobalFinitetimeAttitude2016} for a global finite-time attitude tracking controller (GFTAC).
The resulting control laws possess a simple, nonlinear, proportional-derivative (PD) structure that ensures bounded control torques. 
\par From \eqref{eq:c-euler-error}, the torque required to maintain the desired attitude motion with zero tracking error can be written as 
\begin{equation}
    L_d = \bar{\boldsymbol{\omega}}_d^X \bar{J}  \bar{\boldsymbol{\omega}}_d + \bar{J}  A(\boldsymbol{\delta} \mathbf{q}) \dot{\boldsymbol{\omega}}_d 
\label{eq:c-required-torque}
\end{equation}

In order to define the control law, a hysteretic switching law is employed \cite{mayhewQuaternionBasedHybridControl2011} with the goal of guaranteeing global asymptotic stabilization.
This is through the employment of a binary logic variable $h \in H = \{-1, 1 \}$ as well as a constant hysteresis gap $\star \in (0, 1)$. The value of 
$h$ is multiplied by the scalar component of the quaternion to define the equilibrium from $ \pm 1$. A hybrid system is described as 
% \textcolor{red}{choose hybrids} ???
\begin{equation}
\mathcal{H} = \left\{ \begin{array}{cl}
    \dot{\mathbf{x}} = F(\mathbf{x}) & : \ \mathbf{x} \in C \\
    {\mathbf{x}}^+ = G(\mathbf{x}) & : \ \mathbf{x} \in D 
    \end{array} \right.
\label{eq:c-hybrid-system}
\end{equation}
where the flow map F governs the continuous evolution of state $\mathbf{x}$ while on set $C$, while the jump map $G$ governs the discrete dynamics over
the jump $D$ and $\mathbf{x}^+$ denotes the state value immediately after the jump.
\par In this case, the considered state is $\mathbf{x}_1 = (\boldsymbol{\delta} \mathbf{q}, \boldsymbol{\delta} \boldsymbol{\omega}, h)$. The flow and jump sets can be defined respectively as
\begin{equation}
    C_1 = \{ \mathbf{x}_1 :  h \delta q_4 \geq - \star \}
\label{eq:c-c-space}
\end{equation}
\begin{equation}
    D_1 = \{ \mathbf{x}_1  :  h \delta q_4 \leq - \star \}
    \label{eq:c-d-space}
\end{equation}
where $C_1 \cup D_1 = S^3 \times R^n \times H $. A hybrid attitude controller is the designated as
\begin{equation}
    L(\mathbf{x}_1) = L_d - k_1 \kappa_1 (h \boldsymbol{\delta} \mathbf{q}, 1 - \alpha_1 ) - k_2 sat_{\alpha_2} \boldsymbol{\delta} \boldsymbol{\omega}, \mathbf{x}_1 \in C_1
\label{eq:c-control-law-gftac-1}
\end{equation}
\begin{equation}
    \mathbf{x}_1^+ = G_1 (\mathbf{x}_1) = ( \boldsymbol{\delta} \mathbf{q}, \boldsymbol{\delta} \boldsymbol{\omega}, \bar{sgn}( \delta q_4)), \mathbf{x}_1 \in D_1
\label{eq:c-control-law-gftac-2}
\end{equation}
where $k_1 , k_2 > 0$, $ 0 < \alpha_1 < 1$ and $ \alpha_2 = 2 \alpha_1 /(1 + \alpha_1)$. It is noteworthy that $h$ reverses its sign only on the jump set 
$D_1$ in order to change the rotation signal. Otherwise, it remains constant and thus $\dot{h}$ = 0. The function $ \kappa_1$ is defined as 
\begin{equation}
    \kappa_1 = \left\{ \begin{array}{cl}
        \frac{\mathbf{q}}{\sqrt{2(1-\delta q_4)}}, &  \delta q_4 \neq 1 \\
        0, & : \delta q_4 = 1
        \end{array} \right.              
\label{eq:c-kappa-1}
\end{equation}
and $sat_{\alpha} (\mathbf{x})= [ sgn(x_1)min \{ | x_1 |^{\alpha}, 1 \} , ..., sgn(x_n)min \{ | x_n |^{\alpha}, 1 \} ]$ for $ \textbf(x) \in R^N$, where $sgn()$ is the standard sign function 
and  $\bar{sgn}$ is given as 
\begin{equation}
    \bar{sgn}(x) = \left\{ \begin{array}{cl}
        sgn(x) & : \ x \neq 0 \\
        \{ -1 , 1\} & : \ x = 0
        \end{array} \right. 
\label{eq:c-sgn-bar}
\end{equation}

\subsection{Sliding Controller}
\label{subsection:slinding-controller}

A sliding mode controller, also referred to as variable structure control, varies its control law based on the position of the state trajectory.
%Copied selected text to selection primary: the surface.
Once the state trajectories reach the sliding surface, referred to as the reaching phase,
the discontinuous controller forces the states to slide towards the origin, referred to as the sliding phase. The basics of the sliding controller
are presented in \cite{slotineAppliedNonlinearControl1991}. 

%\par \textcolor{red}{Should I explain the basics of the sliding controller?}

\par A sliding controller, as defined in \cite{markleyFundamentalsSpacecraftAttitude2014}, has the following sliding surface 
\begin{equation}
    \mathbf{s} = ( \boldsymbol{\omega} - \boldsymbol{\omega}_d ) + k \boldsymbol{\delta} \mathbf{q}_{1:3}
\label{eq:c-sliding-surface}
\end{equation}
where $\boldsymbol{\omega}_d$ and $\boldsymbol{\delta} \mathbf{q}_{1:3}$ follow the definitions in Section \ref{subsection:GFTAC}. If $\mathbf{s} = 0$ then there will be no error 
whilst following the commanded trajectory. The time derivative of \eqref{eq:c-sliding-surface} is given as
\begin{equation}
    \dot{\mathbf{s}} = ( \dot{\boldsymbol{\omega}} - \dot{\boldsymbol{\omega}}_d ) + k \boldsymbol{\delta} \dot{\mathbf{q}}_{1:3}
\label{eq:eq:c-sliding-surface-dot}
\end{equation}

\par The error dynamics for $\boldsymbol{\delta} \mathbf{q}_{1:3}$ are given as 
% \textcolor{red}{maybe ref estimation}^ 
\begin{equation}
    \boldsymbol{\delta} \dot{\mathbf{q}}_{1:3} = \frac{1}{2} \delta q_4 ( \boldsymbol{\omega} - \boldsymbol{\omega}_d ) + \frac{1}{2} \delta \mathbf{q}_{1:3} \times ( \boldsymbol{\omega} + \boldsymbol{\omega}_d )
\label{eq:c-error-dynamics}
\end{equation}
By substituting \eqref{eq:c-dynamics} and \eqref{eq:c-error-dynamics} into \eqref{eq:eq:c-sliding-surface-dot} gives   
\begin{equation}
    \dot{\mathbf{s}} = - \bar{J} ^{-1} [ \boldsymbol{\omega} \times ] \bar{J}  \boldsymbol{\omega} + \bar{J} ^{-1} L_e - \dot{\boldsymbol{\omega}}_d + \frac{ k }{2} [ \delta q_4 ( \boldsymbol{\omega} - \boldsymbol{\omega}_d ) + \delta \mathbf{q}_{1:3} \times ( \boldsymbol{\omega} + \boldsymbol{\omega}_d ) ]
\label{eq:eq:c-sliding-surface-dot-2}
\end{equation}
where $L_e$ denotes the best estimate of the equivalent control. In order to account for model uncertainties a discontinuous term is added across the sliding surface, such as 
\begin{equation}
    \mathbf{s} = ( \boldsymbol{\omega} - \boldsymbol{\omega}_d ) + k sign(\delta q_4) \boldsymbol{\delta} \mathbf{q}_{1:3}
\label{eq:c-sliding-surface-2}
\end{equation}
that gives the following control law that reorients the spacecraft in the shortest distance possible \cite{crassidisOptimalVariableStructureControl2000} \cite{crassidisOptimalVariableStructureControl2000}
\begin{equation}
    L = \bar{J} \{ \frac{ k }{2} [ |\delta q_4| ( \boldsymbol{\omega}_d - \boldsymbol{\omega} ) - sign(\delta q_4) \boldsymbol{\delta} \mathbf{q}_{1:3} \times ( \boldsymbol{\omega} + \boldsymbol{\omega}_d ) ] + \dot{\boldsymbol{\omega}_d} - G \bar{\mathbf{s}} \} + [ \boldsymbol{\omega} \times ] \bar{J}  \boldsymbol{\omega}
\label{eq:c-sliding-control-law}
\end{equation}
where $G$ is a positive definite matrix and $\bar{\mathbf{s}} = [\bar{s}_1, \bar{s}_2 ,\bar{s}_3]$ and $\bar{s}_i$ are given as 
\begin{equation}
    \bar{s}_i = sat(s_i, \epsilon_i), i = 1, 2, 3
\label{eq:c-s-bar}
\end{equation}
where $\epsilon_i$ is a positive constant and $s_i$ is the $i$th component of the sliding vector. The saturation function is defined by 
\begin{equation}
    sat(s_i, \epsilon_i) \equiv \left\{ \begin{array}{cl}
        1 & \text{for} \ s_i > \epsilon_i \\
        \frac{s_i}{\epsilon_i} & \text{for} \ |s_i| \le  \epsilon_i \\
        -1 & \text{for} \ s_i < \epsilon_i
        \end{array} \right.
\label{eq:c-sat}
\end{equation}
\par The stability of the closed-loop system of the controller defined above can be proven using the following candidate Lyapunov function \cite{crassidisOptimalVariableStructureControl2000}
\begin{equation}
   V = \frac{1}{2} \mathbf{s}^T \mathbf{s}
\label{eq:c-lyapunov}
\end{equation}
Using \eqref{eq:c-dynamics}, \eqref{eq:c-sliding-surface-2} and \eqref{eq:c-sliding-control-law} the time derivative of \eqref{eq:c-lyapunov} is given by 
\begin{equation}
    \dot{V} = -  \mathbf{s}^T G   \bar{\mathbf{s}}
 \label{eq:c-lyapunov-dot}
 \end{equation}
 which is always less than or equal to zero as long as $G$ is positive definite. 

 \subsection{PD controller - without feedforward}
A considerable simplification to the tracking case could be done by assuming that the attitude variation is much smaller than the controller and system response and treating it as a fixed attitude 
with zero angular velocity. The performance of this method is expected to be lower but was implemented as another comparison for the methods above. 
In \cite{markleyFundamentalsSpacecraftAttitude2014} different control laws are proposed, with corresponding stability analysis, but the one that demonstrated the best performance in the tracking case was 
\begin{equation}
    L = - k_p \text{sign}(\delta q_4) \boldsymbol{\delta} \mathbf{q}_{1:3} - k_d (1 \pm \boldsymbol{\delta} \mathbf{q}_{1:3}^T \boldsymbol{\delta} \mathbf{q}_{1:3}) \boldsymbol{\omega}
\end{equation}


\section{Detumbling}
\label{section:detumbling}
The first maneuver to be engaged is detumbling control. As discussed in Section \ref{section:actuators},
these very high angular rates cannot be effectively controlled by the Reaction Wheels, therefore the Magnetometers are employed. For ISTsat-1, different detumbling algorithms were studied for 
the same actuators. It was concluded that the bag-bang B-dot controller would be the advisable controller when compared to the regular B-dot and the angular rate feedback due to robustness and less
consumed energy \cite{nevesControlAlgorithmISTsat12019}. 
\par The proposed B-dot controller generates a magnetic dipole based on the rate of change of the Earth's magnetic field measured such that \cite{avanziniMagneticDetumblingRigid2012}
\begin{equation}
    \mathbf{m} =  \frac{k}{|| \mathbf{B} || } \boldsymbol{\omega} \times \mathbf{b}
\label{eq:c-b-dot}
\end{equation}
where $\mathbf{b}$ is the unit vector parallel to the local geomagnetic vector.  This gives the control torque  
\begin{equation}
    L = - k (I_3 - \mathbf{b} \mathbf{b}^T) \boldsymbol{\omega}
\label{eq:c-b-dot-control-torque}
\end{equation}
To prove the stability of this control law the following candidate Lyapunov function is considered:
\begin{equation}
    V = \frac{1}{2} \boldsymbol{\omega}^T \bar{J} \boldsymbol{\omega}
\label{eq:c-b-dot-lyapunov}
\end{equation}
Using equations \eqref{eq:c-dynamics} and \eqref{eq:c-b-dot-control-torque}, the derivative of the Lyapunov function is given by
\begin{equation}
    \dot{V} = -k \boldsymbol{\omega}^T (I_3 - \mathbf{b} \mathbf{b}^T) \boldsymbol{\omega}
\label{eq:c-b-dot-lyapunov-dot}
\end{equation}
The function $ \dot{V}$ is only negative semi-definite since when $\boldsymbol{\omega}$ is parallel to $\mathbf{b}$ , $\dot{V} = 0$. In practical situations this condition is never met thus 
it can be considered asymptotically stable since $\boldsymbol{\omega}$ is not only decreasing but also converging to zero.