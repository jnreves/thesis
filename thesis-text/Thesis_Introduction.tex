%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
%     File: Thesis_Introduction.tex                                    %
%     Tex Master: Thesis.tex                                           %
%                                                                      %
%     Author: Andre C. Marta                                           %
%     Last modified : 13 May 2019                                      %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Introduction}
\label{chapter:introduction}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Motivation}
\label{section:motivation}
The first satellite to be propelled into space was the Sputnik 1 in 1957 by the USSR, with the goal of having the upper hand in the race to Space with USA.
In this period, the study of all subjects regarding space had a rise in popularity and established the foundations for the work that is done to this date.
Since then, the satellites' missions have diversified, transforming from proof of concept to essential commodities in today's world.
Since the attitude determination and control system (ADCS) is the subsystem responsible for pointing the satellite to a specific location, advancements in this area is what allows the diversification of missions.
The pointing accuracy of the satellite is directly correlated to the imaging accuracy or capacity of communication, some of the most common applications for satellites.

\par The deployment of a satellite is costly and is directly related to its size and mass, so the trend in technology investments goes towards smaller satellites. 
The SmallSat category\cite{WhatAreSmallSats2015} has up to $180 kg$ and includes various sub-divisions, such as the Nanosatellite, which weight ranges from $1 kg$ to $10 kg$. 
CubeSats are a class of nanosatellites composed of one or more standardized units (U), a cube with $10 cm$ edges and mass up to $1.33 kg$, and have access to commercial launchers.
This category was created by California Polytechnic State University at San Luis Obispo (Cal Poly) and Stanford University in 1999 to provide a cheap and accessible platform for education and space exploration.
In order to stimulate students to learn the techniques and methods professionals use, the \textit{Fly Your Satellite!} \cite{FlyYourSatellite} programme was created.
The teams are guided through the typical development cycle of a space mission of a CubeSat and undergo accurate verification, with project reviews at certain phases. 

\par When the CubeSat platform is chosen for implementation, it comes with constraints in terms of size, weight, and power (SWaP), in addition to the severe environmental conditions during launch and in-orbit that already posed a challenge regarding the miniaturization and ruggedization of the payload. 
This has stimulated the development of commercial-off-the-shelf (COTS) solutions, that with the technological advancements are able to comply not only with the harsh performance requirements but also withstand the space environmental challenges.
However, in order to meet the SWaP demands of missions, the cost is steep, reaching $3300$ USD for some of the most precise sensors \cite{gutierrezIntroducingSOSTUltraLowCost2020}, therefore there is an incentive to find innovative solutions.

\par This work is focused on designing and evaluating an ADCS, while addressing these issues. The subsystem is integrated in the QuantSat-PT satellite, developed by Instituto Superior Técnico. 

\section{QuantSat-PT project}
\label{section:q-sat}
In the information age, it is crucial to create and maintain secure communications.
One way of improving our current security standards is to use Quantum Key Distribution (QKD) methods, which allows two parties to exchange encryption keys with absolute confidence that any eavesdropping by a third party will be detected \cite{rennerSECURITYQUANTUMKEY2008}.
Given that this property of QKD stems from fundamental quantum mechanics laws, it is theoretically impossible to intercept the encryption keys without this interference being detected and will remain so even considering future technological developments.
QKD is based on photonic communication links. Ground-based links typically rely on optical fibers, which have non-negligible losses, thus limiting transmission distances to a few hundreds of kilometers.
As such, satellite-based QKD is a promising approach to establish a global quantum network.
The development of a reliable and efficient space-to-ground link is an important first step, which has been demonstrated by the Micius satellite \cite{liaoSatellitetogroundQuantumKey2017}.
Implementing this technology in a CubeSat is a further step in the deployment of QKD technology, allowing for the low-cost deployment of large constellations.
Given the limited quantum link budget, extremely narrow optical beams must be used. This places strict requirements on the attitude determination and control system to guarantee pointing accuracy in the range of tens of microradians \cite{serraOpticalFrontendQuantum2021}.
Furthermore, due to the fact that space-based QKD can only be performed while the spacecraft is in eclipse, significant temperature variations can be expected and present a challenge for the internal alignment of optical components.
These constraints are addressed using a three-level pointing system which allows for internal angular corrections using fast-steering mirrors (FSMs) and for spacecraft-to-ground locking using a laser beacon.

% QuantumSat-PT
The QuantSat-PT project is the first step of a larger and longer-term vision of developing quantum communication satellites and ground stations in Portugal, making the country autonomous in such sovereignty technologies, and integrating Portugal in the future European Quantum Communication Infrastructure.
The aim is to begin the development of a 3U CubeSat for a quantum downlink. Namely, the goals of project QuantSat-PT are:
\begin{itemize}
\item To develop a quantum payload for space-earth quantum key distribution in 2U,
\item To test the quantum payload on earth over a distance of several kilometres,
\item To develop the preliminary design of the space segment.
\end{itemize}
To attain these goals, the project QuantSat-PT brings together a unique and multidisciplinary team.
It explores the complementary expertise of IT researchers from:
\begin{itemize}
\item The Physics of Information and Quantum Technologies Group responsible for the development of the first free-space quantum key distribution system in Portugal,
\item The Network Architecture and Protocols Group and the Antennas and Propagation Group, both involved in the ISTSat One, a Portuguese classical communications CubeSat to be sent to space in 2022 through the ESA program \textit{Fly Your Satellite!},
\item The Optical Networking Group,  expert in classical and quantum optical communications.
\end{itemize}
%Furthermore, project QuantSat-PT will benefit from knowledge transfer from the group developing ISTSat-1, that will be the first Portuguese CubeSat to be launched into space.

\section{Problem Statement}
\label{section:problem}
\begin{comment}
\par In order to comply with the 3U cubesat restrictions, the ADCS must be focused 
around low weight, volume and power restrictions. With this in mind, careful choice of components is
done as well as innovative uses of available hardware are considered.
\par Since the mission requires an exact alignment between the satellite and the ground station
(GS), a laser beacon tracker is transmitted from the GS, received through a telescope and processed 
in a camera and used to determine the spacecraft's attitude relative to the beacon direction. 
FSMs are used to make the fine adjustments to heading of the quantum payload. 
The accuracy of this pointing level is of 1 $\mu$ rad. However, a conventional ADCS is still 
required to provide coarse adjustments to the satellite. The satellite must be positioned in such a
way that the beacon is received as well as the FSM can be used to make the fine corrections. 
\par Also, after the satellite is launched, stabilization of its excess angular velocity must be
performed. This process is also necessary in order to ensure radio communication throughout the
orbit. 


\subsection{Pointing Requirements}
\label{subsection:pointing_req}
In order to ensure that there is a connection between the satellite and the GS, the Pointing
will be divided in 3 stages, as seen in \ref{fig:pointing_stages}.

\begin{itemize}
    \item The first stage is responsible to direct the satellite to the GS and ensure that the beacon is 
being captured in the camera's FOV (field-of-view). The sensors used will be inertial, using as reference the
environment measures (e.g. earth's magnetic field) and actuators to rotate the satellite on its
axis. The pointing error that is allowed in this stage is TODO, and is defined by the minimum accuracy
needed in the attitude in order for the beacon to be received in the camera's FOV.  
    \item  The second stage will make use of the beacon as a ground-reference frame. Even though the
beacon is detected by the camera, a finer precision is required in order to use the FSM. The
attitude change is also accomplished by the actuators and needs to have an error lower than TODO. 
    \item The third and final stage will use the FSM to point the Payload's onboard photon source to the
GS detector, using the beacon as reference. In order to guarantee a connection, this stage must have
a pointing accuracy of 1 $\mu$ rad.
\end{itemize}
\par This thesis will be focused on the first and second stage of the pointing sequence.


\subsection{General architecture}
\end{comment}
From the mission objectives, the attitude requirements are defined. In order to ensure Quantum communications, an attitude error in the range of tens of microradians must be obtained whilst in eclipse.
Due to restrictions imposed by the payload size, the volume available for sensors and actuators is reduced, which often leads to an increase in the cost of the satellite, if no performance cutbacks are possible.
With this in mind, the telescope from the payload will be used not only for communications but also to provide a ground reference, which replaces other sensors.
Other CubeSats with optical communications missions have been designed to meet the attitude accuracy requirements through star trackers \cite{nguyenDevelopmentPointingAcquisition2015}. 
In \cite{roseOpticalCommunicationsDownlink2019}, a staged approach is proposed for the attitude determination, where the beacon, when received, is used to improve pointing accuracy in the fine stage.

\par The proposed architecture for the attitude determination system can be seen in Fig. \ref{fig:pointing_stages}. The pointing requirements increase when more accurate sensors and actuators can be employed.
The first stage consists of using the common ADCS sensors to receive a signal from the beacon being transmitted by the ground station (GS). The attitude required for this stage is where the payload is pointed at the GS with an attitude error of 10°, equivalent to the field of view (FOV) of the telescope that perceives the beacon.
The second stage uses the Earth's reference from the beacon to improve the attitude estimation, with the goal of obtaining a pointing error of $0.1^{\circ}$. 
This value is defined by the amplitude of actuation of the FSM. 
These two stages are treated as one when it comes to the control and estimation algorithms and, when the required attitude is achieved, the following stage is employed.

\par A fine optical pointing stage is necessary to obtain the pointing accuracy required for quantum communications. 
The optical pointing system within the payload is in charge of this part of the mission. The laser beacon coming from the ground enters through the telescope and passes through an assembly of mirrors and lenses, guiding it to a quad-cell. 
The error signal generated from the difference between the measured and desired position of the beacon on the quad-cell is employed to drive a FSM to actuate on the beacon attitude, augmenting the pointing accuracy without the need to move the satellite structure.
This fine pointing process is executed continuously throughout the mission, designed as a closed control loop that runs in parallel with the other stages managed by the ADCS. The process, architecture, and implementation of this section of the pipeline for the Quantum-Sat project are illustrated in \cite{borralhoAlignmentControlOptical2021}.


\begin{figure}[!htb]
  \centering
  \includegraphics[width=\textwidth]{Figures/Pointing_stages.png}
  \caption[Pointing Stages.]{Pointing Stages.}
  \label{fig:pointing_stages}
\end{figure}


\subsection{ADCS Requirements}
\label{subsection:adcs_req}
Although accomplishing the mission pointing requirements are the main objective of the ADCS, there are
some other requirements it needs to comply with:
\begin{itemize}
    \item \textbf{Detumbling:} The satellite must reduce its angular velocity in after deployment in
	less than one orbit,
    \item \textbf{Communications:} The satellite must be able to point to nadir with an accuracy of
	20 degree in order to allow communications,
    \item \textbf{Off-nadir pointing:} The satellite must be able to point towards the GS while passing over it.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\section{State-of-the-Art}
%\label{section:sota}

% Other CubeSats with optical communications missions have been designed to meet the attitude accuracy requirements through start rackers [7]. In [8], a staged approach is proposed for the attitude determination, where the beacon, when received, is used to improve pointing accuracy in the fine stage.
%\textcolor{blue}{TODO - fix bibliography conflics before merge, done in broken branch}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{comment}
\section{Objectives}
\label{section:objectives}

Explicitly state the objectives set to be achieved with this thesis...

Also list the expected deliverables.
\end{comment}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Thesis Outline}
\label{section:outline}

The thesis is organized in the following chapters:
\begin{itemize}
    \item \textbf{Chapter 2} gives a summary of the background concepts, such as attitude representations, different frames and spacecraft mechanics;
    \item \textbf{Chapter 3} selects the sensors that are used in the implementation and exposes the attitude estimation and determination algorithms;
    \item \textbf{Chapter 4} analyses the actuators for attitude control, as well as the algorithms for the different manoeuvres of the satellite;
    \item \textbf{Chapter 5} describes the simulation environment used to validate and compare the different methods. It also provides the parameters for the orbit, environment and sensor/actuators parameters;
    \item \textbf{Chapter 6} displays the results of the simulations and reflects on their performance;
    \item \textbf{Chapter 7} draws conclusions on the work done and suggests future work.
\end{itemize}


%paper
\begin{comment}

    % Intro
    In the information age, it is crucial to create and maintain secure communications. 
    One way of improving our current security standards is to use Quantum Key Distribution (QKD) methods, which allows two parties to exchange encryption keys with absolute confidence that any eavesdropping by a third party will be detected [1]. 
    Given that this property of QKD stems from fundamental quantum mechanics laws, it is theoretically impossible to intercept the encryption keys without this interference being detected and will remain so even considering future technological developments.
    QKD is based on photonic communication links. Ground-based links typically rely on optical fibers, which have non-negligible losses, thus limiting transmission distances to a few hundreds of km [2]. 
    As such, satellite-based QKD is a promising approach to establishing a global quantum network. 
    The development of a reliable and efficient space-to-ground link is an important first step, which has been demonstrated by the Micius satellite [3]. 
    Implementing this technology in a CubeSat is a further step in the deployment of QKD technology, allowing for the low-cost deployment of large constellations. 
    Given the limited quantum link budget, extremely narrow optical beams must be used. This places strict requirements on the attitude determination and control system to guarantee pointing accuracy in the range of tens of microradians [4]. 
    Furthermore, due to the fact that space-based QKD can only be performed while the spacecraft is in eclipse, significant temperature variations can be expected and present a challenge for the internal alignment of optical components. 
    These constraints are addressed using a three-level pointing system which allows for internal angular corrections using Fast 
    Steering Mirrors and for spacecraft-to-ground locking using a laser beacon. 
    
    % QuantumSat-PT
    Project QuantSat-PT is the first step of a larger and longer-term vision of developing quantum communication satellites and ground stations in Portugal, making the country autonomous in such sovereignty technologies, and integrating Portugal in the future European Quantum Communication Infrastructure.
    This project is funded by the Instituto de Telecomunicações. 
    The aim is to begin the development of a 3U CubeSat for a quantum downlink. Namely, the goals of project QuantSat-PT are:
    • To develop a quantum payload for space-earth quantum key distribution in 2U.
    • To test the quantum payload on earth over a distance of several kilometres.
    • To develop the preliminary design of the space segment.
    To attain these goals, the project QuantSat-PT brings together a unique and multidisciplinary team. 
    It explores the complementary expertise of IT researchers from:
    • The Physics of Information and Quantum Technologies Group responsible for the development of the first free-space quantum key distribution system in Portugal.
    • The Network Architecture and Protocols Group and the Antennas and Propagation Group, both involved in the ISTSat One, a Portuguese classical communications CubeSat to be sent to space in 2022 through the ESA program Fly Your Satellite.
    • The Optical Networking Group,  expert in classical and quantum optical communications.
    Furthermore, project QuantSat-PT will benefit from knowledge transfer from the group of Dr. Rupert Ursin (Institute for Quantum Optics and Quantum Information, Vienna), a leading expert in the domain of satellite quantum communications, as well as from the involvement of Lusospace, a leading Portuguese company in spatial optical communications.


    % Cubesat Implementation 
    Since the CubeSat platform is now established as an attractive alternative for education and research organizations that aim to bring innovative technology to space, at a lower cost than larger platforms, it was selected as the basis for this mission. 
    These nanosatellites have a standardized form factor, allowing for quick and accessible low-risk testing of novel technological concepts, as well as access to commercial launchers. 
    Nevertheless, this implementation constrains the QuantSat-PT outline in terms of Size, Weight, and Power, in addition to the severe environmental conditions during launch and in-orbit that already posed a challenge regarding the miniaturization and ruggedization of the payload. 
    This article is focused on the Attitude Determination and Control System (ADCS) which is responsible for determining the orientation of a satellite and then controlling it so that it points to the desired direction [5]. 
    It also includes other functions, such as providing initial damping for the satellite angular motion after deployment. 
    It is composed of sensors, actuators and an onboard computer.  
    ADCS design is subject to strict constraints when it comes to size and mass, often limiting the pointing and slew-rate requirements. 
    With these restrictions in mind, the proposed solution repurposes the Optical devices from the Quantum payload to increase attitude estimation accuracy without the use of additional sensors. 
    The Ground Station (GS) beacon is used as the Earth reference on an attitude estimation filter, when available. 


    [1] 	Renner, R. Security of quantum key distribution. International Journal of Quantum Information, 6(01), 1-127, 2008.
    [2] 	Zhang, Q., Xu, F., Chen, Y. A., Peng, C. Z., & Pan, J. W. Large scale quantum key distribution: challenges and solutions. Optics express, 26(18), 24260-24273, 2018.
    [3] 	Liao, S. K., Cai, W. Q., Liu, W. Y., Zhang, L., Li, Y., Ren, J. G., ... & Pan, J. W. Satellite-to-ground quantum key distribution. Nature, 549(7670), 43-47, 2017.
    [4]  Serra, P., Čierny, O., Kammerer, W., Douglas, E. S., Kim, D. W., Ashcraft, J. N., ... & Cahoy, K. Optical front-end for a quantum key distribution CubeSat. In International Conference on Space Optics—ICSO 2020 (Vol. 11852, p. 118523C). International Society for Optics and Photonics, 2021.
    [5] 	F. Markley and J. Crassidis, "Fundamentals of Spacecraft Attitude Determination and Control", 2014. Available: 10.1007/978-1-4939-0802-8 [Accessed 8 March 2022].
    [6] 	J. Bae, Y. Kim and H. Kim, Satellite Attitude Determination and Estimation using Two Star Trackers, AIAA Guidance, Navigation, and Control Conference, 2010.
    [8]	T. Rose et al., Optical communications downlink from a low-earth orbiting 15U CubeSat, Optics Express, vol. 27, no. 17, p. 24382, 2019. 
    [7] 	T. Nguyen, K. Riesing, R. Kingsbury and K. Cahoy, Development of a pointing, acquisition, and tracking system for a CubeSat optical communication module, SPIE Proceedings, 2015.
    [9]	Tese da Ana
\end{comment}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
