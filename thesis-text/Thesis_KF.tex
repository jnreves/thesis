\section{State Estimation Algorithms}
\label{section:est-algo}

State estimation refers to the process of estimating the state of a system, based on a set of
measurements corrupted by random noise, with known or estimated statistics.
The dynamic model of a system is used in conjunction with measurements to find the best estimation.
This type of approach also employs sensor fusion, since low-frequency measurements can be combined
with kinematic or dynamic models, and high-frequency sensor measurements allow the state to be propagated. 
It also allows for a representation of a state that is not directly measurable through the use of sensors  and models, as well as
filtering of noisy data.
In comparison with static methods, the main advantage is that it incorporates memory of previous
observations. 

% (batch vs single?)
This section reviews the concepts of state and observation models, establishes the fundamentals of
the Kalman Filter (KF), introducing then the formulation of linear and non-linear KF. Then, it
is applied to attitude estimation and an onboard filter is formulated. Other estimation methods will also be discussed.


\subsection{State and Observation Models}
The description of a system is based on the state variables, which usually refer to a physical
concept, directly related to the storage or dissipation of energy and are collected in the state 
vector. The size of this vector is known as the order of the system. 
When a dynamic system is being modelled, differential equations are used
to characterize the interactions between the different states and their number is connected to the
order of the system. In mechanical systems, the state usually includes the linear or angular position, as well as the corresponding velocity.
\par A $n$-th order system can be written as $n$ first order equations
that form the following system of state equations:

\begin{equation}
    \dot{\mathbf{x}}(t) =F(\mathbf{x}(t), \mathbf{u}(t), t),\quad \mathbf{x}(t_{0}) =\mathbf{x}_{0}
\label{eq:system}
\end{equation}

where $F$ is an $n \times 1$ sufficiently differentiable vector, $\mathbf{x}$ is
an $n \times 1$ state vector, and $\mathbf{u}$ is a $q \times 1$ vector that represents
any input that does not depend on the state elements.

\par If \eqref{eq:system} is particularized for linear systems, where the superposition
principle is satisfied, the following system is obtained:
\begin{equation}
    \dot{\mathbf{x}}(t) =F(t)\mathbf{x}(t) + B(t)\mathbf{u}(t),\quad  \mathbf{x}(t_{0}) =\mathbf{x}_{0}
\label{eq:system-linear}
\end{equation}

where $F$ is the $n \times n$ state matrix and $B$ is the $n \times q$ input matrix. The solution of
\eqref{eq:system-linear} is given by:

\begin{equation}
    \mathbf{x} = \Phi (t, t_{0})\mathbf{x}_{0} + \int_{t_{0}}^t
    \Phi(t,\tau) B(\tau)\mathbf{u}(\tau) d\tau
\label{eq:transition}    
\end{equation}

where $\Phi(t,t_{0})$ is known as the state transition matrix, which has the following properties:


\begin{subequations}
    \begin{gather}
        \Phi(t_{0}, t_{0}) = \textit{I} \\
        \Phi(t_{0}, t) = \Phi^{-1}(t, t_{0}) \\
        \Phi(t_{2}, t_{0}) = \Phi(t_{2}, t_{1})\Phi(t_{1}, t_{0}) \\
        \dot{\Phi}(t, t_{0}) = F(t)\Phi(t, t_{0}).
    \end{gather}
    \label{eq:transition-def}
\end{subequations}

%% observation
\par Observation vectors are used to disclose how the sensor measurements relate to the systems'
states. The number of elements in an observation sensor is usually determined by the number of
types of sensors and their multiplicity. The general form of an observation vector is given by

\begin{equation}
    \mathbf{y}(t) = h(\mathbf{x}(t), \mathbf{u}(t), t)
\label{eq:general-obs-vec}
\end{equation}
where $h$ is a $p \times 1$ observation. In linear systems, the previous equation can be
written as:

\begin{equation}
    \mathbf{y}(t) = H(t)\mathbf{x}(t) + D(t)\mathbf{u}(t)
\label{eq:general-obs-vec-linear}
\end{equation}

where H is the $p \times n$ observation/sensitivity  matrix and D is the
$p \times q$ direct transmission matrix.

Since sensors and actuators do not work in continuous-time, it is relevant to introduce a
discrete-time representation of the dynamic systems and observations. The following considerations
are based in a zero-order hold sample-type since it is the most common. As the sampling
interval $\Delta t$ decreases, the sampled signal resembles more precisely the real
signal.

When considering the first sample interval $ \Delta t$, $F$ and $B$ constant, \eqref{eq:transition}
simplifies to:

\begin{equation}
    \Delta \mathbf{x} = \Phi \mathbf{x} (0) + \Gamma \mathbf{u} (0)
    \label{eq:discrete-transition-1st}
\end{equation}

with $\Phi \equiv e^{F\Delta t}$ and $\Gamma \equiv [\int_{0}^{\Delta t}e^{Ft} dt]B$.

Extending \eqref{eq:discrete-transition-1st} for $k+1$ samples:
\begin{equation}
    \mathbf{x}[(k+1) \Delta t] = \Phi \mathbf{x} (k \Delta t) + \Gamma \mathbf{u} (k \Delta t))
    \label{eq:discrete-transition-k}
\end{equation}

The common notation for \eqref{eq:discrete-transition-k} is:
\begin{equation}
    \mathbf{x}_{k+1} = \Phi \mathbf{x}_{k} + \Gamma u_{k}
    \label{eq:discrete-transition-k-2}
\end{equation}

and \eqref{eq:general-obs-vec-linear} can also be written in discrete-time to complete the state-space description
such as:

\begin{equation}
    \mathbf{y}_{k} = H\mathbf{x}_{k} + D\mathbf{u}_{k}
\label{eq:general-obs-vec-linear-ss}
\end{equation}



\subsection{Kalman Filter}
\label{section:lkf}

The Kalman filter was proposed in the 1950s by Rudolph Emil Kalman, as a technique for 
filtering and prediction in linear systems \cite{thrunProbabilisticRobotics2002}. 
The filter uses knowledge of the system, along with its related uncertainties, and
fuses any available measurements, with corresponding errors, to create an optimum
estimate of the systems' variables/states. 
When applied to a linear system where the process noise and the measurement noise follow statistical 
properties, the KF satisfies the optimality criterion that minimizes the trace of the
covariance of the estimation error between the true state and the estimated one. 
The filter is recursive since it has memory of previous measurements, but it only needs to process
the previous iteration's results.


The KF finds the optimal estimate of $\mathbf{x}$, given by $\hat{\mathbf{x}}$.
A residual error is defined as the difference between the true value and the estimate

\begin{equation}
    \boldsymbol{\epsilon}= \mathbf{x} - \hat{\mathbf{x}}
    \label{eq:discrete-residual}
\end{equation}

with covariance 
\begin{equation}
    P = E\{ \boldsymbol{\epsilon} \boldsymbol{\epsilon}^T \} 
    \label{eq:discrete-residual-covariance}
\end{equation}
The three steps of the KF are shown in  Alg. \ref{alg:lkf}. The Initialization step specifies the initial 
values for $\hat{\mathbf{x}}_{0}$ and $P_0$. The following two steps alternate recursively,
minimizing the trace of P.
The Propagation step is based on the system kinematics and dynamics and estimates the state $\hat{\mathbf{x}}_{k+1}^-$
and covariance $P_{k+1}^-$ at from $t_{k}$ to time $t_{k+1}$.  These are known as the \textit{a priori} 
estimates and are denoted as $\square^-$, since they do not include the observations.
The Update step utilizes the observations at $\mathbf{y}_{k+1}$ to correct the \textit{a priori} estimates, 
providing the \textit{a posteriori} estimates,  $\hat{\mathbf{x}}_{k+1}^+$ $P_{k+1}^+$, denoted by $\square^+$.
These values are used in the following iteration of the Propagation step. 



\begin{comment}
\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.5\textwidth]{Figures/KF_Diagram.png}
    \caption[Kalman Filter structure.]{Kalman Filter steps - Propagation uses the process model to create the \textit{a priori} estimate and the Update, alongside with the observations, outputs the \textit{a posteriori} estimate}
    \label{fig:kf-structure}
  \end{figure}
\end{comment}

\begin{algorithm}
    \caption{Linear Kalman Filter}
    \begin{algorithmic} 
        \State Initialization: $\hat{\mathbf{x}}_0 , P_0$
        \Loop
            \State Propagation: $\hat{\mathbf{x}}_{k+1}^- , P_{k+1}^- $
            \If{observations ready: $\mathbf{y}_{k+1}$ }        
            \State Update: $\hat{\mathbf{x}}_{k+1}^+ , P_{k+1}^+$ 
    \EndIf

    \Return $\hat{\mathbf{x}}_{k}^+ , P_{k}^+$ 
\EndLoop
\end{algorithmic}
\label{alg:lkf}
\end{algorithm}

% real
The KF algorithm starts by expanding the linear dynamic model in \eqref{eq:system}
with noise terms
\begin{equation}
    \dot{\mathbf{x}}(t) =F(t)\mathbf{x}(t) + B(t)\mathbf{u}(t) + G(t)\mathbf{w}(t)
\label{eq:system-linear-noise}
\end{equation}
where $\mathbf{w}$ is a zero-mean $p \times 1$ Gaussian white-noise process with covariance matrix $Q$, and  
$G$ is $n \times $ a process noise distribution matrix. 


%If the considered model is autonomous, a steady-state expression for the error covariance
%can be used due to the fact that the covariance converges quickly in this cases. 
According to \eqref{eq:transition} and assuming $\mathbf{u}(t)$ is a piece-wise constant, the solution of \eqref{eq:system-linear-noise} at discrete time instants can be written as 
\begin{equation}
    \mathbf{x}_{k+1} = \Phi_k \hat{\mathbf{x}}_{k} + \Gamma_k \mathbf{u}_{k} + \Upsilon_k \mathbf{w}_k
    \label{eq:discrete-linear-noise}
\end{equation}
where $\mathbf{w}_k$ is a zero-mean $p \times 1$ Gaussian white-noise process with covariance $Q$, which results from sampling a continuous-time random process.
% est
The state estimate has the following continuous-time expression
\begin{equation}
    \dot{\hat{\mathbf{x}}}(t) =F(t)\mathbf{x}(t) + B(t)\mathbf{u}(t) 
    \label{eq:system-linear-noise-2}
\end{equation}
and the discrete-time expression
\begin{equation}
    \hat{\mathbf{x}}_{k+1} = \Phi \hat{\mathbf{x}}_{k} + \Gamma u_{k}
    \label{eq:discrete-linear-noise-2}
\end{equation}
% cov
The previously defined covariance obeys the equation
\begin{equation}
    \dot{P} = E \{ \dot{\boldsymbol{\epsilon}} \boldsymbol{\epsilon}^
    {T} \} + E\{ \boldsymbol{\epsilon} \dot{\boldsymbol{\epsilon}}^{T} \} = FP + P F^{T} + G Q G^{T}
\end{equation}
which is solved for $P(t)$ with initial condition P($t_0$) = $P_0$. 
It can also be shown that the covariance evolves according to \cite{lewisOptimalRobustEstimation}
%\par \textcolor{red}{CHECK - $\Phi_k$ depends on the angular velocity, hence varies each sampling time $k$}
%\par \textcolor{red}{CHECK - $\Upsilon$ is the comes from the riccati equation Fig. \ref{fig:riccati-temp}}
\begin{comment}[H]
    \centering
    \includegraphics[width=\textwidth]{Figures/2022-05-25_19-55_1.png}
    \caption[Pointing Stages.]{\textcolor{red}{lewisOptimalRobustEstimation page 72 (book) $\Upsilon = G_k$ ?}}
    \label{fig:riccati-temp}
\end{comment}
\begin{equation}
    {P}_{k+1} = \Phi_{k} {P}_{k} \Phi_{k}^T + \Upsilon Q_k \Upsilon^T 
\end{equation}


%mes
Since the measurements are performed in discrete-time due to the sensors being analogue, they are modelled by  
\begin{equation}
    \mathbf{y}_{k} = H_k \mathbf{x}_{k} + \mathbf{v}_k
    \label{eq:measurement-discrete}
\end{equation}
where $\mathbf{v}_k$ zero-mean $m \times 1$ Gaussian white-noise process vector with covariance $R_k$.

\vspace{0.4cm}
\textbf{Propagation Step}
\par The propagation step uses the previously defined equations for $\hat{\mathbf{x}}$ and ${P}$ to transport the \textit{a posteriori}
estimates at time $t_k$ to the \textit{a priori} estimates at time $t_{k+1}$
\vspace{0.4cm}
\par \textbf{Update Step}
\par When a measurement is $\mathbf{y}_k$ is available at time $t_k$. The linear discrete-time
update equation is given by
\begin{equation}
    \hat{\mathbf{x}}_{k}^{+} = \hat{\mathbf{x}}_{k}^{-} + K_k [\mathbf{y}_k - H_k \hat{\mathbf{x}}_{k}^{-}]
    \label{eq:update-x}
\end{equation}
The updated covariance expression is given by computing 
\begin{equation}
    P_k^+ = E \{ (\mathbf{x} - \hat{\mathbf{x}}^+)(\mathbf{x} - \hat{\mathbf{x}}^+)^T  \}
    \label{eq:update-cov}
\end{equation}

where $I$ is a $n \times n$ identity matrix. Substituting \eqref{eq:measurement-discrete} and \eqref{eq:update-x} into \eqref{eq:update-cov} leads to tolerance
\begin{equation}
    P_k^+ = [I - K_k H_k] P_k^- [I - K_k H_k]^T + K_k R_k K_k^t
    \label{eq:update-cov-complete}
\end{equation}
where $P_k^-$ is given from the propagated system. The optimal $K_k$ gain is determined by minimizing the 
trace of the updated covariance, such as
\begin{equation}
    J_k = tr P_k^+
\end{equation}
that leads to 
\begin{equation}
    K_k = P_k^- H_k^T [H_k P_k^- H_k^T + R_k]^T
    \label{eq:kalman-gain}
\end{equation}
Substituting \eqref{eq:kalman-gain} into \eqref{eq:update-cov-complete} gives
\begin{equation}
    P_k^+ = [I - K_k H_k] P_k^- 
    \label{eq:update-cov-complete-2}
\end{equation}
This is the most useful covariance update, but \eqref{eq:update-cov-complete} can also be
used when numerical instabilities are a concern.

The discrete-time KF is summarized in Tab. \ref{tab:kf-equations}, where $N(\mathbf{0}, R_k)$ denotes a 
Gaussian distribution with zero mean and covariance $R_k$.

% PICTURE 1
\begin{comment}
    \begin{figure}[H]   
      \includegraphics[width=\textwidth]{Figures/temp-lkf-table.png}
      \caption{temporary in picture, maybe in Annex- LKF table}
      \label{temp1}
    \end{figure}
    \end{comment}
\begin{table}[H]
    \centering
    \begin{tabular}{lll}
        \hline
    \multirow[t]{3}{*}{Model} & $\mathbf{x}_{k+1}=\Phi_{k} \mathbf{x}_{k}+\Gamma_{k} \mathbf{u}_{k}+\Upsilon_{k} \mathbf{w}_{k}, $&$\text { with } \mathbf{w}_{k} \sim N\left(0, \mathbf{Q}_{k}\right) $\\
        & $\mathbf{y}_{k}=H_{k} \mathbf{x}_{k}+\mathbf{v}_{k}, $&$ \text { with } \mathbf{v}_{k} \sim N\left(0, R_{k}\right)$\\
        \hline
    \multirow[t]{3}{*}{Initial Condition} & $\hat{\mathbf{x}}\left(t_{0}\right)=\hat{\mathbf{x}}_{0}$ & \\
        & $P\left(t_{0}\right)=P_{0}$ &\\
        \hline
    \multirow[t]{3}{*}{Propagation} & $\hat{\mathbf{x}}_{k+1}^{-}=\Phi_{k} \hat{\mathbf{x}}_{k}^{+} $\\
        & $P_{k+1}^{-}=\Phi_{k} P_{k}^{+} \Phi_{k}^{T}+\Upsilon_{k} Q_{k} \Upsilon_{k}^{T} $ \\
        \hline
    Gain & $K_{k}=P_{k}^{-} H_{k}^{T}\left[H_{k} P_{k}^{-} H_{k}^{T}+R_{k}\right]^{-1}$\\
    \hline
    \multirow[t]{3}{*}{Update} & $\hat{\mathbf{x}}_{k}^{+}=\hat{\mathbf{x}}_{k}^{-}+K_{k}\left[\mathbf{y}_{k}-H_{k} \hat{\mathbf{x}}_{k}^{-}\right]$ \\
        & $P_{k}^{+}=\left[I-K_{k} H_{k}\right] P_{k}^{-}$\\
        \hline
    \end{tabular}%
    \caption{Summary of KF equations.}
    \label{tab:kf-equations}%
\end{table}

\subsection{Extended Kalman Filter}

Many problems, including attitude estimation, involve nonlinear models.
To tackle these cases, the Extended Kalman Filter (EKF) was created, which revolves around 
linearization about the current best estimate and using the methods defined in Section \ref{section:lkf}.
This section is based on \cite{crassidisOptimalEstimationDynamic2012a}.
\par Consider the following general continuous nonlinear model:

\begin{equation}
    \mathbf{x}(t) = f(\mathbf{x}(t), \mathbf{u}(t), t)) + G(t)\mathbf{w}(t), \mathbf{w}(t) \sim N(\mathbf{0}, Q(t) )
\end{equation}
\begin{equation}
    \mathbf{y}(t) = h(\mathbf{x}(t, t)) + \mathbf{v}(t), \mathbf{v}(t) \sim N(\mathbf{0}, R(t) )
    \label{eq:measurement-ekf}
\end{equation}

and the discrete-time equivalent 
\begin{equation}
   \mathbf{x}_{k+1} = f_k(\mathbf{x}_k, \mathbf{u}_k) + \Upsilon_k \mathbf{w}_k, \mathbf{w}_k \sim N(\mathbf{0}, Q(t) )
    \label{eq:discrete-ekf-state}
\end{equation}
\begin{equation}
    \mathbf{y}_k = h_k(\mathbf{x}_k) + \mathbf{v}_k, \mathbf{v}_k \sim N(\mathbf{0}, R(t) )
\end{equation}

where $f(\mathbf{x}(t), \mathbf{u}(t), t)$ and $h(\mathbf{x}(t), t)$ are continuously differentiable nonlinear functions, 
and $f_k(\mathbf{x}_k, \mathbf{u}_k)$ and $h_k(\mathbf{x}_k)$ are discrete-time differentiable nonlinear functions and the
other variables follow the definition in Section \ref{section:lkf}.
\par The probability density functions of the noise terms are altered when 
transmitted through nonlinear elements, which means that a Gaussian input doesn't inherently cause a non-Gaussian response.
In order to overcome this problem, nonlinear systems are approximated by a linear one, through
the first-order Taylor expansion, by assuming that the true state is 
sufficiently close to the estimate and therefore are valid for small perturbations.

\par The first-order expansion for $f(\mathbf{x}(t), \mathbf{u}(t), t)$ and $h(\mathbf{x}(t), t)$, about a nominal
state $\bar{\mathbf{x}}(t)$ is given by
\begin{equation}
    f(\mathbf{x}(t), \mathbf{u}(t), t)) \simeq f(\bar{\mathbf{x}}(t), \mathbf{u}(t), t)) + \frac{\partial f}{\partial \mathbf{x}}\vert_{\bar{\mathbf{x}}(t)} [\mathbf{x}(t) - \bar{\mathbf{x}}(t)]
    \label{eq:taylor-f}
\end{equation}
\begin{equation}
    h(\mathbf{x}(t, t)) = h(\bar{\mathbf{x}}(t, t)) + \frac{\partial h}{\partial \mathbf{x}}\vert_{\bar{\mathbf{x}}(t)} [\mathbf{x}(t) - \bar{\mathbf{x}}(t)]
    \label{eq:taylor-h}
\end{equation}

where $\bar{\mathbf{x}}(t) = \hat{\mathbf{x}}(t)$ in the case of the EKF. This lead to 
the following estimation error dynamics
% não estás a implementar um EKF no contínuo
% F(t) é a derivada parcial de f(x(t),u(t)) em ordem a x(t) enquanto que não é igual à derivada parcial de f_k(x_k,u_k) em ordem a x_k
\begin{equation}
\Delta \dot{\mathbf{x}}(t) = [F(t) - K(t)H(t)]\Delta \mathbf{x}(t) - G(t)\mathbf{w}(t)+K(t)\mathbf{v}(t)
\label{eq:error-dynamics-ekf}
\end{equation}
where $\Delta \dot{\mathbf{x}}(t) =  \hat{\mathbf{x}} - \mathbf{x}(t)$, $H(t)$ is the Kalman gain matrix and
\begin{subequations}
    \begin{align}
        F(t) = \frac{\partial f(t)}{\partial \mathbf{x}} \vert_{\hat{\mathbf{x}}(t), \mathbf{u}(t)} \\
        H(t) = \frac{\partial h(t)}{\partial \mathbf{x}} \vert_{\hat{\mathbf{x}}(t)} \\
    \end{align}
    \label{eq:linearization}
\end{subequations}
As \eqref{eq:error-dynamics-ekf} is linear, the EKF can be formulated based on error estimation. 
\par The filter structure can be summarized in Alg. \ref{alg:ekf} and yields the following equations in Tab. \ref{tab:ekf-equations}.

% PICTURE 2
\begin{comment}
    \begin{figure}[H]   
      \includegraphics[width=0.7\textwidth]{Figures/temp-ekf-equations.png}
      \caption{temporary in picture, maybe in Annex- EKF table}
          \label{temp2}
    \end{figure}
    \end{comment}
\begin{table}[H]
    \centering
      \begin{tabular}{lll}
          \hline
          \multirow[t]{3}{*}{Model} & $\mathbf{x}_{k+1}=f_{k}\left(\mathbf{x}_{k}, \mathbf{u}_{k}\right)+\Upsilon_{k} \mathbf{w}_{k}, $&$ \mathbf{w}_{k} \sim N\left(0, Q_{k}\right)$ \\
          & $\mathbf{y}_{k}=h_{k}\left(\mathbf{x}_{k}\right)+\mathbf{v}_{k}, $&$ \mathbf{v}_{k} \sim N\left(0, R_{k}\right)$ \\
          \hline
          \multirow[t]{3}{*}{Initial condition} & $\hat{\mathbf{x}}\left(t_{0}\right)=\hat{\mathbf{x}}_{0}$ \\
          & $P\left(t_{0}\right)=P_{0}$ \\
          \hline
          \multirow[t]{3}{*}{Propagation} & $\hat{\mathbf{x}}_{k+1}^{-}=f_{k}\left(\hat{\mathbf{x}}_{k}^{+}\right)$ \\
          & $P_{k+1}^{-}=\Phi_{k} P_{k}^{+} \Phi_{k}^{T}+\Upsilon_{k} Q_{k} \Upsilon_{k}^{T},$&$\left.\quad \Phi_{k} \equiv \frac{\delta f_{k}}{\delta \mathbf{x}}\right|_{\hat{\mathbf{x}}_{k}^{+}}$ \\
          \hline
          Gain & $K_{k}=P_{k}^{-} H_{k}^{T}\left[H_{k} P_{k}^{-} H_{k}^{T}+R_{k}\right]^{-1}, $&$\left.\quad H_{k} \equiv \frac{\delta h_{k}}{\delta \mathbf{x}}\right|_{\hat{\mathbf{x}}_{k}^{-}}$\\
          \hline
          \multirow[t]{3}{*}{Update} & $\Delta \hat{\mathbf{x}}_{k}^{+}=K_{k}\left[\mathbf{y}_{k}-h_{k}\left(\hat{\mathbf{x}}_{k}^{-}\right)\right]$ \\
          & $\hat{\mathbf{x}}_{k}^{+}=\hat{\mathbf{x}}_{k}^{-}+\Delta \hat{\mathbf{x}}_{k}^{+}$ \\
          & $P_{k}^{+}=\left[I-K_{k} H_{k}\right] P_{k}^{-}$\\
          \hline
      \end{tabular}%
      \caption{Summary of EKF equations.}
    \label{tab:ekf-equations}%
  \end{table}


\begin{algorithm}
\caption{Extended Kalman Filter}
\begin{algorithmic} 
\State Initialization: $\hat{\mathbf{x}}_0 , P_0$
\Loop
\State Propagation: $\hat{\mathbf{x}}_{k+1}^- , P_{k+1}^- $
\If{observations ready: $\mathbf{y}_{k+1}$ }
\State calculate kalman gain: $K_k$
\State Calculate uncertainty: $ \delta \hat{\mathbf{x}}_{k+1} $
\State Update: $\hat{\mathbf{x}}_{k+1}^+ , P_{k+1}^+$
\State Reset: $ \delta \hat{\mathbf{x}}_{k+1} = \mathbf{0} $
\EndIf

\Return $\hat{\mathbf{x}}_{k}^+ , P_{k}^+$
\EndLoop
\end{algorithmic}
\label{alg:ekf}
\end{algorithm}


For filter performance, non-Gaussian measurement and process noise errors are a major source of concern, since its most real-world applications the sources are non-Gaussian.
One alternative is to use a coloured-noise filter \cite{lewisOptimalRobustEstimation}.
\par The main concern when it comes to the EKF is the accuracy of the linearized model, which directly influences the accuracy of the filter.
For example, the quaternion kinematic model loses normalization in a straightforward linearization. 


\begin{comment}
\textbf{Filter} 
\par The error, or uncertainty, propagation can also be written in discrete-time, using the first-order Taylor expression in \ref{eq:discrete-ekf-state}, such as 

\begin{equation}
    \Delta \textbf{x}_{k+1}^- = \Phi_k  \Delta \textbf{x}_{k}^+ + \Upsilon_k \textbf{w}_k, \Phi_k = \frac{\partial f_k}{\partial \textbf{x}} \vert_{\hat{\textbf{x}}_k^+}
    \label{eq:ekf-propagated-uncertainty}
\end{equation}

where $\Delta \textbf{x}_k = \textbf{x}_k - \hat{\textbf{x}}_k$,  which results in the following expression for the error covariance propagation
\begin{equation}
P_{k+1}^- = \Phi_k P_{k}^+ \Phi_k^T + \Upsilon_k Q_k \Upsilon_k^T
\label{eq:ekf-propagated-covariance}
\end{equation}
The propagation of the estimate is done with resort to the same nonlinear dynamics function in \ref{eq:discrete-ekf-state} and is given by 
\begin{equation}
    \hat{ \textbf{x} }_{k+1}^- = f_k( \textbf{x}_{k}^+ )
    \label{eq:ekf-propagated-estimate}
\end{equation}
The propagation step is then described by \ref{eq:ekf-propagated-estimate} and \ref{eq:ekf-propagated-covariance}. 

\par For the observations, a discrete linear measurement model is also defined by the difference of the measurements
and the estimated measurements based on the propagated state estimation $\hat{\textbf{x}}_k^-$, as such 
\begin{equation}
    \Delta \textbf{y}_k = \textbf{y}_k - \hat{\textbf{y}}_k = \textbf{y}_k - h_k(\hat{\textbf{x}}_k^-)
\end{equation}
that can be expanded by the same process of \ref{eq:error-dynamics-ekf} using m \ref{eq:taylor-h}. 
The first-order approximation for $\Delta \textbf{y}_k$ is given as 
\begin{equation}
    \Delta \textbf{y}_k = H_k \Delta \textbf{x}_k^- + \textbf{v}_k,   H_k = \frac{\partial h_k}{\partial \textbf{x}} \vert_{\hat{\textbf{x}}_k^-}
\end{equation}

The update step can then be adapted to estimate $\Delta \textbf{x}$ such as

\begin{subequations}
    \begin{align}
        K_k = P_k^- H_k^T [ H_k P_K^- H_k^T + R_k]^{-1} \\
        \Delta \hat{\textbf{x}}_{k}^+ = K_k [\textbf{y}_k - h_k(\textbf{x}_k^{-})] \label{eq:update-2}\\
        \hat{\textbf{x}}_k^+ = \hat{\textbf{x}}_k^- + \Delta \hat{\textbf{x}}_{k}^+ \\
        P_k^+ = [I-K_k H_k ]P_k^-
    \end{align}
    \label{eq:ekf-update}
\end{subequations}
where $K_k$ and $P_k^+$ come from \ref{section:lkf}. The Update step is then characterized as follows.
When a measurement is available at time $t_k$, the uncertainty between the true state
and the estimate $ \Delta \textbf{x}_k^-$ is updated using \ref{eq:update-2} as $ \Delta \textbf{x}_k^+$ . This is called "full-reset" since 
the uncertainty is not propagated itself, but through the state estimate. 
The state estimate $\hat{\textbf{x}}_k^-$ is then improved and yields $\hat{\textbf{x}}_k^+$, that will
propagated to the next iteration. The Update step, along with the Prediction step, are summarized in \textcolor{red}{ref}.

\end{comment}

\input{Thesis_MEKF}


\subsection{Complementary filter}
The Complementary filter (CF) aims to estimate the attitude and gyro bias in a simple way - it was developed with the goal of run in low-power Microcontroller units (MCU). 
This filter was first developed in \cite{mahonyComplementaryFilterDesign2005} and was later reformulated in \cite{mahonyNonlinearComplementaryFilters2008} to address computational overhead due to online attitude reconstruction. 
This filter estimates the rotation from $\mathcal{B}$ to $\mathcal{I}$, as opposed to the previous designation, such that 
\begin{equation}
    \mathcal{R} = A^T = [R^{\mathcal{I}}_{\mathcal{B}} ]
\end{equation}

The same kinematics for the attitude matrix still apply, such that 
\begin{equation}
    \dot{\mathcal{R}} = \mathcal{R} [ \boldsymbol{\omega}_{\mathcal{B}}]_{\times} = (\mathcal{R} \boldsymbol{\omega}_{\mathcal{B}} )_{\times} \mathcal{R}
\end{equation}

The filter creates an auxiliary frame $\hat{\mathcal{B}}$ to denote the estimation of the rotation between the body and the inertial frames. 
\par The attitude estimation error matrix can be written as 
\begin{equation}
    \tilde{\mathcal{R}} = \hat{\mathcal{R}}^T \mathcal{R} 
\end{equation}
The goal of the attitude estimate is to drive $\hat{\mathcal{R}} \to \mathcal{R}$, so $\tilde{\mathcal{R}} \to I_3$. 
The estimator takes the dynamics in \eqref{eq:cf-dynamics}.
\begin{equation} 
    \dot{\hat{\mathcal{R}}} =  \hat{\mathcal{R}} ( \hat{\boldsymbol{\omega}_{\mathcal{B}}} - \boldsymbol{\beta}_g + k_p \boldsymbol{\gamma})_{\times}
    \label{eq:cf-dynamics}
\end{equation}
where $k_p$ is a positive scalar gain.
The $\boldsymbol{\gamma}$ term can be computed in a number of ways: either reconstructing an attitude matrix $\mathcal{R}_y$ from the sensors to then compute $\tilde{\mathcal{R}}$ 
(direct approach) or the use of the estimated rotation $\hat{\mathcal{R}}$ to feedback and compute $\tilde{\mathcal{R}}$. This is the preferential approach since it 
doesn't propagate errors through the reconstruction of $\mathcal{R}_y$. 
%It has the diagram block in Fig. \ref{} and expression for $\boldsymbol{\gamma}$ as 
The expression for $\boldsymbol{\gamma}$ is 
\begin{equation}
    \boldsymbol{\gamma} = \sum_{i=1}^N k_i [ \mathbf{b}_i \times ] A(\hat{\mathbf{q}})\mathbf{r}_i
\end{equation}
where $k_i$ is a positive scalar gain for sensor $i$. It is possible to include a bias estimation for the filter using the correction factor $\boldsymbol{\gamma}$, such as 
\begin{equation}
    \dot{\hat{\boldsymbol{\beta}_g}} = - k_g \boldsymbol{\gamma}
\end{equation}
\par Since quaternions are the preferential attitude representation, \eqref{eq:cf-dynamics} can be rewritten as 
\begin{equation}
    \dot{\hat{\mathbf{q}}} = \Omega ( \hat{\boldsymbol{\omega}_{\mathcal{B}}} - \boldsymbol{\beta} + k_p \boldsymbol{\gamma}) \otimes \hat{\mathbf{q}}
\end{equation}
where $\mathbf{q}$ represents the rotation from $\mathcal{I}$ to $\mathcal{B}$. 

\begin{comment}


    ------ old Linear----



%%%%% FALAR DA MATRIX D?

The Kalman Filter algorithm starts by expanding the linear dynamic model in \ref{eq:system}
with noise terms

\begin{equation}
    \textbf{x}_{k+1} = \Phi \textbf{x}_{k} + \textbf{$\Gamma$ u }_{k} + w_{k}
    \label{eq:discrete-transition-1st}
\end{equation}


\begin{equation}
    \textbf{y}_{k} = H\textbf{x}_{k} + v_{k}
\label{eq:general-obs-vec-linear-ss-noise}
\end{equation}

where $w_{k}$ is a zero-mean $p$ x 1 Gaussian white-noise process vector with spectral density
$Q_{k}$ noise and $v_{k}$ is a zero-mean $m$ x 1 Gaussian white-noise process vector with
covariance $R_{k}$. 

The estimate is simply given by
\begin{equation}
    \hat{\textbf{x}}_{k+1} = \Phi \hat{\textbf{x}}_{k} + \textbf{$\Gamma$ u }_{k}
    \label{eq:discrete-transition-1st}
\end{equation}
A residual error is defined as the difference between the true value and the estimate:

\begin{equation}
    \epsilon_k = \textbf{x}_{k} - \hat{\textbf{x}}_{k}
    \label{eq:discrete-residual}
\end{equation}

The covariance of the residual error is defined as

\begin{equation}
    P = \{ \epsilon_k \epsilon_k^T \} 
    \label{eq:discrete-residual-covariance}
\end{equation}
and its propagation is given as


    ------ end of old Linear----




\end{comment}    