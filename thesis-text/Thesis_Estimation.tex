\chapter{Estimation}
\label{chapter:estimation}

Before attempting the pointing maneuver, attitude knowledge is essential. 
Sensors are used to convey information about the environment by providing measurements, such as the angular rate or the direction of the Sun. 
The technologies used have been subject of study recently and major improvements have been achieved, where SWaP has been minimized whilst maintaining (or even improving) their performance capabilities.
However, the measurements provided by the sensors are corrupted with various types of noise and by themselves are not sufficient for attitude determination.
Algorithms are used to fuse the sensors' measurements and filter the high and low-frequency noise. 
In this process, an estimate of the measurements, based on the satellite's location is necessary. The available sensors are presented in Section \ref{section:Sensors}.

\par Multiple methods have been developed to give an estimate of attitude, divided in two approaches: static and recursive methods. 
The first provide an estimate when two measurements are available using deterministic methods. 
The latter use stochastic methods and combines the model of the systems with corrections through measurements. 
The inclusion of the measurements serves to mitigate the model uncertainties and to compensate for unmodelled disturbances. 
This coalition makes these types of methods more effective. In Section \ref{section:est-algo} the estimation methods are discussed and in Section \ref{section:determination} the attitude determination methods will be presented.

\section{Estimation Sensors}
\label{section:Sensors}
The sensors used are the interface between the environment
and the estimation algorithms, which through (noisy) measurements deliver information on the
behaviour of the satellite or physical phenomena. 
Technological advancements have allowed for the use of a wider range of sensors to be installed in CubeSats, whilst still maintaining a high accuracy. 

\par Each sensor possesses its advantages and shortcomings and before choosing the estimation hardware to
be used in a specific mission, possible candidates should be reviewed. Sensors could be separated in
two categories, regarding if they measure the motion of the satellite, called inertial, or if
they are used to derive heading information from other sources, such as the sun or the Earth's
magnetic field. 

\subsection{Gyroscope}
\par When it comes to inertial sensors, the most common is the gyroscope
\cite{carraraITASATCubeSatDevelopment2017}, used to
measure angular velocity by angular displacement change over a period of time, in order to predict attitude changes between estimates. There are various
types of gyroscopes available, ranging from the more accessible Micro-Electro-Mechanical Systems (MEMS) \cite{chengDesignAttitudeMeasurement2014}, to the mid-range
fibre-optic
gyros (FOGs) and most accurate ring laser gyros (RLGs). 
Knowledge of attitude variation is essential when using estimation algorithms that use the satellite dynamics.
The selection criteria for this sensor are the required accuracy, bias and drift since they will a cause cumulative error that is not
possible to ignore due to the absence of an absolute measurement.

\subsection{Magnetometer}
\par Regarding positioning sensors, which measure changes in the environment, on the lower side of
the spectrum when it comes to price, there is the magnetometer. It can be used by itself
\cite{challaMagnetometeronlyAttitudeRate} in order to estimate attitude and rate, but it is mostly used in
complement with other sensors
\cite{adnaneReliableKalmanFiltering2018}. It provides a measurement of the magnetic field in the
sensor's axis that must be compared to an estimate of the magnetic field which is obtained with
estimates of International Geomagnetic Reference Field
(IGRF), based on an estimation of 
instantaneous orbital position. 

\subsection{Sun Sensor}
\par The Sun sensors take advantage of the brightest reference in space orbit to generate a sun
vector in relation to the satellite. Since the measured Sun vector is the only point of reference, it doesn't provide
complete information by itself for attitude estimation, and should be crossed with a non-collinear vector, as the
Earth vector \cite{wuNanoSatelliteAttitude2016}, obtained through other various types of sensors. The Earth's albedo can also be a
source of measurement disturbance and must be taken into account
\cite{cilden-gulerAttitudeEstimationAlbedo2021}.  
\par There are different types of builds
when it comes to power, whilst passive sensors use the irradiated energy to output the result,
active sensors use multidetector arrays to process the digital image of incidence. The most relevant
shortcoming of sun sensors is the unavailability of measurements whilst the sun is not in the satellite's
FOV. The eclipse will hinder the performance of algorithms highly reliant on these sensors. 
Sun sensors can also be divided in fine or course based on their accuracy. 

\subsection{Star Tracker}
\par Although the sun is the closest star to the Earth, it doesn't mean other can't be used as
reference. Star sensors are cameras mounted on the satellite that compare the image being received with
a reference map and estimate the attitude. Since the position of the stars can be measured with
high accuracy, it is possible to obtain sub-arcminute attitude estimation
\cite{gutierrezIntroducingSOSTUltraLowCost2020} and can be used as the only type of position-sensing
sensor \cite{maHighAccuracyLowCostAttitude2020}. Star trackers use the 2D information of the stars within
their FOV, which allows for absolute attitude estimation by itself and does not rely on other
measurements as the Sun sensors.  
\par The star imaging technology can also be used to create a stellar gyroscope. It compares two
sequential images and by correlating them to the star maps, it is able to calculate a difference in
rotation on the object on which it is mounted. This method is highly reliable and accurate since it
is a differential method (removes noise or dead pixels) and is able to cancel accumulated bias while
the body is detected as stationary. 

\subsection{Horizon Sensors}
These sensors detect the Earth's horizon through infrared sensors. Although there are
various methods of doing so, the most used is edge detection where the sensor is positioned
to detect the edge of the earth. This information can be used to compute the nadir vector.
There are some unavoidable errors in the horizon measurement due to the earth's
atmosphere, which irradiates decreasing intensity from the true horizon. 

\subsection{Global Navigation Satellite System}  
Global Navigation Satellite System (GNSS) measurements cannot be used directly to
estimate attitude but can be useful to have a very good estimate of the satellite position, which
could be used in conjunction with other measurements. In the specific case of GNSS, the most utilized
navigation system, the cost is high so it is not usually present in a Nano Satellite.

\subsection{Beam detection}
The laser beam emitted from the GS marks not only the desired orientation of the satellite, but also is another way to
gain knowledge of the Earth's position. The satellite is composed by a telescope that receives and conditions the optical signal to a 
quad-cell that processes it. Although the QuantSat's optic payload is still under development, its performance can be estimated based
on other implementations, as discussed in the section below. In order to use the Beam's attitude measurement, the
optical signal must be within the FOV of the telescope lens, i.e. when the pointing accuracy is lower than
the FOV.



\subsection{Sensors Selection}
ADCS systems are an essential component of a satellite so a lot of different combinations of sensors have been tested.
In order to choose the appropriate hardware to suit the mission, not
only the pointing requirements must be taken into account but also the 3U CubeSat restrictions, such
as volume and weight. The optic payload occupies a large volume in the CubeSats. When it comes to available
sensors, the use of a GNSS system and the Beam Detection have been established since they are included in the payload
itself to provide timing and quantum communications respectively.
 
\par Although requirements compliance verification can only be done in simulation, literature
provides a few guidelines that can be used. They are by no means an objective approach and are highly
dependent on the sensors' relative quality and the algorithms used to process the data. Although
pointing accuracy is the principal selection criteria, mission requirements (e.g. Earth pointing in
eclipse) and other influences (e.g. redundancy, fault tolerance) must be taken into account.
These guidelines are presented in table \ref{tab:sensor_review}.


\begin{table}[H]
  \begin{tabular}{|l|l|}
  \hline
  \textbf{Required accuracy ($^{\circ}$)} & \textbf{Effect on ADCS}  \\\hline
  $>$ 5 & Major cost savings, sun sensors and magnetometers adequate \\\hline
  1-5 & Sun sensors and horizon sensors adequate, magnetometers useful  \\\hline
  0.1 - 1 & Star tracker or horizon sensors, gyroscope, magnetometers for light satellite  \\\hline
  $<$ 0.1 & Star tracker needed  \\\hline
\end{tabular}
\caption[Attitude sensors comparison.]{Comparison of attitude sensors based on
\cite{wertzSpaceMissionAnalysis1999}}
\label{tab:sensor_review}
\end{table}

The table above does not include the beacon detection since it is not a conventional sensor. In \cite{nguyenDevelopmentPointingAcquisition2015},
the attitude knowledge accuracy of a similar system is evaluated to be of 0.0015 $\degree$ (mean), which is comparable
to the typical accuracy of a Star tracker and superior to an Earth Sensor 
\cite{gutierrezIntroducingSOSTUltraLowCost2020}. With this in mind, and considering that these are 
costly and redundant sensors, they are disregarded in the implementation.


\par Since the proposed ADCS works in two separate stages, two distinct analyses could be conducted. 
However, if an estimation error of less that $10^{\circ}$ can be guaranteed, the first staged can be disregarded.
This work will focus on this goal.

\par For the sensors, when the required accuracy is not high, sun sensors and magnetometers are enough
to obtain all attitude information. The downfall of this sensor suite is the eclipse, which
will be addressed further on. For more accurate systems, horizon sensors or even star
trackers must be used, with the addition of the gyroscope for attitude propagation. 


\subsection{Sensors Model}
\label{subsection:sensors-model}
\textbf{Gyroscope}
\par The angular velocity measurement $\hat{\boldsymbol{\omega}}$ from the gyroscope is considered to be corrupted by noise,
$\eta_{arw}$, as well as a slow-varying bias, $\boldsymbol{\beta}_{g}$. As such, the gyroscope model is given as 

\begin{equation}
\hat{\boldsymbol{\omega}} = \boldsymbol{\omega} + \boldsymbol{\beta}_{g} + \eta_{arw}
\label{eq:gyro-w}
\end{equation}

\begin{equation}
\dot{\boldsymbol{\beta}_{g}} = 0.
\label{eq:gyro-b}
\end{equation}
 
The sensor perturbation $\eta_{arw}$ can be interpreted as an independent zero-mean Gaussian white-noise process.
\vspace{0.4cm}
\par \textbf{Magnetometer}
\par The magnetic field measurement is defined as the true magnetic field vectors, corrupted by a bias $\boldsymbol{\beta}_{B}$
and Gaussian white-noise $\eta_{B}$, such as

\begin{equation}
  \hat{\textbf{B}} = \textbf{B} + \boldsymbol{\beta}_{B} + \eta_{B}
  \label{eq:magnetometer}
\end{equation}
\vspace{0.4cm}
\textbf{Beacon}
\par The optical measurement for the GS laser beacon is given as 
\begin{equation}
  \hat{b}_{b} = A_{\eta_{b}}b_{b} = A(\textbf{$e_{1}$}, \phi_{b})A(\textbf{$e_{2}$}, \theta_{b})A(\textbf{$e_{3}$}, \xi_{b})b_{b}
  \label{eq:beacon}
\end{equation}

where $b_{b}$ is the normalized direction vector of the GS, in the body frame.
The euler angles used to generate the noise rotation matrix $A_{\eta_{b}}$ can be characterized as Gaussian white-noise.
This measurement model is based on Star Trackers, converted from quaternions to rotation matrix \cite{baeSatelliteAttitudeDetermination2010}.
\vspace{0.4cm}
\par \textbf{Sun sensors}
\par Similarly to the Beacon, the Sun sensor measurements are given as
\begin{equation}
  \hat{b}_{s} = A_{\eta_{s}}b_{s} = A(\textbf{$e_{1}$}, \phi_{s})A(\textbf{$e_{2}$}, \theta_{s})A(\textbf{$e_{3}$}, \xi_{s})b_{s}
  \label{eq:sun-sensor-model}
\end{equation}
where $b_{sun}$ is the normalized Sun vector, in the body frame. 
The euler angles used to generate the noise rotation matrix 
$A_{\eta_{sun}}$ can be characterized as Gaussian white-noise, with different variances
for boresight and cross-boresight axis.
\vspace{0.4cm}
\par \textbf{GNSS} 
\par The GNSS measurement is defined in the Inertial frame, given by the true position $\mathbf{p}_{sat}$ of the sensor with a sphere of uncertainty, such as:
\begin{equation}
  \hat{\mathbf{r}}_{sat} = \mathbf{r}_{sat} + \begin{bmatrix}
    r \sin(\phi)\cos(\theta) \\
    r \sin(\phi)\sin(\theta) \\
    r \cos(\phi)
  \end{bmatrix}
\end{equation}

% E\{ \phi_s \} = E\{ \theta_s \} = E\{ \xi_s \} = 0 \\
% E\{ \phi_s \phi_s^T \} = E\{ \theta_s \theta_s^T \} = \eta_{1} \quad E\{ \xi_s \xi_s^T \} = \eta_{2}
\input{Thesis_KF}

\section{Attitude determination methods}
\label{section:determination}
Even though these methods are not the main focus of this thesis and usually offer lower accuracy \cite{computersciencescorporationSpacecraftAttitudeDetermination1978}, an implementation is proposed to
serve as a comparison for the method above. The TRIaxial Attitude Determination (TRIAD) method was chosen since it uses two measurements, the maximum simultaneous available 
measurements due to mission requirements.
\par The attitude matrix to be determined is the matrix that rotates vectors from the reference frame $I$ to the satellite body frame $B$, such that
\begin{equation}
  A \mathbf{r}_i = \mathbf{b}_i \quad \text{for} \quad i = 1, 2
  \label{eq:est-triad-1}
\end{equation}

This is only true for error-free measurement, since \eqref{eq:est-triad-1} implies that 
\begin{equation}
  \mathbf{b}_1 \cdot \mathbf{b}_2 = (A \mathbf{r}_1) \cdot (A \mathbf{r}_2) = \mathbf{r}_1^T A^T A \mathbf{r}_2 = \mathbf{r}_1 \cdot \mathbf{r}_2
\end{equation}
The TRIAD algorithm assumes that one vector (e.g. $\mathbf{b}_1$) is more accurately determined than the other, such that $A \mathbf{r}_1 = \mathbf{b}_1$ exactly,
but $A \mathbf{r}_2 = \mathbf{b}_2$ only approximately. 
\par It is assumed that the orthonormal right-handed triad of vectors $\{ \mathbf{v}_1 , \mathbf{v}_2 , \mathbf{v}_3 \}$ in the reference frame has a corresponding orthonormal
right-handed triad of vectors $\{ \mathbf{w}_1 , \mathbf{w}_2 , \mathbf{w}_3 \}$ in the satellite body frame and form the attitude matrix
\begin{equation}
  A = [ \mathbf{w}_1 \mathbf{w}_2 \mathbf{w}_3 ] [ \mathbf{v}_1 \mathbf{v}_2 \mathbf{v}_3 ]^T = \sum_{i=1}^3 \mathbf{w}_i \mathbf{v}_i^T 
\end{equation} 
The algorithm forms each triad from the vectors in both frames, such that 
\begin{subequations}
  \begin{equation}
    \mathbf{v}_1 = \mathbf{r}_1 , \quad \mathbf{v}_2 = \mathbf{r}_{\times} \equiv \frac{\mathbf{r}_1 \times \mathbf{r}_2 }{|| \mathbf{r}_1 \times \mathbf{r}_2 ||}, \quad \mathbf{v}_3 = \mathbf{r}_1 \times \mathbf{r}_{\times}
  \end{equation}
  \begin{equation}
    \mathbf{w}_1 = \mathbf{b}_1 , \quad \mathbf{w}_2 = \mathbf{b}_{\times} \equiv \frac{\mathbf{b}_1 \times \mathbf{b}_2 }{|| \mathbf{b}_1 \times \mathbf{b}_2 ||}, \quad \mathbf{w}_3 = \mathbf{b}_1 \times \mathbf{b}_{\times}
  \end{equation}
\end{subequations}
The estimate of the attitude matrix is calculated as such 
\begin{equation}
  \hat{A}_{TRIAD} = \mathbf{b}_1 \mathbf{r}_1^T + (\mathbf{b}_1 \times \mathbf{b}_{\times})(\mathbf{r}_1 \times \mathbf{r}_{\times})^T + \mathbf{b}_{\times} \mathbf{r}_{\times}^T
\end{equation}
This attitude matrix is invalid if the reference or the observed vectors are parallel or antiparallel. 

\begin{comment}
  \begin{figure}[!htb]
    \centering
    \includegraphics[width=\textwidth]{Figures/adcs-levels.png}
    \caption[Pointing Stages.]{ADCS requirements and designated sensors and actuators}
    \label{fig:adcs-levels}
  \end{figure}

\begin{table}[H]
\begin{tabular}{|l|l|l|l|}
\hline
    Study & Algorithm(s) & Sensor(s) & Accuracy \\\hline
     &  &  & \\\hline
\end{tabular}
\caption[Estimation algorithms comparision]{Comparision of estimation algorithms}
\label{tab:estimation_algorithms}
\end{table}
\begin{figure}[!htb]
  \centering
  \includegraphics[width=\textwidth]{Figures/earth-pointing-adcs.png}
  \caption[Pointing Stages.]{Earth Pointing ADCS - temporary as picture.}
  \label{fig:earth-pointing}
\end{figure}

A comparision of the different attitude sensor characteristics can be seen in
Tab.table \ref{tab:sensor_review}.
\begin{table}[H]
\begin{tabular}{|l|l|l|l|l|l|}
\hline
Attitude sensors & Typical precision    & Frequency (Hz) & Volume (1U) & Avg. power (W) & Avg. cost (USD) \\\hline
Gyroscope        & Drift rate of 1°/hr  & 100            & 1/1000      & 0.02              & 30 \\\hline
Magnetometer     & 0.5° - 3°            & 10             & 1/1000      & 0.02              & 15 \\\hline
Earth Sensor     & 0.25°                & 1              & 1/20        & 0.1               & 15000 \\\hline
Fine Sun Sensor  & 6'                   & 5              & 1/50        & 0.04              & 12000 \\\hline
Star Tracker     & 2" (cross-boresight) & 10             & 1/4         & \textless{}1      & 33000 \\\hline
\end{tabular}
    \caption[Attitude sensors comparision]{Comparision of attitude sensors based on
    \cite{gutierrezIntroducingSOSTUltraLowCost2020}}
\label{tab:sensor_review}
\end{table}


\begin{subequations}
  \begin{align}
    \phi_{beacon} = 
  \end{align}
\end{subequations}

\end{comment}