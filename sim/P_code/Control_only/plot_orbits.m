%range = 3800:5100; % sampling time 0.5 sec
range = 2000:6000;
% p_sat = out.ScopeData2.signals(1).values(range,:); % satellite position
% p_gs = out.ScopeData2.signals(2).values(range,:); % ground station position
% p_dif = out.ScopeData2.signals(3).values(range,:); % difference ?
p_sat = out.satPos.Data(range, :); % satellite position
p_gs = out.gsPos.Data(range, :); % ground station position
p_dif = out.difPos.Data(range, :); % difference ?
coef = out.coef.Data(range,:);

figure(23)
plot3(p_sat(:,1),p_sat(:,2),p_sat(:,3),'m')
 axis([-7e6 7e6 -7e6 7e6 -7e6 7e6])
grid on
hold on
plot3(p_gs(:,1),p_gs(:,2),p_gs(:,3),'k')
%plot3([0 2e6],[0 0],[0 0],'r.-');
%plot3([0 0],[0 2e6],[0 0],'g.-');
%plot3([0 0],[0 0],[0 2e6],'b.-');
for i = 1:150:length(range)-150
    v = cross(p_sat(i,:),p_sat(i+150,:))/1e7; % sat axis of rotation
%    plot3([p_sat(i,1) p_sat(i,1)+v(1)],[p_sat(i,2) p_sat(i,2)+v(2)],[p_sat(i,3) p_sat(i,3)+v(3)],'m');
%    v = v*10;
%    plot3([0 v(1)],[0 v(2)],[0 v(3)], 'm');

    u1 = v/norm(v);
    %u2 = [0;1;0];
    u2 = [0;-1;0];
    u = cross(u1,u2);
    ssc = skew(u);
    %align sat y axis with sat axis of rotation
    R1 = (eye(3) + ssc + ssc^2*(1-dot(u1,u2))/(norm(u))^2);
    x = R1'*[5e5;0;0];
    y = R1'*[0;5e5;0];
    z = R1'*[0;0;5e5];
%     plot3([p_sat(i,1) p_sat(i,1)+x(1)],[p_sat(i,2) p_sat(i,2)+x(2)],[p_sat(i,3) p_sat(i,3)+x(3)],'r--');
%     plot3([p_sat(i,1) p_sat(i,1)+y(1)],[p_sat(i,2) p_sat(i,2)+y(2)],[p_sat(i,3) p_sat(i,3)+y(3)],'g--');
%     plot3([p_sat(i,1) p_sat(i,1)+z(1)],[p_sat(i,2) p_sat(i,2)+z(2)],[p_sat(i,3) p_sat(i,3)+z(3)],'b--');
    v = p_sat(i+1,:) - p_sat(i,:);
    v = v/norm(v);
    %u1 = R1*p_dif(i,:)'/norm(p_dif(i,:));
    %u2 = [0;0;-1];
    %u = cross(u1,u2);
    u1 = R1*v';
    u2 = [1;0;0];
    u = cross(u1,u2);
    ssc = skew(u);
    % align sat z axis to point to gs
    R2 = (eye(3) + ssc + ssc^2*(1-dot(u1,u2))/(norm(u))^2)*R1; 
    
    x = R2'*[5e5;0;0];
    y = R2'*[0;5e5;0];
    z = R2'*[0;0;5e5];
    plot3([p_sat(i,1) p_sat(i,1)+x(1)],[p_sat(i,2) p_sat(i,2)+x(2)],[p_sat(i,3) p_sat(i,3)+x(3)],'r--');
    plot3([p_sat(i,1) p_sat(i,1)+y(1)],[p_sat(i,2) p_sat(i,2)+y(2)],[p_sat(i,3) p_sat(i,3)+y(3)],'g--');
    plot3([p_sat(i,1) p_sat(i,1)+z(1)],[p_sat(i,2) p_sat(i,2)+z(2)],[p_sat(i,3) p_sat(i,3)+z(3)],'b--');
    
    %%%%%% if coef 0 dont do it
    if(coef(i) ~= 0)
    u1 = R2*p_dif(i,:)'/norm(p_dif(i,:));
    u2 = [0;0;-1];
    u = cross(u1,u2);
    u = u*coef(i);
    ssc = skew(u);
    % align sat z axis to point to gs
    R = (eye(3) + ssc + ssc^2*(1-dot(u1,u2))/(norm(u))^2)*R2; 
    x = R'*[5e5;0;0];
    y = R'*[0;5e5;0];
    z = R'*[0;0;5e5];
    else
       R = R2; 
    end

    plot3([p_sat(i,1) p_sat(i,1)+x(1)],[p_sat(i,2) p_sat(i,2)+x(2)],[p_sat(i,3) p_sat(i,3)+x(3)],'r.-');
    plot3([p_sat(i,1) p_sat(i,1)+y(1)],[p_sat(i,2) p_sat(i,2)+y(2)],[p_sat(i,3) p_sat(i,3)+y(3)],'g.-');
    plot3([p_sat(i,1) p_sat(i,1)+z(1)],[p_sat(i,2) p_sat(i,2)+z(2)],[p_sat(i,3) p_sat(i,3)+z(3)],'b.-');
    plot3([p_sat(i,1) p_gs(i,1)],[p_sat(i,2) p_gs(i,2)],[p_sat(i,3) p_gs(i,3)],'k:.');
aux = R'-inv(R); 
if ((sum(all(aux<0.01)) ~=3 ) || abs((abs(det(R))-1))>0.01 )
  a = 1 
end
    
end

% % label
% % legend("")
% % for p=1:1
% %     x = linspace(0,1, 10);
% %     cols =  ["red", "green", "blue", "red", "green", "blue"];
% %     line_type = ["-", "-", "-", ".-", ".-", ".-"];
% %     for i = 1:9
% %         y = x + i/3; 
% %         col_i = cols(mod(i,3) + 1);
% %         line_type_i = line_type(mod(i,3) + 1);
% %         plot(x,y, line_type_i, 'Color', col_i)
% %         hold on
% %     end
% %     legend('','Location', 'eastoutside')
% %     col_names = ["x-axis", "y-axis", "z-axis", "x", "y", "z"];
% %     for j =1:length(col_names)
% %         plot([NaN NaN], [NaN NaN], 'Color', cols(j), 'DisplayName', col_names(j))
% %     end
% % end

hold off


% 
% figure(33)
% plot3(p_dif(:,1),p_dif(:,2),p_dif(:,3))
% hold on
% grid on
% 
% R= eye(3);
% j=1;
% for i = 1:150:length(range)-150
%     u1 = p_dif(i,:)/norm(p_dif(i,:));
%     if i == 1
%         u2 = [0;0;1];
%     else
%         u2 = p_dif(i-150,:)/norm(p_dif(i-150,:));
%     end
%     u = cross(u1,u2);
%     ssc = skew(u);
%     R = R*(eye(3) + ssc + ssc^2*(1-dot(u1,u2))/(norm(u))^2);
%     
%     x = R'*[5e5;0;0];
%     y = R'*[0;5e5;0];
%     z = R'*[0;0;5e5];
% 
%     plot3([0 p_dif(i,1)],[0 p_dif(i,2)],[0 p_dif(i,3)],'c');
%     v1(j,:) = cross(p_dif(i,:),[1;0;0]);%p_dif(i+150,:))/1e5;
%     plot3([0 v1(j,1)],[0 v1(j,2)],[0 v1(j,3)],'m');
%     plot3([p_dif(i,1) p_dif(i,1)+x(1)],[p_dif(i,2) p_dif(i,2)+x(2)],[p_dif(i,3) p_dif(i,3)+x(3)],'r');
%     plot3([p_dif(i,1) p_dif(i,1)+y(1)],[p_dif(i,2) p_dif(i,2)+y(2)],[p_dif(i,3) p_dif(i,3)+y(3)],'g');
%     plot3([p_dif(i,1) p_dif(i,1)+z(1)],[p_dif(i,2) p_dif(i,2)+z(2)],[p_dif(i,3) p_dif(i,3)+z(3)],'b');
%     j = j+1;
% end
%     plot3([0 2e6],[0 0],[0 0],'r.-');
%     plot3([0 0],[0 2e6],[0 0],'g.-');
%     plot3([0 0],[0 0],[0 2e6],'b.-');
% %plot3([0 v(end,1)],[0 v(end,2)],[0 v(end,3)]);
% %v(j,:) = cross(p_dif(1,:),p_dif(end,:))/1e5;
% %plot3([0 v(j,1)],[0 v(j,2)],[0 v(j,3)]);
% 
% hold off

% function R = calculateDesiredRotmNET(u1, u2)
% v = cross(u1,u2);
% ssc = [0 -v(3) v(2); v(3) 0 -v(1); -v(2) v(1) 0];
% R = eye(3) + ssc + ssc^2*(1-dot(u1,u2))/(norm(v))^2;
% 
% aux = R'-inv(R); 
% if ((sum(all(aux<0.01)) ~=3 ) || abs((abs(det(R))-1))>0.01 )
%   a = 1 
% end