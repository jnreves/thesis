function [quat_ham] = ham2jpl(quat_jpl)
quat_ham = [quat_jpl(4) quat_jpl(1) quat_jpl(2) quat_jpl(3)]';
end
