Control.controlInst     = 0;
    Control.period = 0.5;
Sat.I = diag([6.65, 33.25, 33.25])*10^-3; %kg*m^2

    Control.k1 = 0.0005;%8.5e-04;
    Control.k2 = 0.008;%0.0005;
    Control.alpha1 = 0.6;
    Control.alpha2 = 2*Control.alpha1/(Control.alpha1+1);
    Control.sigma = 0.3;
    
    Sat.q = [-0.547212863050710,0.325040552001621,0.738795346989299,0.221558473846721];   %Initial attitude.
    
    Sat.W = [0.00592284901709164,-0.00139271885446594,0.00625576123743776]; % Initial angular velocity, changes every time a script is run
    
    load('Beacon.mat');
     load('SatPosition_v1_50000.mat');
    
    Orbit.SatPosECI = SatPosition_out.SatPosECI;