ini = [0.94, -0.32, 0.14, 0];
Sat.q = ini/norm(ini);
Control.k1 = 5.500e-04;
Control.k2 = 0.0008;

baseRSun = eye(3); %0.01
baseRMag = diag([0.16, 0.16, 0.45]); %0.0055
baseRBeacon = diag([0.0001, 0.0001, 0.0003]); %0.001


sun_vec = linspace(0.0005, 0.0015, 3);
mag_vec = linspace(0.0005, 0.0006, 3);
beacon_vec = linspace(0.000001, 0.001, 3);



for k=1:size(beacon_vec,2)
    mekf.R_beacon = beacon_vec(k)*baseRBeacon;
    for j=1:size(sun_vec,2)
        mekf.R_sun = sun_vec(j)*baseRSun;
        for i=1:size(mag_vec,2)
            mekf.R_mag = mag_vec(i)*baseRMag;
            sim('ADCS_Model_Complete');
            simout(k, j,i)= ans.est_error;
            figure;
            plot(simout(k, j,i));
            title("mult for beacon =" + num2str(beacon_vec(k)) + " sun =" + num2str(sun_vec(j))+ " mag =" + num2str(mag_vec(i))); 
            min_ang(k, j,i) = min(simout(k, j,i).Data);
            avg(k, j,i) = sum(simout(k, j,i).Data,'omitnan')/(nnz(~isnan(simout(k, j,i).Data)));
        end
    end
end


