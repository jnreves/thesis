function magFieldNED = Compute_Earth_Magnetic_Field(G,H,phi,lambda,rSat,nMax)
%% Input Data
radiusEarth  = 6378137;
%% Recursively calculate Legendre functions
% Compute the Gauss-normalized associated Legendre functions like in the
% WMM code
x=sin(phi);
numTerms = 100; %((n_max + 1) * (n_max + 2) / 2);
Pcup = ones(numTerms,1);
dPcup = zeros(numTerms,1);
%sin (geocentric latitude) - sin_phi 
z = sqrt((1-x)*(1+x));
for n=1:nMax
    for m=0:n
        index = (n*(n+1)/2+m)+1;
        if (n == m)
            index1 = (n-1)*n/2 + m;
            Pcup(index) = z * Pcup(index1);
            dPcup(index) = z * dPcup(index1) + x * Pcup(index1);
        elseif (n == 1 && m == 0)
            index1 = (n-1)*n/2 + m + 1;
            Pcup(index) = x * Pcup(index1);
            dPcup(index) = x * dPcup(index1) - z * Pcup(index1);
        elseif(n > 1 && n ~= m)
            index1 = (n-2)*(n-1)/2 + m + 1;
            index2 = (n-1)*n/2 + m + 1;
            if (m > n - 2)
                Pcup(index) = x * Pcup(index2);
                dPcup(index) = x * dPcup(index2) - z * Pcup(index2);
            else
                k = ((n-1)^2 - m^2)/((2*n-1)*(2*n-3));
                Pcup(index) = x * Pcup(index2) - k * Pcup(index1);
                dPcup(index) = x * dPcup(index2) - z * Pcup(index2) - k * dPcup(index1);
            end
        end
    end
end

% Compute the ratios between the Schmidt quasi-normalized associated Legendre
% functions and the Gauss-normalized version

schmidtQuasiNorm = ones(numTerms,1);
for n=1:nMax
    index = (n*(n+1)/2) + 1;
    index1 = (n-1)*n/2  + 1;
    % for m = 0
    schmidtQuasiNorm(index) = schmidtQuasiNorm(index1)*(2*n-1)/n;    
    for m=1:n
        index = (n*(n+1)/2+m+1);
        index1 = (n*(n+1)/2+m);
        if m==1
            aux = 2;
        else
            aux = 1;
        end
        schmidtQuasiNorm(index) = schmidtQuasiNorm(index1) * sqrt(((n-m+1)*aux)/(n + m));
    end
end

% Converts the  Gauss-normalized associated Legendre functions to the Schmidt quasi-normalized version 
% using pre-computed relation stored in the variable schmidtQuasiNorm 

for n=1:nMax    
    for m=0:n        
        index = (n*(n+1)/2+m+1);
        Pcup(index) = Pcup(index) * schmidtQuasiNorm(index);
        dPcup(index) = -dPcup(index) * schmidtQuasiNorm(index);
        % The sign is changed since the new WMM routines use derivative with respect to latitude
        % insted of co-latitude
    end
end
%% Recursively calculate Br, Bt and ...
X=0;
Y=0;
Z=0;

for n=1:nMax
    K = (radiusEarth/rSat)^(n+2);
    for m=0:n
        index = (n*(n+1)/2+m+1);

        % Compute sum_X, sum_Y e sum_Z
        X = X -   (G(n,m+1)*cos(m*lambda)+H(n,m+1)*sin(m*lambda))*dPcup(index)*K;
        Y = Y +   (G(n,m+1)*sin(m*lambda)-H(n,m+1)*cos(m*lambda))*Pcup(index)*K/cos(phi)*m;
        Z = Z -   (G(n,m+1)*cos(m*lambda)+H(n,m+1)*sin(m*lambda))*Pcup(index)*K*(n+1);
    end
    
end
magFieldNED = [X;Y;Z];
end



