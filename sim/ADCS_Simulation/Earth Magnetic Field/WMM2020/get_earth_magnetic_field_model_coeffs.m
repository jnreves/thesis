%% Define the date here
date = 2022.5; 


%% Read coefficients and generate g_nm and h_nm - validated
initDate = 2020.0;
coeffs = readmatrix('WMM2020_coeff.csv');
n = coeffs(:,1);
m = coeffs(:,2);
G0 = coeffs(:,3);
H0 = coeffs(:,4);
Gdot = coeffs(:,5);
Hdot = coeffs(:,6);

G = zeros(12,13);
H = zeros(12,13);
for i=1:length(G0)
    G(n(i),m(i)+1) = G0(i) + (date-initDate)*Gdot(i);
    H(n(i),m(i)+1) = H0(i) + (date-initDate)*Hdot(i);
end
save('MagneticFieldModelCoeffs','G','H')