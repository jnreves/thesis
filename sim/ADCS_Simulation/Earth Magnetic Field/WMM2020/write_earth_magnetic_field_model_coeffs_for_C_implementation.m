load('MagneticFieldModelCoeffs.mat');
[sz1,sz2] = size(G);
fileID = fopen('coeffs_for_C_implementation.txt','w');
NumTerms = sz1/2*(sz2+2);

fprintf(fileID,'{0');
n=0;
m=0;
for i=1:NumTerms
    if n==m
        n=n+1;
        m=0;
    else
        m=m+1;
    end
    fprintf(fileID,',%d',G(n,m+1)*10000);
    if i/10 == ceil(i/10)
        fprintf(fileID,'\n');
    end
end
 fprintf(fileID,'};\n');
 
fprintf(fileID,'{0');
n=0;
m=0;
for i=1:NumTerms
    if n==m
        n=n+1;
        m=0;
    else
        m=m+1;
    end
    fprintf(fileID,',%d',H(n,m+1)*10000);
    if i/10 == ceil(i/10)
        fprintf(fileID,'\n');
    end
end
 fprintf(fileID,'};\n');
 