%% Save times
DataSaveSampleTimes.magFieldBody_hat = 1;
DataSaveSampleTimes.magFieldBody = 1;
DataSaveSampleTimes.magFieldECI = 1;
DataSaveSampleTimes.magFieldECI_hat = 1;

DataSaveSampleTimes.sunVecBody_hat = 1;
DataSaveSampleTimes.sunVecBody = 1;
DataSaveSampleTimes.sunPosECI = 1;

DataSaveSampleTimes.q = 1;
DataSaveSampleTimes.q_mekf = 1;

%% program
% nT
% b1 - magnetic field in body
% b1_hat - measurement of magnetic field in body based on sensor model 
% nT
% r1 - magnetic field in inertial
% r1_hat - estimation of magnetic field in inertial based on orbit
% no unit
% q - quaternion from simulation
% q_mekf - quaternion from mekf (propagation only)

b1 = ADCSModelComplete.magFieldBody.signals.values;
b1_hat = ADCSModelComplete.magFieldBody_hat.signals.values; 
r1 = ADCSModelComplete.magFieldECI.signals.values;
r1_hat = ADCSModelComplete.magFieldECI_hat.signals.values;

b2 = ADCSModelComplete.sunVecBody.signals.values;
b2_hat = ADCSModelComplete.sunVecBody_hat.signals.values; 
r2 = ADCSModelComplete.sunPosECI.signals.values;

q_in = ADCSModelComplete.q.signals.values;
q_mekf_in_sim = ADCSModelComplete.q_mekf.signals.values;

secs = 3990;
start_t = 500;
end_t = 800;
plot_time = 5:3990;
% plot_time = start_t:end_t;

for m = 1
q=zeros(secs,4);
q(:, 1) = q_in((1:secs), 2);
q(:, 2) = q_in((1:secs), 3);
q(:, 3) = q_in((1:secs), 4);
q(:, 4) = q_in((1:secs), 1);

q_mekf_sim=zeros(secs,4);
q_mekf_sim(:, 1) = q_mekf_in_sim((1:secs), 2);
q_mekf_sim(:, 2) = q_mekf_in_sim((1:secs), 3);
q_mekf_sim(:, 3) = q_mekf_in_sim((1:secs), 4);
q_mekf_sim(:, 4) = q_mekf_in_sim((1:secs), 1);

q_mekf_local=zeros(secs,4);
q_mekf_local(1:4, :) = q_mekf_sim(1:4, :);
end

iter = secs - 4;

for m= 1
mag_error_body_sensor = zeros(1, iter);
%mag_error_body_conversion = zeros(1, iter);
mag_error_eci_model = zeros(1, iter);
mag_3_error_sensor = zeros(3, iter);
mag_3_error_model = zeros(3, iter);
mag_r_error = zeros(3, iter);

sun_error_body_sensor = zeros(1, iter);
sun_3_error_sensor = zeros(3, iter);
sun_error_eci_model = zeros(1, iter);
sun_3_error_model = zeros(3, iter);
sun_error_q_rotation_vs_model = zeros(1, iter);
end

y = zeros(6, iter);
h = zeros(6, iter);
y_h = zeros(6, iter);
y_h_errors = zeros(2, iter);

for n=1:size(b1, 1)
    
    b1(n, :) = b1(n, :)/norm(b1(n, :));
    b1_hat(n, :) = b1_hat(n, :)/norm(b1_hat(n, :));
    r1(n, :) = r1(n, :)/norm(r1(n, :));
    r1_hat(n, :) = r1_hat(n, :)/norm(r1_hat(n, :));

    b2(n, :) = b2(n, :)/norm(b2(n, :));
    b2_hat(n, :) = b2_hat(n, :)/norm(b2_hat(n, :));
    r2(n, :) = r2(n, :)/norm(r2(n, :));
    
end


for j=5:secs-1
    i = j-3;
    
    % rotation matrices creation a - q; c - q_mekf 
    for r=1
    a1=q(i, 1)^2-q(i, 2)^2-q(i, 3)^2+q(i, 4)^2;
    a2=2*(q(i, 1)*q(i, 2)+q(i, 3)*q(i, 4));
    a3=2*(q(i, 1)*q(i, 3)-q(i, 2)*q(i, 4));
    a4=2*(q(i, 1)*q(i, 2)-q(i, 3)*q(i, 4));
    a5=-q(i, 1)^2+q(i, 2)^2-q(i, 3)^2+q(i, 4)^2;
    a6=2*(q(i, 2)*q(i, 3)+q(i, 1)*q(i, 4));
    a7=2*(q(i, 1)*q(i, 3)+q(i, 2)*q(i, 4));
    a8=2*(q(i, 2)*q(i, 3)-q(i, 1)*q(i, 4));
    a9=-q(i, 1)^2-q(i, 2)^2+q(i, 3)^2+q(i, 4)^2;
    a=[a1 a2 a3;a4 a5 a6;a7 a8 a9];
    
    c1=q_mekf_sim(i, 1)^2-q_mekf_sim(i, 2)^2-q_mekf_sim(i, 3)^2+q_mekf_sim(i, 4)^2;
    c2=2*(q_mekf_sim(i, 1)*q_mekf_sim(i, 2)+q_mekf_sim(i, 3)*q_mekf_sim(i, 4));
    c3=2*(q_mekf_sim(i, 1)*q_mekf_sim(i, 3)-q_mekf_sim(i, 2)*q_mekf_sim(i, 4));
    c4=2*(q_mekf_sim(i, 1)*q_mekf_sim(i, 2)-q_mekf_sim(i, 3)*q_mekf_sim(i, 4));
    c5=-q_mekf_sim(i, 1)^2+q_mekf_sim(i, 2)^2-q_mekf_sim(i, 3)^2+q_mekf_sim(i, 4)^2;
    c6=2*(q_mekf_sim(i, 2)*q_mekf_sim(i, 3)+q_mekf_sim(i, 1)*q_mekf_sim(i, 4));
    c7=2*(q_mekf_sim(i, 1)*q_mekf_sim(i, 3)+q_mekf_sim(i, 2)*q_mekf_sim(i, 4));
    c8=2*(q_mekf_sim(i, 2)*q_mekf_sim(i, 3)-q_mekf_sim(i, 1)*q_mekf_sim(i, 4));
    c9=-q_mekf_sim(i, 1)^2-q_mekf_sim(i, 2)^2+q_mekf_sim(i, 3)^2+q_mekf_sim(i, 4)^2;
    c=[c1 c2 c3;c4 c5 c6;c7 c8 c9];
    
    end
    % kalman
    b1_q_sim = a*(r1_hat(i, :)');
    b1_q_mekf = c*(r1_hat(i, :)');
    b2_q_sim = a*(r2(i, :)');
    b2_q_mekf = c*(r2(i, :)');
    
    y(:, i) = [b1_hat(i, :)'; b2(i, :)'];
    h(:, i) = [b1_q_mekf; b2_q_mekf];
    y_h(:, i) = y(:, i)-h(:, i);
        
    y_h_errors(:, i) = [sum(sqrt((y(1:3, i)-h(1:3, i)).^2)); sum(sqrt((y(4:6, i)-h(4:6, i)).^2))];
    
    % mag error calculation
    for m=1
    
        % sensor error
        mag_3_error_sensor(:, i) = (b1_hat(i, :)'-b1(i, :)');
        mag_error_body_sensor(i) = sum(sqrt((b1(i, :)-b1_hat(i, :)).^2));
        %mag_error_body_conversion(i) = sum(sqrt((b1_q_sim'-(b1(i, :))).^2));
        
        % model error
        mag_error_eci_model(i) =  sum(sqrt((r1(i, :)-(r1_hat(i, :)))).^2);
        mag_3_error_model(:, i) = (r1(i, :)-(r1_hat(i, :)));
    end
    
    % sun error calculation
    for s=1
        % sensor error
        sun_3_error_sensor(:, i) = (b2_hat(i, :)'-b2(i, :)');
        sun_error_body_sensor(i) = sum(sqrt((b2(i, :)-b2_hat(i, :)).^2));
        sun_error_q_rotation_vs_model(i) = sum(sqrt((b2_q_mekf-b2(i, :)').^2));
                
    end
    

end


%% Plot mag

plot((5:secs-4), mag_error_body_sensor(5:secs-4));
title('mag body sensor error');
% figure()
% plot((5:secs-4), mag_error_body_conversion(5:secs-4));
% title('mag body conversion error');

figure();
for k=1:3
   plot((5:secs-4), mag_3_error_sensor(k, (5:secs-4))); 
   hold on
end
title('mag 3 body sensor error');


figure()
plot(5:secs-4, mag_error_eci_model(5:secs-4));
title('mag eci model error');

figure();
for k=1:3
   plot(5:secs-4, mag_3_error_model(k, 5:secs-4)); 
   hold on
end
title('mag 3 eci model error');

%% Plot sun

plot((5:secs-4), sun_error_body_sensor(5:secs-4));
title('sun body sensor error');

figure();
for k=1:3
   plot((5:secs-4), sun_3_error_sensor(k, (5:secs-4))); 
   hold on
end
title('sun 3 sun sensor error');

figure();
plot((5:secs-4), sun_error_q_rotation_vs_model(5:secs-4));
title('sun body q rot vs model');

for k=1:3
    figure(k*10);
   plot((5:secs-4), b2((5:secs-4), k)); 
   hold on
end
for k=1:3
    figure(k*10);
   plot((5:secs-4), b2_hat((5:secs-4), k)); 
   hold on
end
legend('s1', 's2', 's3', 's1_hat', 's2_hat', 's3_hat');
title('sun hat vs real');



%% Plot kalman

figure();
for k=1:3
   plot((5:secs-4), y_h(k, (5:secs-4))); 
   hold on
end
title('y-h mag');

figure();
for k=4:6
   plot((5:secs-4), y_h(k, (5:secs-4))); 
   hold on
end
title('y-h sun');


figure();
for k=1:2
   plot((5:secs-4), y_h_errors(k, (5:secs-4))); 
   hold on
end
title('y-h erros');
% 
% figure();
% for k=1:4
%    plot(1:secs, q(:, k) ); 
%    hold on
% end
% title('mag 3 body hat vs not hat');


%%
figure();
for k=1:3
   plot((5:secs-4),ADCSModelComplete.wSat.signals.values((5:secs-4), k))
   hold on
end
title('wsat');

%% 

q_real = ADCSModelComplete.q.signals.values;
q_Mekf = ADCSModelComplete.q_mekf.signals.values;
%q_Mekf = ADCSModelComplete.q_discrete.signals.values;

versor = [1, 1, 1]'/norm([1, 1, 1]);

dif_quats = zeros(size(q_real,1), 4);
abs_dif_quats = zeros(size(q_real,1), 1);
q1_vec = zeros(size(q_real,1), 3);
q2_vec = zeros(size(q_real,1), 3);
dot_vecs = zeros(size(q_real,1), 3);
dot_vecs_manual = zeros(size(q_real,1), 3);
dif_vecs = zeros(size(q_real,1), 3);
abs_dif_vecs = zeros(size(q_real,1), 1);

for f=1:size(q_real,1)
    dif_quats(f, :) = q_real(f, :) - q_Mekf(f, :);
    abs_dif_quats(f) = sum(sqrt((q_real(f, :)-q_Mekf(f, :)).^2));
    q1_vec(f, :) = quat2rotm(q_real(f, :))*versor;
    q2_vec(f, :) = quat2rotm(q_Mekf(f, :))*versor;
    dot_vecs(f, :) = dot(q1_vec(f, :), q2_vec(f, :));
    dot_vecs_manual(f, :) = q1_vec(f, :)*(q2_vec(f, :)');
    dif_vecs(f, :) = q1_vec(f, :)-q2_vec(f, :);
    abs_dif_vecs(f) = sum(sqrt((q1_vec(f, :)-q2_vec(f, :)).^2));
end

% figure();
% plot(ADCSModelComplete.q.time(plot_time), dot_vecs(plot_time));
% title('dot product with func');

% figure();
% plot(ADCSModelComplete.q.time(plot_time), dot_vecs_manual(plot_time));
% title('dot product manual');

% figure()
% plot(ADCSModelComplete.q.time(plot_time), q1_vec(plot_time, :));
% title('q1 vec over time');
% figure()
% plot(ADCSModelComplete.q.time(plot_time), q2_vec(plot_time, :));
% title('q2 vec over time');
% figure()

% plot(ADCSModelComplete.q.time(plot_time), dif_vecs(plot_time, :));
% title('dif in vecs');
% figure()
% plot(ADCSModelComplete.q.time(plot_time), abs_dif_vecs(plot_time));
% title('abs dif in vecs');
figure()
plot(ADCSModelComplete.q.time(plot_time), dif_quats(plot_time, :));
title('dif in quats');
% figure()
% plot(ADCSModelComplete.q.time(plot_time), abs_dif_quats(plot_time));
% title('abs dif in quats');


%%
figure()
plot(ADCSModelComplete.q.time(plot_time),q_real(plot_time, :));
figure()
plot(ADCSModelComplete.q.time(plot_time),q_Mekf(plot_time, :));
title('q mekf');
