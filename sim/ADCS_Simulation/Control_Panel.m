p(1) = "des";
p(2) = "hat";
p(3) = "delta";
p(4) ="inputTorque";
p(5) ="";
p(6) ="";
p(7) ="";
p(8) ="";
p(9) ="";
cd # 
plots = 4;

%% 
if plots == 4
    figure;
    for i=1:4
    prev =subplot(2,2,i);
    pos1 = get(prev, 'Position') % gives the position of current sub-plot
    new_pos1 = pos1 +[0 0 0 0.05]
    set(prev, 'Position',new_pos1 ) % set new position of current sub - plot
    plot(ADCSModelComplete.(p(i)));
    title(p(i));
    end
end

%% 
if plots == 9
    figure;
    for i=1:9
    subplot(3,3,i);
    plot(ADCSModelComplete.(p(i)));
    title(p(i));
    end
end
%%

function varargout = tightSubplot(m,n,i)
    [x,y] = ind2sub([m,n],i);
    if nargout > 0
        varargout{1} = axes('units','normalized','position',[(x-1)/m,(y-1)/n,1/m,1/n]);
    else
        axes('units','normalized','position',[(x-1)/m,(y-1)/n,1/m,1/n]);
    end
end