%% Manage Paths
path_strs = importdata('Other Data/Paths.csv');
for i=1:length(path_strs)
    oldpath = path;
    if exist(strcat(path_strs{i}(1:end-1)), 'dir')
        path(oldpath,strcat(path_strs{i}));
    end
end
clear oldpath path_strs


load('Orbit Determination/GMAT_Validation/PositionValidationGMAT.mat');

%% Get Setup Simulation Duration
i=1;
while Simulation.simDuration > Simulation.standardSimDurations(i)
    i=i+1;
end
Simulation.setupSimDuration = Simulation.standardSimDurations(i);
clear i

%% Enables
Simulation.enables.aeroTorque         = 1;
Simulation.enables.resMagDipTorque    = 1;
Simulation.enables.gravGradTorque     = 1;
Simulation.enables.photodiodes        = 1;
Simulation.enables.albedo             = 1;
Simulation.enables.magnetometer       = 1;
Simulation.enables.gyroscope          = 1;
Simulation.enables.star_tracker       = 1;
Simulation.enables.attitudeEstimator  = 1;
Simulation.enables.positionEstimation = 0;
Simulation.enables.magFieldEstimation = 1;

Simulation = ProcessEnables(Simulation);

%% C Code testing
Simulation.runCCode.ssProcessing       = 0;
Simulation.runCCode.pointingControl    = 0;
Simulation.runCCode.detumblingControl  = 0;
Simulation.runCCode.magFieldEstimation = 0;
Simulation.runCCode.positionEstimation = 0;
Simulation.runCCode.attitudeEstimation = 0;

Simulation.preCompute.positionEstimation = 0; % 1 or 0
Simulation.preCompute.magFieldEstimation = 0; % 1 or 0
%% Setup Simulation Details
% General Setup
Mission.startDate = [2021,12,1,12,0,0];
Mission.startJulianDate = juliandate(Mission.startDate);

Orbit = [];
Orbit = define_DCM_eci_ecef_transformations(Mission.startDate, Orbit);

% Orbital Elements SSO orbit 2021 -  (these are currently not configurable)
Orbit.elem.a = 6871; % Semi-major axis (km)
Orbit.elem.ecc = 0.0; % Eccentricity [-]
Orbit.elem.inc = 97.86; % Inclination [�]
Orbit.elem.Omega = 22.5; % RAAN [�] unknown value
Orbit.elem.w = 0; % Argument at Perigee [�] unknown value
Orbit.elem.M = 0; % Mean anomaly [�] unknown value

% Satellite Properties
Sat = [];
Sat = get_satellite_initial_pos_and_vel(Sat);
MagneticFieldCoeffs = load('MagneticFieldModelCoeffs.mat');
GravityFieldCoeffs = load('CoeffsGravitySphericalHarmonics');

% Data Saving Sample Times
DataSaveSampleTimes = define_sample_times_for_data_save(setupTarget,'Other Data/DataSaveSampleTimes.csv');

%% Support Models

% Calculate Ground station Positin
if setupTarget.Beacon
    Beacon.GSposLLA = [38, 150, 159];
    Beacon.maxDisLOS = 2290000; % http://www.spaceacademy.net.au/spacelink/spcomcalc.htm  - horizon distance @ 400 km
    Beacon.cameraFOV = 3.5;
    Beacon.scalingFactorCoordenates = 139800; % norm of most far out vector within FOV and max dist
    Beacon.freq = 20;
    Beacon.conversionStepSize = 0.5;
    GroundStationECI = zeros(ceil(Simulation.simDuration/Beacon.conversionStepSize), 4);
    stDt = datetime(Mission.startDate);
    secs = 0;
    for i=1:size(GroundStationECI, 1)
        stDt = stDt + seconds(Beacon.conversionStepSize);
        GroundStationECI(i, 1:3) = lla2eci(Beacon.GSposLLA,[stDt.Year stDt.Month stDt.Day stDt.Hour stDt.Minute stDt.Second]);
        secs = secs + Beacon.conversionStepSize;
        GroundStationECI(i, 4) = secs;
    end
    Beacon.GroundStationECI = GroundStationECI;
    clear secs
    cd 'Simulation Data Repository'
    save('Beacon.mat', 'Beacon')
    cd ..
else
    load('Simulation Data Repository/Beacon.mat');
end

% Setup + Run: Moon and Sun Position Integrator
if setupTarget.MoonSun
    %% Setup MoonSun
    MoonSun.ephemerisUpdatePeriod = 1000;
    MoonSun.stepSize              = 0.5;
    %% Run MoonSun
    tic
    filename = strcat('MoonSun_',simTags.MoonSun,'_',num2str(Simulation.setupSimDuration),'.mat');
    cd ('Simulation Data Repository')
    if exist(filename,'file')
        load(filename);
        cd ..
    else
        % Run Function
        cd ('../Orbit Determination')
        [MoonSun_out] = Moon_Sun_Position_Model_of_Reality(MoonSun,Simulation.setupSimDuration,Mission.startJulianDate,testMode.MoonSun);
        MoonSun_out.execTime = toc;
        cd ..
        % Save variables
        cd ('Simulation Data Repository')
        save(filename,'MoonSun_out');
        cd ..
        % Display Information
        disp(strcat('MoonSun time: ',32,num2str(MoonSun_out.execTime)));
        if testMode.MoonSun
            if MoonSun_out.avrgDrift.earthPosition(1) ~=0
                fprintf('\t Average Drift Earth Position: %.1e %%\n',MoonSun_out.avrgDrift.earthPosition*100);
                fprintf('\t Average Drift Moon Position: %.1e %%\n',MoonSun_out.avrgDrift.moonPosition*100);
                fprintf('\t Ephemeris Update Period: %d seconds\n',MoonSun.ephemerisUpdatePeriod);
            end
        end
        
    end
    % Final Housekeeping
    Orbit.MoonPosECI = MoonSun_out.MoonPosECI;
    Orbit.SunPosECI = MoonSun_out.SunPosECI;
    Orbit.MoonSun.avrgDrift = MoonSun_out.avrgDrift;
    Orbit.MoonSun.execTime = MoonSun_out.execTime;
    clear MoonSun_out MoonSun filename 
end
% Setup + Run: Satellite Position Integrator
if setupTarget.SatPosition
    %% Setup Position Propagator
    SatPosition.samplingPeriod    = 0.5;
    SatPosition.stepSizeRK8       = 10;
    %% Run Position Propagator
    tic
    filename = strcat('SatPosition_',simTags.SatPosition,'_',num2str(Simulation.setupSimDuration),'.mat');
    cd ('Simulation Data Repository')
    if exist(filename,'file')
        load(filename);
        cd ..
    else
        % Run Function
        cd ('../Orbit Determination')
        [SatPosition_out] = Sat_Position_Model_of_Reality(SatPosition,Sat,Orbit,Simulation.setupSimDuration,testMode.SatPosition);
        SatPosition_out.execTime = toc;
        cd ..
        % Save variables
        cd ('Simulation Data Repository')
        save(filename,'SatPosition_out');
        cd ..
        % Display Information
        disp(strcat('SatPosition time: ',32,num2str(SatPosition_out.execTime)));
        if testMode.SatPosition 
            fprintf('\t Maximum Position Drift: %.3e meters\n',SatPosition_out.maximumDrift);
        end
    end
    % Final Housekeeping
    Orbit.SatPosECI = SatPosition_out.SatPosECI;
    Orbit.SatVelECI = SatPosition_out.SatVelECI;    
    Orbit.SatPosition.maximumDrift = SatPosition_out.maximumDrift;
    Orbit.SatPosition.execTime = SatPosition_out.execTime;
    clear SatPosition_out SatPosition filename
end
% Setup + Run: Earth_Albedo_Model
if setupTarget.Earth_Albedo_Model
    %% Setup Albedo Model
    Albedo.samplingPeriod           = 10;
    Albedo.validationSamplingPeriod = 1000;
    %% Run Albedo Model
    tic
    filename = strcat('Albedo_',simTags.Albedo,'_',num2str(Simulation.setupSimDuration),'.mat');
    cd ('Simulation Data Repository')
    if exist(filename,'file')
        load(filename);
        cd ..
    else
        % Run Function
        cd ('../Earth Albedo')
        load('AlbedoModelInputData.mat');
        load('AlbedoModelInputData_Original.mat');
        AlbedoModel = sim('Earth_Albedo_Model',Simulation.setupSimDuration);
        AlbedoModel.execTime = toc;
        cd ..
        % Save variables
        cd ('Simulation Data Repository')
        save(filename,'AlbedoModel');
        cd ..
        % Display Infomation
        disp(strcat('Albedo time: ',32,num2str(AlbedoModel.execTime)));
        if testMode.Earth_Albedo_Model
            fprintf('\t Average Reflectivity Error: %.3f %%\n',mean(AlbedoModel.avrgErrorAlbedo.signals.values)*100);
        end
    end
    % Final Housekeeping
    clear Albedo reflData cellArea filename reflData_struct cellArea_original reflData_original
end
% Setup + Run: Earth Magnetic Field Model
if setupTarget.Earth_Magnetic_Field_Model
    %% Setup Magnetic Model
    MagneticField.samplingPeriod  = 1/10;
    %% Run Magnetic Model
    tic
    filename = strcat('MagneticField_',simTags.MagneticField,'_',num2str(Simulation.setupSimDuration),'.mat');
    cd ('Simulation Data Repository')
    if exist(filename,'file')
        load(filename);
        cd ..
    else
        % Run Function
        cd ('../Earth Magnetic Field')
        MagneticFieldModel = sim('Earth_Magnetic_Field_Model',Simulation.setupSimDuration);
        MagneticFieldModel.execTime = toc;
        cd ..
        % Save variables
        cd ('Simulation Data Repository')
        save(filename,'MagneticFieldModel');
        cd ..
        % Display Information
        disp(strcat('MagneticField time: ',32,num2str(MagneticFieldModel.execTime)));
    end
    % Final Housekeeping
    clear MagneticField filename
end
% Setup + Run: Estimators Model (position and magnetic field estimators)
if setupTarget.Estimators_Model
    %% Setup Estimators Model
    % Satellite Position Estimation 
    Estimation.position.samplingTimeRK4 = 10;
    Estimation.position.updateTimeRV = 5739; % Time in seconds between updates of keplerian elements to satellite
    Estimation.position.timeFirstUpdate = 793;
    Estimation.position.samplingPeriod = 0.5;
    Estimation.sunPos.samplingPeriod = 0.5;
    % Magnetic Field Estimation
    Estimation.magField.n_max = 5;
    Estimation.magField.samplingPeriod=2;
    Estimation.MagneticFieldCoeffs = MagneticFieldCoeffs;
    %% Run Estimators Model
    tic
    filename = strcat('Estimators_',simTags.Estimators,'_',num2str(Simulation.setupSimDuration),'.mat');
    cd ('Simulation Data Repository')
    if exist(filename,'file')
        load(filename);
        cd ..
    else
        % Run Function
        cd ..
        EstimatorsModel = sim('Estimators_Model',Simulation.setupSimDuration);
        EstimatorsModel.execTime = toc;
        % Save variables
        cd ('Simulation Data Repository')
        save(filename,'EstimatorsModel');
        cd ..
        % Display Information
        disp(strcat('Estimators time: ',32,num2str(EstimatorsModel.execTime)));
    end
    % Final Housekeeping
    clear Estimation filename
end

%% Different ADCS Operation Models 
% Setup variables
if setupTarget.ADCS_Complete_Model
    %% OPERATIONAL VARIABLES
    Mission.orbitTimeDelay = 0; % to change the initial point on the orbit
    wSat_magn = 0.5;                  % Initial angular rate of 5 for pointing, 30 for detumbling.
    %wSat_magn = 30;
    wSat_magn_parallel_magField = 0.3; % Initial angular rate around the vector of the magnetic field
    
    %% ENVIRONMENT
    %% Orbit
    Orbit.posUpdatePeriod = 0.5; % Sampling Period of Position related variables
    Orbit.eclipseAperture = 68;  % Aperture of the eclipse 
    %% Rotation Dynamics
    % Inertia Tensor
    Sat.I = diag([6.65, 33.25, 33.25])*10^-3; %kg*m^2     
    % Attitude and Angular Velocity
    q = compact(randrot); % Random selection from Uniform distribution
    Sat.q = [-0.547212863050710,0.325040552001621,0.738795346989299,0.221558473846721];   %Initial attitude.
    Sat = get_satellite_initial_angular_velocity(Sat,wSat_magn,wSat_magn_parallel_magField,q,MagneticFieldModel.magFieldECI.signals.values(1,:));  
    clear q wSat_magn wSat_magn_parallel_magField
    %% Torque Disturbances
    % Satellite Aerodynamic Caracteristics
    Orbit.drag.cd = 1.05; %Drag coeficient
    Orbit.drag.refArea = 0.1*0.1; %Sattelite Drag Normal Area m2
    Orbit.drag.torque = 2.6587e-08/sqrt(2); %Maximum Drag Torque (2.6587e-08 at 400km, 1.9144e-07 at 300km and 1.5798e-06 at 200km)
    Orbit.drag.phase(1) = 0; %Axis X torque sinusoid phase
    Orbit.drag.phase(2) = 2*pi/3; %Axis Y torque sinusoid phase
    Orbit.drag.phase(3) = 4*pi/3; %Axis Z torque sinusoid phase   
    % ResidualMagneticDipole
    Orbit.resMagDip = 0.01;   % A/m^2
    %% Pointing Direction
    Mission.pointingDirECI    = [0; 0; 0]; % Nothing for now  -  state 1
    Mission.pointingTargetECI = [0; 0; 0]; % Nothing for now  -  state 2
    Mission.pointingDirState = 2;
    Mission.pointingVecBody = [0;0;-1];
    
    
    %% SENSORS
    %% Magnetometer
    % HMC5983
    Sensors.mag.sat = 100000;          % Magnetometer range (nT)
    Sensors.mag.bits = 12;             % Magnetometer ADC bit size
    Sensors.mag.freq = 10;             % Magnetometer frequency sample rate (Hz)
    Sensors.mag.noiseVar = 200^2;      % Magnetometer noise power spectrum density (nT^2)
    Sensors.mag.bias = [100,-100,100]; % Magnetometer readings bias
    Sensors.mag.linError = 0.999;      % Linearization error gain
    %% Gyroscopes
    % MPU9250 
    Sensors.gyro1.sat      = 250;     % Gyroscope max scope (deg/s)
    Sensors.gyro1.bits     = 16;      % Gyroscope ADC bit size
    % Sensors.gyro1.freq     = 10;      % Gyroscope frequency sample rate (Hz)
    Sensors.gyro1.freq     = 20;      % Gyroscope frequency sample rate (Hz)
    Sensors.gyro1.AR_PSD   = 1e-4;    % Angle random noise power spectrum density 1e-4 (deg/s)^2/Hz (3.0462e-08 (rad/s)^2/Hz )
    Sensors.gyro1.RRW_bias = 9E-5;    % Drift noise bias (not used) ~0.005�/s
    Sensors.gyro1.RRW_PSD  = 1.96e-08;  % Rate Random Walk noise power spectrum density 1.96e-08 (deg/s^2)^2/Hz (5.9705e-12 (rad/s^2)^2/Hz )
    Sensors.gyro1.LinError = 0.999;   % Linearization error gain
    %% Photodiodes
    Sensors.photodiode.curr = 4e-8; % Typical photodiode current in amperes
    Sensors.photodiode.freq = 10; % Sun sensor signal sample frequency in Hertz (Hz)
    Sensors.photodiode.intensityMagnitudeThreshold = 0.9; % If the total intensity magnitude (normalized) is below 0.9,
                                                          % we assume the sun is iluminating the -Z face
    Sensors.photodiode.albedoSampPeriod = 1;

    %% Star tracker;
    Sensors.star_sensor.freq = 1;                       % Star tracker sample rate (hz)
    Sensors.star_sensor.rotation_to_body = [0, 0, 0];  % Rotation from sensor to body axis  
    
    %% GPS
    Sensors.GPS.freq = 1;
    Sensors.GPS.acc = 10;
    
    %% Coarse Sun sensor
    Sensors.coarseSunSensor.fov = 50;
    Sensors.coarseSunSensor.freq = 40;
    Simulation.enables.coarseSunSensor = 1;
    Sensors.coarseSunSensor.xgain = 0.5;
    Sensors.coarseSunSensor.ygain = 0.5;
    
    %% ESTIMATION    
    %% Complementary Filter  
    Estimation.attitude.magGain  = 1;
    Estimation.attitude.sunGain  = 0.1;
    Estimation.attitude.totGain  = 0.7;
    Estimation.attitude.biasGain = 0.1;
    
    Estimation.attitude.SSValidityThreshold = 0; % Minimum intensity that is considered. If the FOV has to be limited, this value should be cos(FOV)

    %% MEKF
    %q_init = Sat.q;
    %q_init = [-0.4104 0.3900 0.7697 0.2951]; % 9.8 deg init error
    q_init = [-0.4776 0.3602 0.7577 0.2608]; % 5.3 deg init error
    b_init = [0,0,0];
    c = 0.000001;
    P_init = diag([c, c, c, c, c, c]);
    mekf.enable = 1;

    mekf.propagation_delta = 0.025;
    mekf.update_delta = 0.1;
    % Noise config 0 no noise, 1 noise
    mekf.noise.w = 1; 
    mekf.noise.magBody = 1;
    mekf.noise.sunBody = 1;
    mekf.noise.magECI = 1;

    mekf.update_enable = 1; % 0 or 1
    mekf.always_update = 1; % 0 or 1
    mekf.control = -1;
    
    mekf.R_mag = eye(3)*(0.0055);
    mekf.R_sun = 0.01*diag([0.16, 0.16, 0.45]);
    mekf.R_beacon =  0.001*diag([0.0001, 0.0001, 0.0003]);
    
    %% Magnetic Field Estimation
    Estimation.magField.n_max = 5;
    Estimation.MagneticFieldCoeffs = MagneticFieldCoeffs;
    
    
    %% CNTRL
    %% Controllers
    Control.toPointingThreshold = 1; % Angular rate threshold for the transition to pointing state, either from the detumbling state or from the initial state
    Control.toDetumblingThreshold = 5; % Angular rate threshold for the transition to detumbling state from the pointing state
    Control.pwmMax = 1000; % Magnetorquers maximum duty-cycle for 10us pulse (+/- 10kHz)
    Control.detumblingK = 0.00003; % Gain used for detumbling
    Control.pointingD = 1e-6; % Gain used for detumbling inside the pointing
    Control.pointingRotation = 1e-6; % Gain used for detumbling inside the pointing
    Control.pointingP = 1e-6;
    Control.pointingAccuracyTarget = 20; % Used for analysis and optimization
    
    Control.period = 0.5;
    
    Control.kp = 8.4000e-05;
    Control.kd = 2.3000e-05;
    
    Control.kp3 = 8.4000e-05;
    Control.kd3 = 2.3000e-05;
    
    Control.k1 = 8.5e-04;
    Control.k2 = 0.0005;
    Control.alpha1 = 0.6;
    Control.alpha2 = 2*Control.alpha1/(Control.alpha1+1);
    Control.sigma = 0.3;
    
    Control.sldK = 0.026; %0.015
    Control.sldEpsilon = 0.008; %0.01
    Control.sldG = 0.0005*eye(3); %0.00015
    Control.sldJHat = Sat.I;
  
    %% ACTUATORS
    %% Magnetorquers
    Actuators.magn.dipole = 0.131; % Magnetic dipole
    Actuators.magn.dipole2curr = 0.078/Actuators.magn.dipole; % Convertion from magnetic dipole to current
    Actuators.magn.Volt = 3.3; % Working voltage
    Actuators.magn.Pos{1} = [1 0 0] ; %Magnetorquer 1 normal orientation in the body frame
    Actuators.magn.Pos{2} = [0 1 0] ; %Magnetorquer 2 normal orientation in the body frame
    Actuators.magn.Pos{3} = [0 0 1] ; %Magnetorquer 3 normal orientation in the body frame
    
    %% Reaction Wheel
    % Nano Avionics RW01
    % Sensors.rw.freq = 40;
    % Sensors.rw.mass = 0.137;
    % Sensors.rw.RPM = 6400;
    % Sensors.rw.r = 0.0234;
    % Sensors.rw.k = 0.403;
    % Sensors.rw.momentumMax = 20*10^-3;
    % Sensors.rw.torqueMax = 3.2*10^-3;

    % Cubespace Cubewheel Small
    Sensors.rw.freq = 40; % to do, but approximated
    Sensors.rw.mass = 0.05; % estimated based on total weight
    Sensors.rw.RPM = 7990;
    Sensors.rw.r = 0.0230; 
    Sensors.rw.k = 0.1178; % to do
    Sensors.rw.momentumMax = 1.77*10^-3;
    Sensors.rw.torqueMax = 0.23*10^-3;
    

    

    % Create Estimation Data Bus for Simulink Model
    rw_bus_info = Simulink.Bus.createObject(Sensors.rw);
    rw_bus = evalin('base', rw_bus_info.busName);
    
    % Inertia Tensor
    Sat.I = Sat.I + eye(3)*Sensors.rw.k*Sensors.rw.mass*Sensors.rw.r*Sensors.rw.r;
    %% TIMINGS - unsued
    Sensors.detumbling_nMeas = 1; % Each set of measurements has 100 ms
    Sensors.nMeas = 3;   % Each set of measurements has 100 ms
    Estimation.attitude.samplingPeriod = 1;
    Estimation.magField.samplingPeriod = 1;
    Estimation.position.samplingPeriod = 1;
    Estimation.instant_ms = 300;
    Control.period = 1;
    Control.estimationInst  = 0; 
    Control.controlInst     = 0;
    Control.maxDutyCycle    = 0.6; % Limit of the Duty Cycle of the actuation
    
end
if setupTarget.ACS_Model
    %% OPERATIONAL VARIABLES
    Mission.orbitTimeDelay = 0; % to change the initial point on the orbit
    wSat_magn = 10;                  % Initial angular rate of 5 for pointing, 30 for detumbling.
    wSat_magn_parallel_magField = 5; % Initial angular rate around the vector of the magnetic field
    
    %% ENVIRONMENT
    %% Orbit
    Orbit.posUpdatePeriod = 0.5; % Sampling Period of Position related variables
    Orbit.eclipseAperture = 68;  % Aperture of the eclipse 
    %% Rotation Dynamics
    % Inertia Tensor
    Sat.I = diag([6.65, 33.25, 33.25])*10^-3; %kg*m^2     
    % Attitude and Angular Velocity
    q = compact(randrot); % Random selection from Uniform distribution
    Sat.q = [-0.547212863050710,0.325040552001621,0.738795346989299,0.221558473846721];   %Initial attitude.
    Sat = get_satellite_initial_angular_velocity(Sat,wSat_magn,wSat_magn_parallel_magField,q,MagneticFieldModel.magFieldECI.signals.values(1,:));  
    clear q wSat_magn wSat_magn_parallel_magField
    %% Torque Disturbances
    % Satellite Aerodynamic Caracteristics
    Orbit.drag.cd = 1.05; %Drag coeficient
    Orbit.drag.refArea = 0.1*0.1; %Sattelite Drag Normal Area m2
    Orbit.drag.torque = 2.6587e-08/sqrt(2); %Maximum Drag Torque (2.6587e-08 at 400km, 1.9144e-07 at 300km and 1.5798e-06 at 200km)
    Orbit.drag.phase(1) = 0; %Axis X torque sinusoid phase
    Orbit.drag.phase(2) = 2*pi/3; %Axis Y torque sinusoid phase
    Orbit.drag.phase(3) = 4*pi/3; %Axis Z torque sinusoid phase   
    % ResidualMagneticDipole
    Orbit.resMagDip = 0.01;   
    %% Pointing Direction
    Mission.pointingDirECI    = [0; 0; 0]; % Nothing for now  -  state 1
    Mission.pointingTargetECI = [0; 0; 0]; % Nothing for now  -  state 2
    Mission.pointingDirState = 2;
    Mission.pointingVecBody = [0;0;-1];
    
    
    %% SENSORS
    %% Magnetometer
    % HMC5983
    Sensors.mag.sat = 100000;          % Magnetometer range (nT)
    Sensors.mag.bits = 12;             % Magnetometer ADC bit size
    Sensors.mag.freq = 10;             % Magnetometer frequency sample rate (Hz)
    Sensors.mag.noiseVar = 200^2;      % Magnetometer noise power spectrum density (nT^2)
    Sensors.mag.bias = [100,-100,100]; % Magnetometer readings bias
    Sensors.mag.linError = 0.999;      % Linearization error gain
    %% Gyroscopes
    % MPU9250 
    Sensors.gyro1.sat      = 250;     % Gyroscope max scope (deg/s)
    Sensors.gyro1.bits     = 16;      % Gyroscope ADC bit size
    Sensors.gyro1.freq     = 40;      % Gyroscope frequency sample rate (Hz)
    Sensors.gyro1.AR_PSD   = 1e-4;    % Angle random noise power spectrum density 1e-4 (deg/s)^2/Hz (3.0462e-08 (rad/s)^2/Hz )
    Sensors.gyro1.RRW_bias = 9E-5;    % Drift noise bias (not used) ~0.005�/s
    Sensors.gyro1.RRW_PSD  = 1.96e-08;  % Rate Random Walk noise power spectrum density 1.96e-08 (deg/s^2)^2/Hz (5.9705e-12 (rad/s^2)^2/Hz )
    Sensors.gyro1.LinError = 0.999;   % Linearization error gain
    %% Photodiodes
    Sensors.photodiode.curr = 4e-8; % Typical photodiode current in amperes
    Sensors.photodiode.freq = 2; % Sun sensor signal sample frequency in Hertz (Hz)
    
    %% CONTROL
    %% Controllers
    Control.toPointingThreshold = 1; % Angular rate threshold for the transition to pointing state, either from the detumbling state or from the initial state
    Control.toDetumblingThreshold = 5; % Angular rate threshold for the transition to detumbling state from the pointing state
    Control.samplingPeriod = 0.5; % Controller n ones out there, actuator sampling time (For detumbling use 2s)
    Control.pwmMax = 1000*Control.samplingPeriod; % Magnetorquers maximum duty-cycle for 10us pulse (+/- 10kHz)
    Control.detumblingK = 0.00003; % Gain used for detumbling
    Control.pointingD = 1e-6; % Gain used for detumbling inside the pointing
    Control.pointingRotation = 1e-6; % Gain used for detumbling inside the pointing
    Control.pointingP = 1e-6;
    Control.dutyCycleLimit = 0.8; % Limit of the Duty Cycle of the actuation
    Control.pointingDutyCycleSat = 0.8;
    Control.pointingAccuracyTarget = 20; % Used for analysis and optimization
    % Needed for sensor data processing
    Estimation.attitude.samplingPeriod = Control.samplingPeriod;
    
    %% ACTUATORS
    %% Magnetorquers
    Actuators.magn.samplingPeriod = Control.samplingPeriod; % Sample frequency in Hz
    Actuators.magn.dipole = 0.131; % Magnetic dipole
    Actuators.magn.dipole2curr = 0.078/Actuators.magn.dipole; % Convertion from magnetic dipole to current
    Actuators.magn.Volt = 3.3; % Working voltage
    Actuators.magn.Pos{1} = [1 0 0] ; %Magnetorquer 1 normal orientation in the body frame
    Actuators.magn.Pos{2} = [0 1 0] ; %Magnetorquer 2 normal orientation in the body frame
    Actuators.magn.Pos{3} = [0 0 1] ; %Magnetorquer 3 normal orientation in the body frame
    
    
end
if setupTarget.ADS_Model
    %% OPERATIONAL VARIABLES
    Mission.orbitTimeDelay = 0; % to change the initial point on the orbit
    wSat_magn = 10;                  % Initial angular rate of 5 for pointing, 30 for detumbling.
    wSat_magn_parallel_magField = 5; % Initial angular rate around the vector of the magnetic field
    
    %% ENVIRONMENT
    %% Orbit
    Orbit.posUpdatePeriod = 0.5; % Sampling Period of Position related variables
    Orbit.eclipseAperture = 68;  % Aperture of the eclipse 
    %% Rotation Dynamics
    % Inertia Tensor
    Sat.I = diag([6.65, 33.25, 33.25])*10^-3; %kg*m^2     
    % Attitude and Angular Velocity
    q = compact(randrot); % Random selection from Uniform distribution
    Sat.q = [-0.547212863050710,0.325040552001621,0.738795346989299,0.221558473846721];   %Initial attitude.
    Sat = get_satellite_initial_angular_velocity(Sat,wSat_magn,wSat_magn_parallel_magField,q,MagneticFieldModel.magFieldECI.signals.values(1,:));  
    clear q wSat_magn wSat_magn_parallel_magField
    %% Torque Disturbances
    % Satellite Aerodynamic Caracteristics
    Orbit.drag.cd = 1.05; %Drag coeficient
    Orbit.drag.refArea = 0.1*0.1; %Sattelite Drag Normal Area m2
    Orbit.drag.torque = 2.6587e-08/sqrt(2); %Maximum Drag Torque (2.6587e-08 at 400km, 1.9144e-07 at 300km and 1.5798e-06 at 200km)
    Orbit.drag.phase(1) = 0; %Axis X torque sinusoid phase
    Orbit.drag.phase(2) = 2*pi/3; %Axis Y torque sinusoid phase
    Orbit.drag.phase(3) = 4*pi/3; %Axis Z torque sinusoid phase   
    % ResidualMagneticDipole
    Orbit.resMagDip = 0.01;   
    %% Pointing Direction
    Mission.pointingDirECI    = [0; 0; 0]; % Nothing for now  -  state 1
    Mission.pointingTargetECI = [0; 0; 0]; % Nothing for now  -  state 2
    Mission.pointingDirState = 2;
    Mission.pointingVecBody = [0;0;-1];
    
    
    %% SENSORS
    %% Magnetometer
    % HMC5983
    Sensors.mag.sat = 100000;          % Magnetometer range (nT)
    Sensors.mag.bits = 12;             % Magnetometer ADC bit size
    Sensors.mag.freq = 10;             % Magnetometer frequency sample rate (Hz)
    Sensors.mag.noiseVar = 200^2;      % Magnetometer noise power spectrum density (nT^2)
    Sensors.mag.bias = [100,-100,100]; % Magnetometer readings bias
    Sensors.mag.linError = 0.999;      % Linearization error gain
    %% Gyroscopes
    % MPU9250 
    Sensors.gyro1.sat      = 250;     % Gyroscope max scope (deg/s)
    Sensors.gyro1.bits     = 16;      % Gyroscope ADC bit size
    Sensors.gyro1.freq     = 40;      % Gyroscope frequency sample rate (Hz)
    Sensors.gyro1.AR_PSD   = 1e-4;    % Angle random noise power spectrum density 1e-4 (deg/s)^2/Hz (3.0462e-08 (rad/s)^2/Hz )
    Sensors.gyro1.RRW_bias = 9E-5;    % Drift noise bias (not used) ~0.005�/s
    Sensors.gyro1.RRW_PSD  = 1.96e-08;  % Rate Random Walk noise power spectrum density 1.96e-08 (deg/s^2)^2/Hz (5.9705e-12 (rad/s^2)^2/Hz )
    Sensors.gyro1.LinError = 0.999;   % Linearization error gain
    %% Photodiodes
    Sensors.photodiode.curr = 4e-8; % Typical photodiode current in amperes
    Sensors.photodiode.freq = 2; % Sun sensor signal sample frequency in Hertz (Hz)
    Sensors.photodiode.intensityMagnitudeThreshold = 0.9; % If the total intensity magnitude (normalized) is below 0.9,
                                                          % we assume the sun is iluminating the -Z face
    
    
    %% ESTIMATION    
    %% Attitude Estimation
    Estimation.attitude.magGain  = 1;
    Estimation.attitude.sunGain  = 0.1;
    Estimation.attitude.totGain  = 0.7;
    Estimation.attitude.biasGain = 0.1;
    Estimation.attitude.samplingPeriod = 0.5;
    Estimation.attitude.SSValidityThreshold = 0; % Minimum intensity that is considered. If the FOV has to be limited, this value should be cos(FOV)
    %% Satellite Position Estimation 
    Estimation.position.preCompute = 1; % 1 or 0
    Estimation.position.samplingTimeRK4 = 10;
    Estimation.position.updateTimeRV = 10000; % Time in seconds between updates of keplerian elements to satellite
    Estimation.position.samplingPeriod = 0.5;
    %% Magnetic Field Estimation
    Estimation.magField.preCompute = 1; % 1 or 0
    Estimation.magField.n_max = 5;
    Estimation.magField.samplingPeriod=2;
    Estimation.MagneticFieldCoeffs = MagneticFieldCoeffs;
    
    
    %% CONTROL
    %% Controllers
    Control.toPointingThreshold = 1; % Angular rate threshold for the transition to pointing state, either from the detumbling state or from the initial state
    Control.toDetumblingThreshold = 5; % Angular rate threshold for the transition to detumbling state from the pointing state
    Control.samplingPeriod = 0.5; % Controller n ones out there, actuator sampling time (For detumbling use 2s)
    Control.pwmMax = 1000*Control.samplingPeriod; % Magnetorquers maximum duty-cycle for 10us pulse (+/- 10kHz)
    Control.detumblingK = 0.00003; % Gain used for detumbling
    Control.pointingD = 1e-6; % Gain used for detumbling inside the pointing
    Control.pointingRotation = 1e-6; % Gain used for detumbling inside the pointing
    Control.pointingP = 1e-6;
    Control.pointingD_ref = 0; % deg/s
    Control.dutyCycleLimit = 0.8; % Limit of the Duty Cycle of the actuation
    Control.pointingDutyCycleSat = 0.8;
    Control.pointingAccuracyTarget = 20; % Used for analysis and optimization
    
    %% ACTUATORS
    %% Magnetorquers
    Actuators.magn.samplingPeriod = Control.samplingPeriod; % Sample frequency in Hz
    Actuators.magn.dipole = 0.131; % Magnetic dipole
    Actuators.magn.dipole2curr = 0.078/Actuators.magn.dipole; % Convertion from magnetic dipole to current
    Actuators.magn.Volt = 3.3; % Working voltage
    Actuators.magn.Pos{1} = [1 0 0] ; %Magnetorquer 1 normal orientation in the body frame
    Actuators.magn.Pos{2} = [0 1 0] ; %Magnetorquer 2 normal orientation in the body frame
    Actuators.magn.Pos{3} = [0 0 1] ; %Magnetorquer 3 normal orientation in the body frame
    
    
end % Not used
if setupTarget.ADCS_Detumbling_Model
    %% OPERATIONAL VARIABLES
    Mission.orbitTimeDelay = 0; % sec
    wSat_magn = 100;                  % Initial angular rate of 5 for pointing, 30 for detumbling.
    wSat_magn_parallel_magField = 50; % Initial angular rate around the vector of the magnetic field
    
    %% ENVIRONMENT
    %% Orbit
    Orbit.posUpdatePeriod = 0.5; % Sampling Period of Position related variables
    %% Rotation Dynamics
    % Inertia Tensor
    Sat.I = diag([6.65, 33.25, 33.25])*10^-3; %kg*m^2     
    % Attitude and Angular Velocity
    q = compact(randrot); % Random selection from Uniform distribution
    Sat.q = q;   %Initial attitude.
    Sat = get_satellite_initial_angular_velocity(Sat,wSat_magn,wSat_magn_parallel_magField,q,MagneticFieldModel.magFieldECI.signals.values(1,:));   
    clear q wSat_magn wSat_magn_parallel_magField
    %% Torque Disturbances
    % Satellite Aerodynamic Caracteristics
    Orbit.drag.cd = 1.05; %Drag coeficient
    Orbit.drag.refArea = 0.1*0.1; %Sattelite Drag Normal Area m2
    Orbit.drag.torque = 2.6587e-08/sqrt(2); %Maximum Drag Torque (2.6587e-08 at 400km, 1.9144e-07 at 300km and 1.5798e-06 at 200km)
    Orbit.drag.phase(1) = 0; %Axis X torque sinusoid phase
    Orbit.drag.phase(2) = 2*pi/3; %Axis Y torque sinusoid phase
    Orbit.drag.phase(3) = 4*pi/3; %Axis Z torque sinusoid phase   
    % ResidualMagneticDipole
    Orbit.resMagDip = 0.01;   

    
    
    %% SENSORS
    %% Magnetometer
    % HMC5983
    Sensors.mag.sat = 100000;          % Magnetometer range (nT)
    Sensors.mag.bits = 12;             % Magnetometer ADC bit size
    Sensors.mag.freq = 10;             % Magnetometer frequency sample rate (Hz)
    Sensors.mag.noiseVar = 200^2;      % Magnetometer noise power spectrum density (nT^2)
    Sensors.mag.bias = [100,-100,100]; % Magnetometer readings bias
    Sensors.mag.linError = 0.999;      % Linearization error gain
    
    %% CONTROL
    %% Controllers
    Control.samplingPeriod = 0.5; % Controller n ones out there, actuator sampling time (For detumbling use 2s)
    Control.pwmMax = 1000*Control.samplingPeriod; % Magnetorquers maximum duty-cycle for 10us pulse (+/- 10kHz)
    Control.detumblingK = 0.00003; % Gain used for detumbling
    Control.dutyCycleLimit = 0.8; % Limit of the Duty Cycle of the actuation
    Control.toPointingThreshold = 1; % Angular rate threshold for the transition to pointing state, either from the detumbling state or from the initial state
    
    %% ACTUATORS
    %% Magnetorquers
    Actuators.magn.samplingPeriod = Control.samplingPeriod; % Sample frequency in Hz
    Actuators.magn.dipole = 0.131; % Magnetic dipole
    Actuators.magn.dipole2curr = 0.078/Actuators.magn.dipole; % Convertion from magnetic dipole to current
    Actuators.magn.Volt = 3.3; % Working voltage
    Actuators.magn.Pos{1} = [1 0 0] ; %Magnetorquer 1 normal orientation in the body frame
    Actuators.magn.Pos{2} = [0 1 0] ; %Magnetorquer 2 normal orientation in the body frame
    Actuators.magn.Pos{3} = [0 0 1] ; %Magnetorquer 3 normal orientation in the body frame
    
end




%% Other Functions

% The Inital satellite position that corresponds to the pre-defined orbital
% elements is currently not calculated here
% As a matter of fact, a more accurate estimate was determined using GMAT
function Sat = get_satellite_initial_pos_and_vel(Sat)
load('Orbit Determination/GMAT_Validation/PositionValidationGMAT')
Sat.posInit = PositionValidation(1,1:3)';
Sat.velInit = VelocityValidation(1,1:3)';
end

% Direction Cosine Matrices to transform ECI <-> ECEF
function Orbit = define_DCM_eci_ecef_transformations(startDate,Orbit)
% Update Direct Cosine Matrix for ecef<->eci transformation
Orbit.DCM.eci2ecef = dcmeci2ecef('IAU-2000/2006',startDate);
Orbit.DCM.ecef2eci = inv(Orbit.DCM.eci2ecef);
Orbit.DCM_for_C.eci2ecef = reshape(round(Orbit.DCM.eci2ecef*10^9)',[1,9]);
Orbit.DCM_for_C.ecef2eci = reshape(round(Orbit.DCM.ecef2eci*10^9)',[1,9]);
end

% Determine Initial satellite Angular Velocity thorugh a stochastic process
function Sat = get_satellite_initial_angular_velocity(Sat,wSat_magn,wSat_magn_parallel_magField,q,magFieldECI)
% Define a arbitrary reference frame with the x axis parallel to the
% magnetic field
magFieldBody = quatrotate(q,magFieldECI);
ex = magFieldBody/norm(magFieldBody);
ey = cross([0 0 1],ex);
ey = ey/norm(ey);
ez = cross(ex,ey);

% Satellite Attitude Initial Conditions
aux_w = [rand rand];
aux_w = aux_w/norm(aux_w)*sqrt(wSat_magn^2-wSat_magn_parallel_magField^2);
Sat.W = deg2rad(wSat_magn_parallel_magField*ex + aux_w(1)*ey + aux_w(2)*ez); 
end

% Define the Structure with information relative to data saving sample
% times
function DataSaveSampleTimes = define_sample_times_for_data_save(setupTarget,filename)
dataSaveInfo = importdata(filename);

relevantLines = zeros(size(dataSaveInfo.data,1),1);
for i=1:length(setupTarget.indexVector)
    relevantLines = relevantLines + dataSaveInfo.data(:,setupTarget.indexVector(i)+1);
end
relevantLines = find(relevantLines>0);

for i=1:length(relevantLines)
     DataSaveSampleTimes.(dataSaveInfo.textdata{relevantLines(i)+1,2}) = dataSaveInfo.data(relevantLines(i),1);
end
end

function Simulation = ProcessEnables(Simulation)
if Simulation.detumblingOnly
    Simulation.enables.photodiodes        = 0;
    Simulation.enables.albedo             = 0;
    Simulation.enables.gyroscope          = 0;
    Simulation.enables.attitudeEstimator  = 0;
    Simulation.enables.positionEstimation = 0;
    Simulation.enables.magFieldEstimation = 0;
end
if Simulation.perfectSensing
    Simulation.enables.photodiodes        = 0;
    Simulation.enables.albedo             = 0;
    Simulation.enables.magnetometer       = 0;
    Simulation.enables.gyroscope          = 0;
    Simulation.enables.attitudeEstimator  = 0;
    Simulation.enables.positionEstimation = 0;
    Simulation.enables.magFieldEstimation = 0;   
end
if Simulation.enables.photodiodes == 0
    Simulation.enables.albedo             = 0;
end
end