[sph_x, sph_y, sph_z] = sphere(20);
sph_x = sph_x*(6378000);
sph_y = sph_y*(6378000);
sph_z = sph_z*(6378000);
mesh(sph_x, sph_y, sph_z, 'FaceAlpha', '0.5');
hold on
interval = (1900*2):(2500*2);
plot3(Orbit.SatPosECI(interval,1), Orbit.SatPosECI(interval,2), Orbit.SatPosECI(interval,3))
plot3(Beacon.GroundStationECI(interval,1), Beacon.GroundStationECI(interval,2), Beacon.GroundStationECI(interval,3), '.')
diff =Orbit.SatPosECI(interval,:)-Beacon.GroundStationECI(interval,:);
plot3(diff(:, 1), diff(:, 2), diff(:, 3));