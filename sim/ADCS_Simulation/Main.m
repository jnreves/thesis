clearvars -except ADCSModelComplete ACSModel ADSModel ADCSDetumblingModel

if ~exist('Simulation Data Repository', 'dir')
    mkdir('Simulation Data Repository')
end

%% Define Simulation Details
% these tags allow the user to create more than one possible model of 
% the same thing and determine which one should be used. For now only one
% version exists for each model (v1) 
simTags.Estimators           = 'v1';
simTags.MagneticField        = 'v1';
simTags.Albedo               = 'v1';
simTags.SatPosition          = 'v1';
simTags.MoonSun              = 'v1';

%% Setup Target(s)
% define the subset of models that the user wants to use
setupTarget.ADCS_Complete_Model        = 1;
setupTarget.ACS_Model                  = 0;
setupTarget.ADS_Model                  = 0; % for now this should be setup via ADCS_Complete_Mode, since that one needs to be run first
setupTarget.ADCS_Detumbling_Model      = 0;
setupTarget.MoonSun                    = 0;
setupTarget.SatPosition                = 0;
setupTarget.Earth_Magnetic_Field_Model = 0;
setupTarget.Earth_Albedo_Model         = 0;
setupTarget.Estimators_Model           = 0;
setupTarget.CCodeImplementation        = 0;

setupTarget.Beacon                     = 0;

Simulation.detumblingOnly = 0;
Simulation.perfectSensing = 0;

setupTarget = finish_setupTarget_definition(setupTarget);
  
%% Test Mode Definition
% these are used as flags that define if testing and validation should 
% be performed (where this is available)
testMode.MoonSun                    = 1;
testMode.SatPosition                = 1;
testMode.Earth_Magnetic_Field_Model = 1;
testMode.Earth_Albedo_Model         = 1;
testMode.Estimators_Model           = 1;
testMode.CCodeImplementation        = 1;


Simulation.standardSimDurations = [10000,20000,50000,100000];   % Simulation Durations that are considered for data saving

%% USER DEFINED 
Simulation.simDuration = 50000;
Simulation.stepSize = 0.025;

% Call the Functions that will performed the largest part of the setup
% process
Model_Setup;

function setupTarget = finish_setupTarget_definition(setupTarget)
% Establish precedences
setupTarget.MoonSun            = (setupTarget.ADCS_Complete_Model) || (setupTarget.ACS_Model)           || (setupTarget.MoonSun) || ...
                                 (setupTarget.SatPosition)         || (setupTarget.Earth_Albedo_Model)  || (setupTarget.CCodeImplementation) || (setupTarget.ADCS_Detumbling_Model);
setupTarget.SatPosition        = (setupTarget.ADCS_Complete_Model) || (setupTarget.ACS_Model)           || (setupTarget.ADCS_Detumbling_Model) ||...
                                 (setupTarget.SatPosition)         || (setupTarget.Earth_Albedo_Model)  || (setupTarget.CCodeImplementation) || ...
                                 (setupTarget.Earth_Magnetic_Field_Model);
setupTarget.Earth_Albedo_Model = (setupTarget.ADCS_Complete_Model) || (setupTarget.Earth_Albedo_Model)  || (setupTarget.CCodeImplementation);
setupTarget.Earth_Magnetic_Field_Model = (setupTarget.ADCS_Complete_Model)        || (setupTarget.ACS_Model)           || (setupTarget.ADCS_Detumbling_Model) ||...
                                         (setupTarget.Earth_Magnetic_Field_Model) || (setupTarget.CCodeImplementation);
setupTarget.Estimators_Model   = (setupTarget.ADCS_Complete_Model) || (setupTarget.Estimators_Model)  || (setupTarget.CCodeImplementation); 
% Define the Setup Indev Vector
aux = [setupTarget.ADCS_Complete_Model,setupTarget.ACS_Model,setupTarget.ADS_Model,...
       setupTarget.ADCS_Detumbling_Model, setupTarget.MoonSun, setupTarget.SatPosition,...
       setupTarget.Earth_Magnetic_Field_Model, setupTarget.Earth_Albedo_Model, setupTarget.CCodeImplementation];
   
setupTarget.indexVector = find(aux==1); 
end














