%kp_vec = linspace(0.00001, 0.0001, 10);
% kp_vec = linspace(7.8e-05, 9e-05, 3); % 2
kp_vec = linspace(5e-05, 10e-05, 10);
%kd_vec = linspace(0.000001, 0.00001, 10);
%kd_vec = linspace(2e-05, 2.6e-05, 3); % 2 
kd_vec = linspace(2e-05, 4e-05, 10);
clear des hat delta wsat_hat inputTorque
for j=1:size(kp_vec,2)
    Control.kp = kp_vec(j);    
    for i=1:size(kd_vec,2)
        Control.kd = kd_vec(i);
        sim('ADCS_Model_Complete');
        des(j,i)= ans.des;
        hat(j, i) =ans.hat;
        delta(j, i) = ans.delta;
        wsat_hat(j, i) = ans.wsat_hat;
        inputTorque(j, i) = ans.inputTorque;
        figure;
        subplot(3,1,1);
        plot(delta(j, i));
        subplot(3,1,2);
        plot(wsat_hat(j, i));
        subplot(3,1,3);
        plot(inputTorque(j, i));
        sgtitle("delta_q, wsat_hat  and input Torque for kp =" + num2str(Control.kp) + " and kd =" + num2str(Control.kd)); 
    end
end
%%
j = 1;
Control.kp = kp_vec(j);
for i=1:size(kd_vec,2)
        Control.kd = kd_vec(i);
        figure;
        subplot(2,1,1);
        plot(delta(j, i));
        subplot(2,1,2);
        plot(wsat_hat(j, i));
        sgtitle("delta_q  and wsat_hat for kp =" + num2str(Control.kp) + " and kd =" + num2str(Control.kd)); 
end
    






%%
u1 = [1, 0, 0];
u2 = [0, 1, 0];
v = cross(u1,u2);
ssc = [0 -v(3) v(2); v(3) 0 -v(1); -v(2) v(1) 0];
R = eye(3) + ssc + ssc^2*(1-dot(u1,u2))/(norm(v))^2
u2' - R*u1'
rad2deg(dcm2angle(R))

