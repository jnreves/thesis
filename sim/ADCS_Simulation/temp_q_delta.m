q1_in = Sat.q;
q2_in = Sat.q - 0.001;
q2_in = q2_in/norm(q2_in);

q1 = [q1_in(2) q1_in(3) q1_in(4) q1_in(1)];
q2 = [q2_in(2) q2_in(3) q2_in(4) q2_in(1)];

q2_conj = [-eye(3), zeros(3, 1); zeros(1, 3), 1]*q2';
q2_inv = q2_conj/(norm(q2)^2);

q1_cross = [0, -q1(3), q1(2);...
            q1(3), 0, -q1(1);...
            -q1(2), q1(1), 0];
q1_ext = [q1(4)*eye(3)-q1_cross, q1(1:3)';...
            -q1(1:3), q1(4)];
delta_q = q1_ext*q2_inv


delta_q_out = [delta_q(4) delta_q(1) delta_q(2) delta_q(3)]' % unconvert q