%% Temporary
%q_init = Sat.q;
%q_init = [-0.4104 0.3900 0.7697 0.2951]; % 9.8 deg init error
q_init = [-0.4776 0.3602 0.7577 0.2608]; % 5.3 deg init error
b_init = [0,0,0];
c = 0.000001;
P_init = diag([c, c, c, c, c, c]);
mekf.enable = 1;

mekf.propagation_delta = 0.025;
mekf.update_delta = 0.1;
% Noise config 0 no noise, 1 noise
mekf.noise.w = 1; 
mekf.noise.magBody = 1;
mekf.noise.sunBody = 1;
mekf.noise.magECI = 1;

mekf.update_enable = 1; % 0 or 1
mekf.always_update = 1; % 0 or 1
mekf.control = -1;


Sensors.coarseSunSensor.fov = 50;
Sensors.coarseSunSensor.freq = 40;
Simulation.enables.coarseSunSensor = 1;
Sensors.coarseSunSensor.xgain = 0.5;
Sensors.coarseSunSensor.ygain = 0.5;
%Sensors.coarseSunSensor

eclipse = 0;

% Nano Avionics RW01
% Sensors.rw.freq = 40;
% Sensors.rw.mass = 0.137;
% Sensors.rw.RPM = 6400;
% Sensors.rw.r = 0.0234;
% Sensors.rw.k = 0.403;
% Sensors.rw.momentumMax = 20*10^-3;
% Sensors.rw.torqueMax = 3.2*10^-3;

% Cubespace Cubewheel Small
Sensors.rw.freq = 40; % to do, but approximated
Sensors.rw.mass = 0.05; % estimated based on total weight
Sensors.rw.RPM = 7990;
Sensors.rw.r = 0.0230; 
Sensors.rw.k = 0.1178; % to do
Sensors.rw.momentumMax = 1.77*10^-3;
Sensors.rw.torqueMax = 0.23*10^-3;

% Inertia Tensor
    Sat.I

% Create Estimation Data Bus for Simulink Model
rw_bus_info = Simulink.Bus.createObject(Sensors.rw);
rw_bus = evalin('base', rw_bus_info.busName);


