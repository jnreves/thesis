% vector definition
vec = -Beacon.GroundStationECI(1, 1:3)+Sat.posInit';
%u1 = -u1;
vec = vec/norm(vec);
z = [0, 0, 1];
%u2 = -u2;
z = z/norm(z);

% method 1
v = cross(vec,z);
ssc = [0 -v(3) v(2); v(3) 0 -v(1); -v(2) v(1) 0];
R1 = eye(3) + ssc + ssc^2*(1-dot(vec,z))/(norm(v))^2
z' - R1*vec'
rad2deg(dcm2angle(R1))

% vector definition
vec = -Beacon.GroundStationECI(1, 1:3)+Sat.posInit';
%u1 = -u1;
vec = vec/norm(vec);
z = [0, 0, -1];
%u2 = -u2;
z = z/norm(z);


% method 2
vec= -vec;
z = -z;
r3 = vec';

r2 = skew(r3)*[1;0;0];
r2 = r2/norm(r2);
r1 = (skew(r3)^2)*[1;0;0];
r1 = r1/norm(r1);

R2 = [r1, r2, r3]'
z' - R2*vec'

