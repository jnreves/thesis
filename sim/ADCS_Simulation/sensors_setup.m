Sensors.coarseSunSensor.fov = 50;
Sensors.coarseSunSensor.freq = 40;
Sensors.coarseSunSensor.xgain = 0.5;
Sensors.coarseSunSensor.ygain = 0.5;
Simulation.enables.coarseSunSensor = 1;

eclipse = 0;
Sensors.rw.freq = 40;
Sensors.rw.mass = 0.137;
Sensors.rw.r = 0.0234;
Sensors.rw.k = 0.403;
Sensors.rw.momentumMax = 20*10^-3;
Sensors.rw.torqueMax = 3.2*10^-3;


% Create Estimation Data Bus for Simulink Model
rw_bus_info = Simulink.Bus.createObject(Sensors.rw);
rw_bus = evalin('base', rw_bus_info.busName);