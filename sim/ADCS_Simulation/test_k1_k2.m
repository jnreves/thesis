clear min_ang avg simout
% Control.k1 = 5.500e-04;
% Control.k2 = 0.0008;
% v old
% kp_vec = linspace(7e-04, 9e-04, 4);
% kd_vec = linspace(0.0005, 0.0006, 4);

%old
% kp_vec = linspace(7e-04, 15e-04, 2);
% kd_vec = linspace(0.0009, 0.0015, 2);

% for H
% kp_vec = linspace(0.7e-04, 2e-04, 3); % j = 3
% kd_vec = linspace(0.0015, 0.0025, 2); % i = 1

simout = zeros(4, 4, 1001);
for j=1:size(kp_vec,2)
    Control.k1H = kp_vec(j);    
    for i=1:size(kd_vec,2)
        Control.k2H = kd_vec(i);
        sim('Control_only');
        if (size(ans.ang_error.Data, 1) >5000)
            %simout(j,i, :)= ans.ang_error.Data(2000*2:2500*2);
            simout(j,i, :)= ans.ang_error.Data(1:1001);
            for h = 1:size(simout, 3)
                a(h) = simout(j, i, h);
            end 
        end
         figure;
        plot(a);
        sgtitle("an error for k1 =" + num2str(Control.k1H) + " and k2 =" + num2str(Control.k2H)); 
%         min_ang(j,i) = min(simout(j,i, :));
%         avg(j,i) = sum(simout(j,i, :),'omitnan')/(nnz(~isnan(simout(j,i, :))));
    end
end
beep();

%% 
i = 3;
j = 4;
figure;
for h = 1:size(simout, 3)
    a(h) = simout(j, i, h);
end 
plot(a);
sgtitle("an error for k1 =" + num2str(kp_vec(j)) + " and k2 =" + num2str( kd_vec(i)));

%% 
i = 1;
j = 3;
Control.k2H = kd_vec(i);
Control.k1H = kp_vec(j);  
sim('Control_only');


