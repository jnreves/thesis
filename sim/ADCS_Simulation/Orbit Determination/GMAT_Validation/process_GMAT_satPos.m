rawdata = importdata('ReportSatPos.txt');
[sz1,sz2] = size(rawdata.data);
PositionValidation = zeros(sz1,sz2);
PositionValidation(:,4) = (rawdata.data(:,1) - rawdata.data(1,1))*86400;
PositionValidation(:,1) = rawdata.data(:,2)*1000;
PositionValidation(:,2) = rawdata.data(:,3)*1000;
PositionValidation(:,3) = rawdata.data(:,4)*1000;

VelocityValidation(:,4) = (rawdata.data(:,1) - rawdata.data(1,1))*86400;
VelocityValidation(:,1) = rawdata.data(:,5)*1000;
VelocityValidation(:,2) = rawdata.data(:,6)*1000;
VelocityValidation(:,3) = rawdata.data(:,7)*1000;
save('PositionValidationGMAT','PositionValidation','VelocityValidation');