function X = Sat_Position_Estimator(X,stepSize,DCM,time,C,S)
% X are the translation states, 3d position vector + 3d velocity vector (6x1)
    k1 = compute_Xdot(X,time,DCM,C,S);
    k2 = compute_Xdot(X+k1*stepSize/2,time+stepSize/2,DCM,C,S);
    k3 = compute_Xdot(X+k2*stepSize/2,time+stepSize/2,DCM,C,S);
    k4 = compute_Xdot(X+k3*stepSize,time+stepSize,DCM,C,S);
% Step forward in time
% fptr = fopen('testPositionMatlab.csv','a');
% fprintf(fptr,'%.8e,%.8e,%.8e,%.8e,%.8e,%.8e\n',X(1),X(2),X(3),X(4),X(5),X(6));
X = X+(stepSize/6)*(k1+2*k2+2*k3+k4);
% fprintf(fptr,'%.8e,%.8e,%.8e,%.8e,%.8e,%.8e\n',k1(1),k1(2),k1(3),k1(4),k1(5),k1(6));
% fprintf(fptr,'%.8e,%.8e,%.8e,%.8e,%.8e,%.8e\n',k2(1),k2(2),k2(3),k2(4),k2(5),k2(6));
% fprintf(fptr,'%.8e,%.8e,%.8e,%.8e,%.8e,%.8e\n',k3(1),k3(2),k3(3),k3(4),k3(5),k3(6));
% fprintf(fptr,'%.8e,%.8e,%.8e,%.8e,%.8e,%.8e\n',k4(1),k4(2),k4(3),k4(4),k4(5),k4(6));
% fprintf(fptr,'%.8e,%.8e,%.8e,%.8e,%.8e,%.8e\n',X(1),X(2),X(3),X(4),X(5),X(6));
% fclose(fptr);
end

function Xdot = compute_Xdot(X,time,DCM,C,S)
    % Constants
    muEarth = 3.986004418*10^14;
    sphericalHarmonicsDegree = 8;  % Spherical Harmonics Approximation of gravity
    % Define base variables
    r = X(1:3);                % Position (m)
    v = X(4:6);                % Velocity (ms^2)
    % Reference transformations
    satPosECEF = eci2ecef_transform(r,time,DCM);
    satPosSph = ecef2sph_transform(satPosECEF);
    phi = satPosSph(1);    % latitudes
    lambda = satPosSph(2); % longitude 
    % dv components
    dvEarthECEF_pert = Spherical_Harmonics_Gravity_Computation(satPosECEF,phi,lambda,sphericalHarmonicsDegree,C,S);
    dvEarthECI_pert = ecef2eci_transform(dvEarthECEF_pert,time,DCM);
    dvEarthECI = -(muEarth/norm(r)^3)*r;
    % xdot computation
    dr = v;
    dv = dvEarthECI+dvEarthECI_pert;
    Xdot = [dr; dv];
%     fptr = fopen('testPositionMatlab.csv','a');
%     fprintf(fptr,'%.8e,%.8e,%.8e\n',dvEarthECI_pert(1),dvEarthECI_pert(2),dvEarthECI_pert(3));
%     fclose(fptr);
end