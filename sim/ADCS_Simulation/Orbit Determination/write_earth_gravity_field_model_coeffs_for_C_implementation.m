load('CoeffsGravitySphericalHarmonics.mat');
[sz1,sz2] = size(C);
fileID = fopen('coeffs_for_C_implementation.txt','w');
NumTerms = (sz1+1)/2*sz2;

fprintf(fileID,'{0');
i=1;
for n=1:10
    for m=0:n
        fprintf(fileID,',%d',round(C(n+1,m+1)*10^14));
        i=i+1;
        if i/10 == ceil(i/10)
             fprintf(fileID,'\n');
        end
    end
end
 fprintf(fileID,'};\n');
 
fprintf(fileID,'{0');
i=1;
for n=1:10
    for m=0:n
        fprintf(fileID,',%d',round(S(n+1,m+1)*10^15));
                i=i+1;
        if i/10 == ceil(i/10)
             fprintf(fileID,'\n');
        end
    end
end
 fprintf(fileID,'};\n');
 