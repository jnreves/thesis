function [SatPosECI,SatVelECI,avrgErrorSphHarmGravModel] = Sat_Position_Propagator_RK8(stepSize,simulationDuration,satPosInit,satVelInit,MoonPosECI,SunPosECI,DCM,testModeOn)
muEarth = 3.986004418*10^14;
%% Initializations
time = 0; % sec
i = 2;
indexSunPosArray = 1;
indexMoonPosArray = 1;
satPosPrev = satPosInit;
satVelPrev = satVelInit;

%% Save variables
SatPosECI(1,1:3) = satPosPrev;
SatVelECI(1,1:3) = satVelPrev;
SatPosECI(1,4) = time;
SatVelECI(1,4) = time;
% Test Spherical Harmonics Model Accuracy
avrgErrorSphHarmGravModel = 0;
numSphHarmGravModelEval = 0;
while time < simulationDuration
    %% Test spherical harmonics gravity calculation accuracy
    if ceil(time/1000) == time/1000 && testModeOn && time ~= 0
        satPosECEF = eci2ecef_transform(satPos,time,DCM);
        satPosSph = ecef2sph_transform(satPosECEF);
        phi = satPosSph(1);    % latitude
        lambda = satPosSph(2); % longitude
        % dv components
        dvEarthECEF_ref = zeros(3,1);
        load('CoeffsGravitySphericalHarmonics.mat');
        dvEarthECEF_test_pert = Spherical_Harmonics_Gravity_Computation(satPosECEF,phi,lambda,18,C,S);
        dvEarthECEF_test = -(muEarth/norm(satPosECEF)^3)*satPosECEF + dvEarthECEF_test_pert; 
        [dvEarthECEF_ref(1),dvEarthECEF_ref(2),dvEarthECEF_ref(3)] = gravitysphericalharmonic(satPosECEF', 'EGM2008', 18);
        avrgErrorSphHarmGravModel = avrgErrorSphHarmGravModel + dvEarthECEF_test - dvEarthECEF_ref;
        numSphHarmGravModelEval = numSphHarmGravModelEval + 1;   
    end
    %% Find location of the correct moon and sun positions on the table
    while time > MoonPosECI(indexMoonPosArray,4)
        indexMoonPosArray=indexMoonPosArray+1;
    end
    while time > SunPosECI(indexSunPosArray,4)
        indexSunPosArray=indexSunPosArray+1;
    end
    
    %% Propagate Satellite Position
    % X are the translation states, 3d position vector + 3d velocity vector (6x1)
    X = [satPosPrev;satVelPrev];
    X = rk8_orbit_integration(X,stepSize,MoonPosECI(indexMoonPosArray,1:3)',SunPosECI(indexSunPosArray,1:3)',0,0,time,DCM);
    satPos = X(1:3);
    satVel = X(4:6);
    
    %% Save variables
    SatPosECI(i,1:3) = satPos;
    SatVelECI(i,1:3) = satVel;
    SatPosECI(i,4) = time+stepSize;
    SatVelECI(i,4) = time+stepSize;
    
    %% Housekeeping
    satPosPrev = satPos;
    satVelPrev = satVel;
    time = time + stepSize;
    i=i+1;
end
avrgErrorSphHarmGravModel = avrgErrorSphHarmGravModel/numSphHarmGravModelEval;
end

function X = rk8_orbit_integration(X,stepSize,moonPos,sunPos,dragK,solarPressK,time,DCM)
b9 = 1/5;
b10 = 13/180;
b1 = 0.05;
b2 = 0;
b3 = 0;
b4 = 0;
b5 = 0;
b6 = 0.272222222222 - b9;
b7 = 0.272222222222 - b10;
b8 = 0.355555555556;
b11 = 0.05;
c2  = 0.183850407856;
a21 = c2;
a32 = 0.016900486234/c2;
c3  = c2;
a31 = c3-a32;
a41 = 0.068943902946;
a42 = 0;
a43 = 0.206831708839;
a51 = 0.689439029462;
a52 = 0;
a53 = -2.585396360481;
a54 = -a53;
a61 = 0.082732683535;
a62 = 0;
a63 = 0;
a64 = 0.413663417677;
a65 = 0.330930734142;
a71 = 0.097115513262;
a72 = 0;
a73 = 0;
a74 = 0.097308686537;
a75 = -0.044005801467;
a76 = 0.022254766315;
a81 = -0.062320146988;
a82 = 0;
a83 = 0;
a84 = -0.259948900289;
a85 = 0.249582954483;
a86 = -0.113452633375;
a87 = 0.686138726168;
a91 = a61 + 0.045090916605/b9;
a92 = 0;
a93 = 0;
a94 = a64 + 0.021208416940/b9;
a95 = a65 - 0.218567298182/b9;
a96 = 0.083972393539/b9;
a97 = - 0.125928146333/b9;
a98 = 0.194223717431/b9;
a101 = a71 + 0.003060721281/b10;
a102 = 0;
a103 = 0;
a104 = a74 + 0.001439603760/b10;
a105 = a75 - 0.014836105169/b10;
a106 = a76 + 0.005699952702/b10 - 0.046098506765*b9/b10;
a107 = - 0.008547862550/b10;
a108 = + 0.013183689976/b10;
a109 = + 0.046098506765*b9/b10;
a111 = -0.499040783934;
a112 = 0;
a113 = 0;
a114 = -1.386394134872;
a115 = 1.331109090575;
a116 = -0.167728029907 - 2.531493157611*b9;
a117 =  2.314646450731 - 16.546536707080*b10;
a118 = - 0.592592592593;
a119 = 2.531493157611*b9;
a1110 = 16.546536707080*b10;

k1 = xdot_computation_satellite(moonPos,sunPos,dragK,solarPressK,time,DCM,X                                                                                               );
k2 = xdot_computation_satellite(moonPos,sunPos,dragK,solarPressK,time,DCM,X + stepSize*(a21*k1)                                                                           );
k3 = xdot_computation_satellite(moonPos,sunPos,dragK,solarPressK,time,DCM,X + stepSize*(a31*k1+a32*k2)                                                                    );
k4 = xdot_computation_satellite(moonPos,sunPos,dragK,solarPressK,time,DCM,X + stepSize*(a41*k1+a42*k2+a43*k3)                                                             );
k5 = xdot_computation_satellite(moonPos,sunPos,dragK,solarPressK,time,DCM,X + stepSize*(a51*k1+a52*k2+a53*k3+a54*k4)                                                      );
k6 = xdot_computation_satellite(moonPos,sunPos,dragK,solarPressK,time,DCM,X + stepSize*(a61*k1+a62*k2+a63*k3+a64*k4+a65*k5)                                               );
k7 = xdot_computation_satellite(moonPos,sunPos,dragK,solarPressK,time,DCM,X + stepSize*(a71*k1+a72*k2+a73*k3+a74*k4+a75*k5+a76*k6)                                        );
k8 = xdot_computation_satellite(moonPos,sunPos,dragK,solarPressK,time,DCM,X + stepSize*(a81*k1+a82*k2+a83*k3+a84*k4+a85*k5+a86*k6+a87*k7)                                 );
k9 = xdot_computation_satellite(moonPos,sunPos,dragK,solarPressK,time,DCM,X + stepSize*(a91*k1+a92*k2+a93*k3+a94*k4+a95*k5+a96*k6+a97*k7+a98*k8)                          );
k10= xdot_computation_satellite(moonPos,sunPos,dragK,solarPressK,time,DCM,X + stepSize*(a101*k1+a102*k2+a103*k3+a104*k4+a105*k5+a106*k6+a107*k7+a108*k8+a109*k9)          );
k11= xdot_computation_satellite(moonPos,sunPos,dragK,solarPressK,time,DCM,X + stepSize*(a111*k1+a112*k2+a113*k3+a114*k4+a115*k5+a116*k6+a117*k7+a118*k8+a119*k9+a1110*k10));

X = X + stepSize*(b1*k1+b2*k2+b3*k3+b4*k4+b5*k5+b6*k6+b7*k7+b8*k8+b9*k9+b10*k10+b11*k11);
end

function Xdot = xdot_computation_satellite(moonPos,sunPos,dragK,solarPressK,time,DCM,X)
    load('CoeffsGravitySphericalHarmonics.mat');
    % Constants
    muSun = 1.32712440018*10^20; % Gravitational parameter Sun   (m^3/s^2)    
    muMoon = 4.9048695*10^12;     % Gravitational parameter Moon  (m^3/s^2)
    muEarth = 3.986004418*10^14;
    sphericalHarmonicsDegree = 18;  % Spherical Harmonics Approximation of gravity
    % Define base variables
    r = X(1:3);                % Position (m)
    v = X(4:6);                % Velocity (ms^2)
    rSuntoSat  = r - sunPos;
    rMoontoSat  = r - moonPos;
    % Reference transformations
    satPosECEF = eci2ecef_transform(r,time,DCM);
    satPosLLA = ecef2lla_transform(satPosECEF);
    phi = satPosLLA(1);    % latitude
    lambda = satPosLLA(2); % longitude 
    % dv components
    dvSun = -(muSun/norm(rSuntoSat)^3)*rSuntoSat;
    dvMoon = -(muMoon/norm(rMoontoSat)^3)*rMoontoSat;
    dvEarthECEF_pert = Spherical_Harmonics_Gravity_Computation(satPosECEF,phi,lambda,sphericalHarmonicsDegree,C,S);
    dvEarthECI_pert = ecef2eci_transform(dvEarthECEF_pert,time,DCM);
    dvEarthECI = -(muEarth/norm(r)^3)*r;
    % xdot computation
    dr = v;
    dv = dvEarthECI+dvEarthECI_pert; %dvSun+dvMoon;
    Xdot = [dr; dv];
end

