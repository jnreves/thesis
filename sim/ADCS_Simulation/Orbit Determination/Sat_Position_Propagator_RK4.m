function [SatPosECI,SatVelECI] = Sat_Position_Propagator_RK4(stepSize,simulationDuration,satPosInit,satVelInit,MoonPosECI,SunPosECI,DCM)
%% Initializations
time = 0; % sec
i = 2;
indexSunPosArray = 1;
indexMoonPosArray = 1;
satPosPrev = satPosInit;
satVelPrev = satVelInit;

%% Save variables
SatPosECI(1,1:3) = satPosPrev;
SatVelECI(1,1:3) = satVelPrev;
SatPosECI(1,4) = time;
SatVelECI(1,4) = time;

while time < simulationDuration
    %% Find location of the correct moon and sun positions on the table
    while time > MoonPosECI(indexMoonPosArray,4)
        indexMoonPosArray=indexMoonPosArray+1;
    end
    while time > SunPosECI(indexSunPosArray,4)
        indexSunPosArray=indexSunPosArray+1;
    end
    
    %% Propagate Satellite Position
    % X are the translation states, 3d position vector + 3d velocity vector (6x1)
    X = [satPosPrev;satVelPrev];
    X = rk4_orbit_integration(X,stepSize,MoonPosECI(indexMoonPosArray,1:3)',SunPosECI(indexSunPosArray,1:3)',0,0,time,DCM);
    satPos = X(1:3);
    satVel = X(4:6);
    
    %% Save variables
    SatPosECI(i,1:3) = satPos;
    SatVelECI(i,1:3) = satVel;
    SatPosECI(i,4) = time+stepSize;
    SatVelECI(i,4) = time+stepSize;
    
    %% Housekeeping
    satPosPrev = satPos;
    satVelPrev = satVel;
    time = time + stepSize;
    i=i+1;
end
end

function X = rk4_orbit_integration(X,stepSize,moonPos,sunPos,dragK,solarPressK,time,DCM)
% Numerical Solution, Runge-Kutta 4th Order
    k1 = xdot_computation_satellite(moonPos,sunPos,dragK,solarPressK,time,DCM,X                );
    k2 = xdot_computation_satellite(moonPos,sunPos,dragK,solarPressK,time,DCM,X+k1*stepSize/2  );
    k3 = xdot_computation_satellite(moonPos,sunPos,dragK,solarPressK,time,DCM,X+k2*stepSize/2  );
    k4 = xdot_computation_satellite(moonPos,sunPos,dragK,solarPressK,time,DCM,X+k3*stepSize    );
% Step forward in time
X = X+(stepSize/6)*(k1+2*k2+2*k3+k4);
end

function Xdot = xdot_computation_satellite(moonPos,sunPos,dragK,solarPressK,time,DCM,X)
    load('CoeffsGravitySphericalHarmonics.mat');
    % Constants
    muSun = 1.32712440018*10^20; % Gravitational parameter Sun   (m^3/s^2)    
    muMoon = 4.9048695*10^12;     % Gravitational parameter Moon  (m^3/s^2)
    muEarth = 3.986004418*10^14;
    sphericalHarmonicsDegree = 18;  % Spherical Harmonics Approximation of gravity
    % Define base variables
    r = X(1:3);                % Position (m)
    v = X(4:6);                % Velocity (ms^2)
    rSuntoSat  = r - sunPos;
    rMoontoSat  = r - moonPos;
    % Reference transformations
    satPosECEF = eci2ecef_transform(r,time,DCM);
    satPosLLA = ecef2lla_transform(satPosECEF);
    phi = satPosLLA(1);    % latitude
    lambda = satPosLLA(2); % longitude 
    % dv components
    dvSun = -(muSun/norm(rSuntoSat)^3)*rSuntoSat;
    dvMoon = -(muMoon/norm(rMoontoSat)^3)*rMoontoSat;
    dvEarthECEF_pert = Spherical_Harmonics_Gravity_Computation(satPosECEF,phi,lambda,sphericalHarmonicsDegree,C,S);
    dvEarthECI_pert = ecef2eci_transform(dvEarthECEF_pert,time,DCM);
    dvEarthECI = -(muEarth/norm(r)^3)*r;
    % xdot computation
    dr = v;
    dv = dvEarthECI+dvEarthECI_pert; % dvSun+dvMoon+
    Xdot = [dr; dv];
end
