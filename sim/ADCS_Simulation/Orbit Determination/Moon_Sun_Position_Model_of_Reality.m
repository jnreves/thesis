function [MoonSun_out] = Moon_Sun_Position_Model_of_Reality(MoonSun,simulationDuration,startJulianDate,testModeOn)
ephemerisUpdatePeriod = MoonSun.ephemerisUpdatePeriod;
stepSize              = MoonSun.stepSize;

% Rotate from Equator to Ecliptic
in = 23;
eci2ecie = [1 0 0; 0 cosd(in) sind(in); 0 -sind(in) cosd(in)];

%% Constants
sec2day = 1/86400;

%% Initializations
tableLength = ceil(simulationDuration/stepSize);
MoonPosECI = zeros(tableLength,4);
SunPosECI  = zeros(tableLength,4);

earthStatesPrev = zeros(6,1);
moonStatesPrev = zeros(6,1);
earthStates = zeros(6,1);
moonStates = zeros(6,1);

%% Main Cycle
% cycle initializations
time = 0;  % sec
i=2;
% Variables used to determine the quality of the position propagation -
% testModeOn = 1 only
avrgDrift.earthPosition = 0;
avrgDrift.moonPosition = 0;
avrgDrift.earthVelocity = 0;
avrgDrift.moonVelocity = 0;
numPositionUpdates = 0;


while time < simulationDuration
    %% Update Positions with ephemeris calculation (chebyshev functions)
    if ceil(time/ephemerisUpdatePeriod) == time/ephemerisUpdatePeriod     % detect if it is time for an update
        julianDate = startJulianDate+time*sec2day;
        [earthStatesPrev,moonStatesPrev] = update_with_ephemeris(julianDate);
        %% Save initial variable values
        if time == 0
            MoonPosECI(1,1:3) = moonStatesPrev(1:3)-earthStatesPrev(1:3);
            SunPosECI(1,1:3)  = eci2ecie*(-earthStatesPrev(1:3));
            MoonPosECI(1,4)   = time;
            SunPosECI(1,4)    = time;
        %% TestMode
        elseif testModeOn
            avrgDrift.earthPosition = avrgDrift.earthPosition + norm(earthStates(1:3) - earthStatesPrev(1:3)) / norm(earthStatesPrev(1:3));
            avrgDrift.moonPosition  = avrgDrift.moonPosition  + norm(moonStates(1:3)  - moonStatesPrev(1:3))  / norm(moonStatesPrev(1:3)-earthStatesPrev(1:3));
            avrgDrift.earthVelocity = avrgDrift.earthVelocity + norm(earthStates(4:6) - earthStatesPrev(4:6)) / norm(earthStatesPrev(4:6));
            avrgDrift.moonVelocity  = avrgDrift.moonVelocity  + norm(moonStates(4:6)  - moonStatesPrev(4:6))  / norm(moonStatesPrev(4:6)-earthStatesPrev(4:6));
            numPositionUpdates = numPositionUpdates + 1;
        end
    end
    
    %% Propagate Earth and Moon Positions
    % X are the translation states, 3d position vector + 3d velocity vector (6x1)
    
    % Moon
    moonStates  = rk4_orbit_integration(moonStatesPrev, stepSize, earthStatesPrev(1:3), 'M');
    % Earth
    earthStates = rk4_orbit_integration(earthStatesPrev, stepSize, moonStatesPrev(1:3),  'E');
    
    %% Save variables
    MoonPosECI(i,1:3) = moonStates(1:3)-earthStatesPrev(1:3);
    SunPosECI(i,1:3)  = eci2ecie*(-earthStates(1:3));
    MoonPosECI(i,4)   = time+stepSize;
    SunPosECI(i,4)    = time+stepSize;
    
    %% Cycle Housekeeping
    moonStatesPrev = moonStates;
    earthStatesPrev = earthStates;
    time = time + stepSize;
    i=i+1;
end
%% TestMode
if testModeOn
    avrgDrift.earthPosition = avrgDrift.earthPosition / numPositionUpdates;
    avrgDrift.moonPosition  = avrgDrift.moonPosition  / numPositionUpdates;
    avrgDrift.earthVelocity = avrgDrift.earthVelocity / numPositionUpdates;
    avrgDrift.moonVelocity  = avrgDrift.moonVelocity  / numPositionUpdates;
end
% Output
MoonSun_out.MoonPosECI = MoonPosECI;
MoonSun_out.SunPosECI  = SunPosECI;
MoonSun_out.avrgDrift  = avrgDrift;
end

%% RK4 integrator with distinct call functions for Xdot depending on the body (Earth or Moon)
function X = rk4_orbit_integration(X,h,thirdBodyPos,bodyIdentifier)
% X are the translation states, 3d position vector + 3d velocity vector (6x1)
if bodyIdentifier == 'E'
    % Numerical Solution, Runge-Kutta 4th Order
    k1 = xdot_computation_earth(X        ,thirdBodyPos);
    k2 = xdot_computation_earth(X+k1*h/2 ,thirdBodyPos);
    k3 = xdot_computation_earth(X+k2*h/2 ,thirdBodyPos);
    k4 = xdot_computation_earth(X+k3*h   ,thirdBodyPos);
    % Step forward in time
    X = X+(h/6)*(k1+2*k2+2*k3+k4);
elseif bodyIdentifier == 'M'
    % Numerical Solution, Runge-Kutta 4th Order
    k1 = xdot_computation_moon(X        ,thirdBodyPos);
    k2 = xdot_computation_moon(X+k1*h/2 ,thirdBodyPos);
    k3 = xdot_computation_moon(X+k2*h/2 ,thirdBodyPos);
    k4 = xdot_computation_moon(X+k3*h   ,thirdBodyPos);
    % Step forward in time
    X = X+(h/6)*(k1+2*k2+2*k3+k4);
end
end

%% Xdot computation for the Moon
function Xdot = xdot_computation_moon(X,thirdBodyPos)
    % Constants
    muSun = 1.32712440018*10^20; % Gravitational parameter Sun   (m^3/s^2)    
    muEarth = 3.986004418*10^14;   % Gravitational parameter Earth (m^3/s^2)
    % Define base variables
    r = X(1:3);                      % Position (m)
    rEarthtoMoon = X(1:3)-thirdBodyPos;
    v = X(4:6);                      % Velocity (ms^2)
    % dv components
    dvEarth = (-muEarth/(norm(rEarthtoMoon))^3).*rEarthtoMoon;     % Newton's law of gravity
    dvSun = (-muSun/(norm(r))^3).*r; % Newton's law of gravity
    % xdot computation
    dr = v;   
    dv = dvSun + dvEarth;
    Xdot = [dr; dv];
end

%% Xdot computation for Earth
function Xdot = xdot_computation_earth(X,thirdBodyPos)
    % Constants
    muSun = 1.32712440018*10^20; % Gravitational parameter Sun   (m^3/s^2) 
    muMoon = 4.9048695*10^12;     % Gravitational parameter Moon  (m^3/s^2)
    % Define base variables
    r = -X(1:3);                     % Position (m)
    rMoontoEarth = X(1:3)-thirdBodyPos;
    v = X(4:6);                     % Velocity (ms^2)
    dr = v;   
    % dv components
    dvSun = (-muSun/(norm(r))^3).*r;     % Newton's law of gravity
    dvMoon = (-muMoon/(norm(rMoontoEarth))^3).*rMoontoEarth; % Newton's law of gravity
    % xdot computation
    dv = dvSun + dvMoon;
    Xdot = [dr; dv];
end

function [earthStatesPrev,moonStatesPrev] = update_with_ephemeris(julianDate)
    % Earth ephemeris
    [earthPosPrev,earthVelPrev] = planetEphemeris(julianDate,'Sun','Earth','432t','km','Warning');
    earthStatesPrev(1:3,1) = earthPosPrev'*1000; % Convert to m
    earthStatesPrev(4:6,1) = earthVelPrev'*1000; % Convert to m/s
    % Moon ephemeris
    [moonPosPrev,moonVelPrev]   = planetEphemeris(julianDate,'Sun','Moon','432t','km','Warning');
    moonStatesPrev(1:3,1) = moonPosPrev'*1000; % Convert to m
    moonStatesPrev(4:6,1) = moonVelPrev'*1000; % Convert to m/s
end