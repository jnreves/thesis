% This function is based on the algorithm used for spherical harmonics
% calculation of the gravity acceleration that is used in the MatLab native
% function gravitysphericalharmonic()
%
% Inputs: satPos - satellite position in ECEF
%         phi    - satellite latitude (LLA position)
%         lambda - satellite longitude (LLA position)
%         nMax   - maximum degree for the spherical harmonics equations
%         C ; S  - matrices with the coefficients C and S for the spherical
%         harmonics equations
%
% Outputs: gravAccel - gravity acceleration vector in ECEF


function [gravAccel_pert] = Spherical_Harmonics_Gravity_Computation(satPos,phi,lambda,nMax,C,S)

%% Constants
muEarth = 3.986004418*10^14;
radiusEarth = 6378137;

%% Variable Initializations 
gravAccel_pert = zeros(3,1);
smlambda  = zeros(nMax+1,1);
cmlambda  = zeros(nMax+1,1);

%% 1 - Pre-compute cos(mlambda) and sin(mlambda)
slambda = sin(lambda);
clambda = cos(lambda);
smlambda(1) = 0;
cmlambda(1) = 1;
smlambda(2) = slambda;
cmlambda(2) = clambda;
for m=3:nMax+1
    smlambda(m) = 2.0.*clambda.*smlambda(m-1) - smlambda(m-2);
    cmlambda(m) = 2.0.*clambda.*cmlambda(m-1) - cmlambda(m-2);
end


%% 2 - Compute the normalized associated legendre functions

P = zeros(nMax+2, nMax+2);
ScaleFactor = zeros(nMax+2, nMax+2);
cphi = cos(pi/2-phi);   % the used latitude is the one measured from the north pole
sphi = sin(pi/2-phi);   % the used latitude is the one measured from the north pole

% Force numerical zeros to be exact zero
if abs(cphi)<=eps
    cphi = 0;
end
if abs(sphi)<=eps
    sphi = 0;
end
% Seeds for recursion formula
P(1,1) = 1;            % n = 0, m = 0;
P(2,1) = sqrt(3)*cphi; % n = 1, m = 0;
ScaleFactor(1,1) = 0;
ScaleFactor(2,1) = 1;
P(2,2) = sqrt(3)*sphi; % n = 1, m = 1;
ScaleFactor(2,2) = 0;
for n = 2:nMax+1
    k = n + 1;
    for m = 0:n
        p = m + 1;
        % Compute normalized associated legendre polynomials, P, via recursion relations 
        % Scale Factor needed for normalization of dUdphi partial derivative        
        if (n == m)           
            P(k,k) = (sqrt(2*n+1)/sqrt(2*n))*sphi*P(k-1,k-1);
            ScaleFactor(k,k) = 0;
        elseif (m == 0)
            P(k,p) = (sqrt(2*n+1)/n)*(sqrt(2*n-1)*cphi*P(k-1,p) - (n-1)/sqrt(2*n-3)*P(k-2,p));
            ScaleFactor(k,p) = sqrt((n+1)*(n)/2);
        else
            P(k,p) = sqrt(2*n+1)/(sqrt(n+m)*sqrt(n-m))*(sqrt(2*n-1)*cphi*P(k-1,p) - sqrt(n+m-1)*sqrt(n-m-1)/sqrt(2*n-3)*P(k-2,p));
            ScaleFactor(k,p) = sqrt((n+m+1)*(n-m));
        end

    end
end


% fptr = fopen('test2.csv','w');
% fprintf(fptr,'%.20f,%.20f\n',phi,lambda);
% for n = 0:nMax+1
%     k = n + 1;
%     for m = 0:n
%         p = m + 1;
%         fprintf(fptr,'%.20f,%.20f\n',P(k,p),ScaleFactor(k,p));
%     end
% end
% fclose(fptr);
%% 3 - Compute the acceleration components
% fptr = fopen('testML.csv','w');
rSat = norm(satPos);
rRatio   = radiusEarth/rSat;
rRatio_n = rRatio;
tphi = tan(phi);
% initialize summation of gravity in radial coordinates 
dUdrSum      = 0;
dUdphiSum    = 0;
dUdlambdaSum = 0;
% summation of gravity in radial coordinates
for n = 2:nMax
    k = n+1;
    rRatio_n      = rRatio_n*rRatio;
    % initialize summation of gravity in radial coordinates over m
    for m = 0:n
        j = m+1;
        dUdrSum      = dUdrSum + P(k,j)*(C(k,j)*cmlambda(j) + S(k,j)*smlambda(j))*rRatio_n*k; 
        dUdphiSum    = dUdphiSum + (P(k,j+1)*ScaleFactor(k,j) - tphi*m*P(k,j))*(C(k,j)*cmlambda(j) + S(k,j)*smlambda(j))*rRatio_n; 
        dUdlambdaSum = dUdlambdaSum + m*P(k,j)*(S(k,j)*cmlambda(j) - C(k,j)*smlambda(j))*rRatio_n;
%         fprintf(fptr,'%.8e,%.8e,%.8e,',P(k,j),P(k,j+1),ScaleFactor(k,j));
% 		fprintf(fptr,'%.8e,%.8e,',cmlambda(j),smlambda(j));
%         fprintf(fptr,'%.8e,%.8e,%.8e,',C(k,j),S(k,j),rRatio_n);
%         fprintf(fptr,'%.8e,%.8e,%.8e\n',dUdphiSum,dUdlambdaSum,-dUdrSum);
    end
end
% fclose(fptr);

% gravity in spherical coordinates
dUdr      = -muEarth/rSat^2*dUdrSum;
dUdphi    =  muEarth/rSat^2*dUdphiSum;      % actually is dUdphi/rSat
dUdlambda =  muEarth/rSat^2*dUdlambdaSum;   % actually is dUdlambda/rSat
% % gravity in ECEF coordinates
gravAccel_pert(1) = ((1/rSat)*dUdr - (satPos(3)/(rSat*sqrt(satPos(1)^2 + satPos(2)^2)))*dUdphi)*satPos(1) ...
               - (dUdlambda*rSat/(satPos(1)^2 + satPos(2)^2))*satPos(2); 
gravAccel_pert(2) = ((1/rSat)*dUdr - (satPos(3)/(rSat*sqrt(satPos(1)^2 + satPos(2)^2)))*dUdphi)*satPos(2) ...
               + (dUdlambda*rSat/(satPos(1)^2 + satPos(2)^2))*satPos(1); 
gravAccel_pert(3) = (1/rSat)*dUdr*satPos(3) + ((sqrt(satPos(1)^2 + satPos(2)^2))/(rSat))*dUdphi;

% special case for poles
atPole = abs(atan2(satPos(3),sqrt(satPos(1)^2 + satPos(2)^2)))==pi/2;
if atPole
    gravAccel_pert(1) = 0;
    gravAccel_pert(2) = 0;
    gravAccel_pert(3) = (1/rSat)*dUdr*satPos(3);
end
end