function [SatPosition_out] = Sat_Position_Model_of_Reality(SatPosition,Sat,Orbit,simulationDuration,testModeOn)

samplingPeriod = SatPosition.samplingPeriod;
stepSizeRK8    = SatPosition.stepSizeRK8;
satPosInit     = Sat.posInit;
satVelInit     = Sat.velInit;
MoonPosECI     = Orbit.MoonPosECI;
SunPosECI      = Orbit.SunPosECI;
DCM            = Orbit.DCM;

%% Initializations
timeArray = (0:samplingPeriod:simulationDuration)';
SatPosECI = zeros(length(timeArray),4);
SatVelECI = zeros(length(timeArray),4);
SatPosSph = zeros(length(timeArray),4);
SatPosECI(:,4) = timeArray;
SatVelECI(:,4) = timeArray;
SatPosSph(:,4) = timeArray;

%% Propagate Position
[satPosECI_RK,satVelECI_RK] = Sat_Position_Propagator_RK8(stepSizeRK8,simulationDuration,satPosInit,satVelInit,MoonPosECI,SunPosECI,DCM,testModeOn);


%% Interpolate positions to achieve a lower sampling period
% Also convert the ECI position to spherical coordinates
k=1;
for i=1:length(timeArray)
    while timeArray(i) > satPosECI_RK(k+1,4)
        k=k+1;
    end
    if timeArray(i) == satPosECI_RK(k+1,4)
        SatPosECI(i,1:3) = satPosECI_RK(k+1,1:3);
        SatVelECI(i,1:3) = satVelECI_RK(k+1,1:3);
    else
        if k==1
            r1 = satPosECI_RK(k  ,1:3);
            r2 = satPosECI_RK(k+1,1:3);
            r3 = satPosECI_RK(k+2,1:3);
            v1 = satVelECI_RK(k  ,1:3);
            v2 = satVelECI_RK(k+1,1:3);
            v3 = satVelECI_RK(k+2,1:3);
            t1 = satPosECI_RK(k  ,4);
            t2 = satPosECI_RK(k+1,4);
            t3 = satPosECI_RK(k+2,4);
            SatPosECI(i,1:3) = quadratic_interpolation(r1,r2,r3,t1,t2,t3,timeArray(i));
            SatVelECI(i,1:3) = quadratic_interpolation(v1,v2,v3,t1,t2,t3,timeArray(i));
        else
            r1 = satPosECI_RK(k-1,1:3);
            r2 = satPosECI_RK(k  ,1:3);
            r3 = satPosECI_RK(k+1,1:3);
            v1 = satVelECI_RK(k-1,1:3);
            v2 = satVelECI_RK(k  ,1:3);
            v3 = satVelECI_RK(k+1,1:3);
            t1 = satPosECI_RK(k-1,4);
            t2 = satPosECI_RK(k  ,4);
            t3 = satPosECI_RK(k+1,4);
            SatPosECI(i,1:3) = quadratic_interpolation(r1,r2,r3,t1,t2,t3,timeArray(i));
            SatVelECI(i,1:3) = quadratic_interpolation(v1,v2,v3,t1,t2,t3,timeArray(i));
        end    
    end
    SatPosECEF = eci2ecef_transform(SatPosECI(i,1:3)',SatPosECI(i,4),DCM);
    SatPosSph(i,1:3) = ecef2sph_transform(SatPosECEF)';
end


%% Determine Maximum Position Drift
load('GMAT_Validation/PositionValidationGMAT');
sz_PosVal = size(PositionValidation,1);
sz_SatPos = size(SatPosECI,1);
k=1;
drift = zeros(sz_PosVal,1);
for i=1:sz_PosVal
    while SatPosECI(k,4) < round(PositionValidation(i,4))
        k=k+1;
        if k > sz_SatPos
            break;
        end
    end
    if k > sz_SatPos
        i=i-1;
        break;
    else
        drift(i) = norm(SatPosECI(k,1:3)-PositionValidation(i,1:3));
    end  
end
maximumDrift = max(drift(1:i));
% Output
SatPosition_out.SatPosECI    = SatPosECI;
SatPosition_out.SatVelECI    = SatVelECI;
SatPosition_out.maximumDrift = maximumDrift;
end



%% Quadratic Interpolation
% obtain the value of r in relation to t having three function evaluations
% r1, r2 and r3 relative to t1,t2 and t3. t is a scalar, r may be vectorial
% and the function considered is r(t)
function r = quadratic_interpolation(r1,r2,r3,t1,t2,t3,t)
    g1 = (t-t2)*(t-t1)/((t3-t2)*(t3-t1));
    g2 = (t-t2)*(t3-t)/((t2-t1)*(t3-t1));
    r = r2 + g1*(r3-r2)+g2*(r2-r1);
end

