function vecLLA = ecef2lla_transform(vecECEF)
radiusEarth = 6378136;
vecLLA = zeros(3,1);
vecLLA(1) = asin((vecECEF(3))/norm(vecECEF));
vecLLA(2) = atan2(vecECEF(2),vecECEF(1));
vecLLA(3) = norm(vecECEF) - radiusEarth;
end
