function vecECEF = eci2ecef_transform(vecECI,time,DCM)
day2sec = 86400;
deg2rad = pi/180;

gsad = 360.98564737*time/day2sec;
%% Rotation from ECI to ECEF at startDate
vecAUX = DCM.eci2ecef*vecECI;
%% Rotation from startDate to current time
rotationMat2 = [ cos(deg2rad*gsad) ,  sin(deg2rad*gsad) , 0 ;...
                -sin(deg2rad*gsad) ,  cos(deg2rad*gsad) , 0 ;...
                 0 ,                  0 , 1 ];
vecECEF = rotationMat2*vecAUX;
end