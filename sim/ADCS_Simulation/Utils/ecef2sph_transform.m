function vecSPH = ecef2sph_transform(vecECEF)
vecSPH = zeros(3,1);
p = sqrt(vecECEF(1)^2+vecECEF(2)^2);
vecSPH(1) = atan(vecECEF(3)/p);
vecSPH(2) = atan2(vecECEF(2),vecECEF(1));
vecSPH(3) = sqrt(p^2+vecECEF(3)^2);
end
