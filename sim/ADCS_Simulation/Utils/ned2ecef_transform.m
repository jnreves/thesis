function vecECEF = ned2ecef_transform(vecNED,phi,lambda)  
vecECEF = zeros(3,1);
vecECEF(1) = -cos(lambda)*sin(phi)*vecNED(1) - sin(lambda)*vecNED(2) - cos(lambda)*cos(phi)*vecNED(3);                  
vecECEF(2) = -sin(lambda)*sin(phi)*vecNED(1) + cos(lambda)*vecNED(2) - sin(lambda)*cos(phi)*vecNED(3);                   
vecECEF(3) = cos(phi)*vecNED(1) - sin(phi)*vecNED(3);
