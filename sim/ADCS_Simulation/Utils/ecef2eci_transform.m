function vecECI = ecef2eci_transform(vecECEF,time,DCM)
day2sec = 86400;
deg2rad = pi/180;

gsad = 360.98564737*time/day2sec;
%% Rotation from current time to startTime
rotationMat1 = [cos(deg2rad*gsad) , -sin(deg2rad*gsad) , 0 ;...
                sin(deg2rad*gsad) ,  cos(deg2rad*gsad) , 0 ;...
                0 ,                  0                 , 1 ];
vecAUX = rotationMat1*vecECEF;

%% Rotation from ECEF at startDate to ECI
vecECI = DCM.ecef2eci*vecAUX;
end