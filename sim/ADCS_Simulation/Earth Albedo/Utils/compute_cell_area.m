% CELLAREA Calculate area of TOMS cell. Input is refl matrix
% indices.
%
% A = cellarea(i,j,sy,sx)
%
% i,j are the indices of the sy x sx REFL matrix.
%
% $Id: cellarea.m,v 1.7 2006/05/17 14:39:17 danji Exp $

function cellAreaMat = compute_cell_area(i,j,sy,sx)
radiusEarth = 6371.01e3;
deg2rad = pi/180;

dx = 2*pi/sx;
dy = pi/sy;
[~,phi] = idx2rad(i,j,dx,dy);

% Grid size
dphi = (180/sy)*deg2rad;
dtheta = (360/sx)*deg2rad;

% Diagonal points
phiMax = phi + dphi/2;
phiMin = phi - dphi/2;

cellAreaMat = radiusEarth^2*dtheta*(cos(phiMin)-cos(phiMax));

return
