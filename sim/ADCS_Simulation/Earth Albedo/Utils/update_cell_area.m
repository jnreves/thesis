load('../AlbedoModelInputData.mat')
[sy sx] = size(reflData);
cellArea = zeros(sy,sx);
for i=1:sy
    for j=1:sx
        cellArea(i,j) = compute_cell_area(i,j,sy,sx);
    end
end
save('../AlbedoModelInputData.mat','reflData','cellArea');