% EARTHFOV Field of view on earth by spherical coordinates.
%
% result = earthfov(satsph, refl [, type])
%
% satsph is the vector to the satellite in ECEF frame and spherical
% coordinates. refl is the reflectivity data used for plotting the fov.
% type is 'p' for 3D plot and 's' for spherical. If refl or type are
% unspecified no plot is generated. If refl is not specified, refl must be 
% a vector of the latitudal (refl(1)) and longitudal (refl(2)) resolution 
% of the output data.
%
% $Id: earthfov.m,v 1.15 2006/02/23 08:31:33 danji Exp $

function FoVMat = compute_fov_mat(satPosSph,sy,sx)
radiusEarth = 6371.01e3;

dx = 2*pi/sx;
dy = pi/sy;
FoVMat = zeros(sy,sx);

% Small Circle Center
theta0 = satPosSph(1);
phi0 = satPosSph(2);

% FOV on earth
rho = acos(radiusEarth/satPosSph(3));

for i = 1:sy
	for j = 1:sx        
		[theta, phi] = idx2rad(i,j,dx,dy);
		% Radial distance
		rd = acos(sin(phi0)*sin(phi)*cos(theta0-theta)+cos(phi0)*cos(phi));
		if rd <= rho
			FoVMat(i,j) = 1;
		end
	end
end
end
