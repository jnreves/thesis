[sx, sy] = size(reflData);
newReflData = zeros(sx/2,sy/2);
for i=1:sx/2
    for j=1:sy/2
        newReflData(i,j)=(reflData(i*2-1,j*2-1)+reflData(i*2,j*2-1)+reflData(i*2-1,j*2)+reflData(i*2,j*2))/4;
    end
end
