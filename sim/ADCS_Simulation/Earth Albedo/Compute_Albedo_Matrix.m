function albedoMat = Compute_Albedo_Matrix(satPosECI,sunPosECI,reflData,cellArea,time,DCM)
radiusEarth = 6371.01e3;

% Support row vectors
if length(satPosECI) > size(satPosECI,1)
  satPosECI = satPosECI';
end
if length(sunPosECI) > size(sunPosECI,1)
  sunPosECI = sunPosECI';
end

% Data size
[sy, sx] = size(reflData);
dx = 2*pi/sx;
dy = pi/sy;

satPos = eci2ecef_transform(satPosECI,time,DCM);
sunPos = eci2ecef_transform(sunPosECI,time,DCM);
satPosSph = zeros(3,1);
sunPosSph = zeros(3,1);

% Spherical coordinates
[satPosSph(1),satPosSph(2),satPosSph(3)] = cart2sph(satPos(1),satPos(2),satPos(3));
[sunPosSph(1),sunPosSph(2),sunPosSph(3)] = cart2sph(sunPos(1),sunPos(2),sunPos(3));

% Convert phi to polar angle
satPosSph(2) = pi/2 - satPosSph(2);
sunPosSph(2) = pi/2 - sunPosSph(2);

% REFL indices
[sun_i, sun_j] = rad2idx(sunPosSph(1),sunPosSph(2),sy,sx);

% Visible elements
fov = compute_fov_mat(satPosSph,sy,sx);

% Sunlit elements
vis = compute_fov_mat(sunPosSph,sy,sx);

% Loop through refl array and select sunlit and satellite visible cells
gridPos = zeros(3,1);
albedoMat = zeros(1500,4);
k=1;
for i=1:sy
    for j=1:sx
        if (fov(i,j) && vis(i,j))
            % Angle of incident solar irradiance
            phiIn = compute_grid_angle(i,j,sun_i,sun_j,dx,dy);
            % Account for numerical inaccuracies.
            if phiIn > pi/2
                phiIn = pi/2;
            end
            % Incident normalized power
            PowerIn = cellArea(i,j)*cos(phiIn);
            % Position of the grid element
            [gridTheta, gridPhi] = idx2rad(i,j,dx,dy);
            [gridPos(1), gridPos(2), gridPos(3)] = sph2cart(gridTheta,pi/2-gridPhi,radiusEarth);
            % Reflected normalized power
                  
            albedoMat(k,1) = PowerIn*reflData(i,j);
            albedoMat(k,2:4) = ecef2eci_transform(gridPos,time,DCM)';   
            k=k+1;
        end
    end
end
end
