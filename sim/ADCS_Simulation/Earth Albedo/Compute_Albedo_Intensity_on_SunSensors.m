function SSAlbedoInt = Compute_Albedo_Intensity_on_SunSensors(satPos,satX,satY,satZ,albedoMat)

% Support row vectors
if length(satPos) > size(satPos,1)
  satPos = satPos';
end

% Normalize the basis vectors for the body reference and support row
% vectors
satX = satX/norm(satX);
satY = satY/norm(satY);
satZ = satZ/norm(satZ);
if length(satX) > size(satX,1)
  satX = satX';
end
if length(satY) > size(satY,1)
  satY = satY';
end
if length(satZ) > size(satZ,1)
  satZ = satZ';
end

% Loop through refl array and select sunlit and satellite visible cells
SSAlbedoInt = zeros(5,1);
sz = size(albedoMat,1);
for i=1:sz
    albedoPower = albedoMat(i,1);
    gridPos = albedoMat(i,2:4)';
    if norm(gridPos) == 0
        break;
    end
    % Angle to sat from grid
    satDist = norm(satPos-gridPos);
    cosPhiOut = ((satPos-gridPos)/satDist)'*gridPos/norm(gridPos);
    
    PowerOut = albedoPower*cosPhiOut/(pi*satDist^2);
    dotX = ((satPos-gridPos)/satDist)'*satX;
    dotY = ((satPos-gridPos)/satDist)'*satY;
    dotZ = ((satPos-gridPos)/satDist)'*satZ;
    % Albedo on +X and -X panels
    if dotX > 0
        SSAlbedoInt(3) = SSAlbedoInt(3) + PowerOut*dotX;
    else
        SSAlbedoInt(2) = SSAlbedoInt(2) - PowerOut*dotX;
    end
    % Albedo on +Y and -Y panels
    if dotY > 0
        SSAlbedoInt(5) = SSAlbedoInt(5) + PowerOut*dotY;
    else
        SSAlbedoInt(4) = SSAlbedoInt(4) - PowerOut*dotY;
    end
    % Albedo on -Z panel
    if dotZ < 0
        SSAlbedoInt(1) = SSAlbedoInt(1) - PowerOut*dotZ;
    end
end
end