clc; 
str = pwd;
cd('C:\Users\Rodrigo\Documents\Software\src\c\OBC\libs\ADCSv2\sim_wrapper_functions'); 
% % Coordinate Transform
% mex -g  ecef2sph_wrapper.c; 
% mex -g  eci2ecef_wrapper.c; 
% mex -g  ecef2eci_wrapper.c; 
% mex -g  ned2ecef_wrapper.c; 
% mex -g  earthECIE2sunECI_wrapper.c; 
% % Quaternion operations
% mex -g quat_rotation_wrapper.c
% mex -g rotm2quat_wrapper.c
% % Sun Sensor Processing
% mex -g ss_processing_wrapper.c
% %% TRIAD
% mex -g triad_wrapper.c
%% Sun Position Propagation
mex -g earth_position_propagator_wrapper.c
% % Magnetic Field
mex -g magnetic_model_wrapper.c
% % % Satellite Position Propagation
% mex -g gravity_field_model_wrapper.c 
mex -g position_propagator_wrapper.c
% % Controllers
% mex -g pointing_wrapper.c
% mex -g detumbling_wrapper.c
% % Attitude Estimator
% mex -g attitude_estimator_wrapper.c
% %% Integration Code
% mex -g integration_wrapper.c
% mex -g j2_wrapper.c
cd(str)