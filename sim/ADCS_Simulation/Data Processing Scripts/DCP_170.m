time = ADCSModelComplete.pointingError_totalEff.time;
pointing_errorEff = ADCSModelComplete.pointingError_totalEff.signals.values;


plot(time,pointing_errorEff);
hold on
plot(time,ones(size(time)) * 20);
title('Pointing error vs time');
xlabel('time (s)');
ylabel('Accuraty (deg)');
legend('pointing error', '20 deg');


pointing_errorEff(pointing_errorEff == 0) = []; % deleted values on which poiting is not valid

pointing_errorEffSorted = sort(pointing_errorEff,'descend');

worst10_percent = pointing_errorEffSorted(1:ceil(length(pointing_errorEffSorted)*0.1));

figure();
histogram(worst10_percent, 10, 'Normalization', 'probability');
title('Probability density of the worst 10% of the cases');

xlabel('Accuracy (deg)');

ylabel('Percentage');
