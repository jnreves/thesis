Control.controlInst     = 0;
Control.period = 0.5;
Sat.I = diag([6.65, 33.25, 33.25])*10^-3; %kg*m^2

Control.k1H = 2e-04;
Control.k2H =  0.0015;
Control.switchThreshold = 1.5;
Control.k1L = 0.001;
Control.k2L =  0.003;

Control.alpha1 = 0.6;
Control.alpha2 = 2*Control.alpha1/(Control.alpha1+1);
Control.sigma = 0.3;

Sat.q = [-0.547212863050710,0.325040552001621,0.738795346989299,0.221558473846721];   %Initial attitude.

Sat.W = [0.00592284901709164,-0.00139271885446594,0.00625576123743776]; % Initial angular velocity, changes every time a script is run

load('Simulation Data Repository/Beacon.mat');
load('Simulation Data Repository/SatPosition_v1_50000.mat');

Orbit.SatPosECI = SatPosition_out.SatPosECI;


% sliding

Control.sldK = 0.3; %0.015 %0.026
Control.sldEpsilon = 0.005; %0.01 %0.008
Control.sldG = 0.015*eye(3); %0.00015 % 0.0005
Control.sldJHat = Sat.I;



% gyro
Sensors.nMeas = 3;
Sensors.gyro1.freq     = 20;      % Gyroscope frequency sample rate (Hz)
Sensors.gyro1.AR_PSD   = 1e-4;    % Angle random noise power spectrum density 1e-4 (deg/s)^2/Hz (3.0462e-08 (rad/s)^2/Hz )
Sensors.gyro1.RRW_bias = 9E-5;    % Drift noise bias (not used) ~0.005º/s
  