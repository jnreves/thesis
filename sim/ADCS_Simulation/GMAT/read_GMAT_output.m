clear
ReadEclipseLocator = 0;
ReadContactLocator = 0;
ReadSatPos         = 1;

filename_Eclipse = 'EclipseLocator.txt';
filename_Contact = 'ContactLocator.txt';
filename_SatPos  = 'ReportSatPos.txt';

MaximumDurationSatPos = 1000000; % sec

startYear = 2021;

if ReadEclipseLocator
    %% Import data from GMAT file and remove unwanted lines
    A = importdata(filename_Eclipse,' ');
    A.textdata = A.textdata(3:end-4,:);

    %% Process Data Inside the field textdata
    sz = size(A.textdata,1);
    EclipseOccurences = zeros(floor(sz/3-1),1);
    k=1;
    start = 1;
    for i=1:sz
        % See if this is the start
        if i>1
            if A.data(i,1) > A.data(i-1,1)
                start = 1;
            else
                start = 0;
            end
        end
        % See if this is the end
        if i==sz
            finish = 1;
        elseif A.data(i+1,1) > A.data(i,1)   
            finish = 1;
        else
            finish = 0;
        end

        if start
            %% Column 1 - Day string to number
            EclipseOccurences(k,1) = str2num(A.textdata{i,1});
            % This variable will have the number of days passed from the initial
            % orbit - including decimal part
            %% Column 2 - Month name to number
            switch(A.textdata{i,2})
                case 'Jan'
                case 'Feb'
                    EclipseOccurences(k,1) = EclipseOccurences(k,1)+31;
                case 'Mar'
                    EclipseOccurences(k,1) = EclipseOccurences(k,1)+59;
                case 'Apr'
                    EclipseOccurences(k,1) = EclipseOccurences(k,1)+90;
                case 'May'
                    EclipseOccurences(k,1) = EclipseOccurences(k,1)+120;
                case 'Jun'
                    EclipseOccurences(k,1) = EclipseOccurences(k,1)+151;
                case 'Jul'
                    EclipseOccurences(k,1) = EclipseOccurences(k,1)+181;
                case 'Aug'
                    EclipseOccurences(k,1) = EclipseOccurences(k,1)+212;
                case 'Sep'
                    EclipseOccurences(k,1) = EclipseOccurences(k,1)+243;
                case 'Oct'
                    EclipseOccurences(k,1) = EclipseOccurences(k,1)+273;
                case 'Nov'
                    EclipseOccurences(k,1) = EclipseOccurences(k,1)+304;
                case 'Dec'
                    EclipseOccurences(k,1) = EclipseOccurences(k,1)+334;
            end
            %% Column 4 - Transform hour:min:sec to secs passed in the day
            EclipseOccurences(k,1) = EclipseOccurences(k,1)+HourOfDay2Secs(A.textdata{i,4})/86400;
        end

        if finish
            %% Column 5 - Day string to number
            EclipseOccurences(k,2) = str2num(A.textdata{i,5});
            %% Column 6 - Month name to number
            switch(A.textdata{i,6})
                case 'Jan'
                case 'Feb'
                    EclipseOccurences(k,2) = EclipseOccurences(k,2)+31;
                case 'Mar'
                    EclipseOccurences(k,2) = EclipseOccurences(k,2)+59;
                case 'Apr'
                    EclipseOccurences(k,2) = EclipseOccurences(k,2)+90;
                case 'May'
                    EclipseOccurences(k,2) = EclipseOccurences(k,2)+120;
                case 'Jun'
                    EclipseOccurences(k,2) = EclipseOccurences(k,2)+151;
                case 'Jul'
                    EclipseOccurences(k,2) = EclipseOccurences(k,2)+181;
                case 'Aug'
                    EclipseOccurences(k,2) = EclipseOccurences(k,2)+212;
                case 'Sep'
                    EclipseOccurences(k,2) = EclipseOccurences(k,2)+243;
                case 'Oct'
                    EclipseOccurences(k,2) = EclipseOccurences(k,2)+273;
                case 'Nov'
                    EclipseOccurences(k,2) = EclipseOccurences(k,2)+304;
                case 'Dec'
                    EclipseOccurences(k,2) = EclipseOccurences(k,2)+334;
            end
            %% Column 8 - Transform hour:min:sec to secs passed in the day
            EclipseOccurences(k,2) = EclipseOccurences(k,2)+HourOfDay2Secs(A.textdata{i,8})/86400;
            k=k+1;
        end
    end

    EclipseOccurencesJulian = zeros(floor(sz/3-1),1);
    JD_start = juliandate([startYear,0,0]);
    k=0;
    for i=1:length(EclipseOccurences)
        if i > 1
            if EclipseOccurences(i,1) < EclipseOccurences(i-1,2)
                k=k+1;
                JD_start = juliandate([startYear+k,0,0]);
            end
        end
        EclipseOccurencesJulian(i,1) = JD_start+EclipseOccurences(i,1);
        if EclipseOccurences(i,2) < EclipseOccurences(i,1)
            k=k+1;
            JD_start = juliandate([startYear+k,0,0]);
        end
        EclipseOccurencesJulian(i,2) = JD_start+EclipseOccurences(i,2);
    end
save('EclipseOccurencesJulian','EclipseOccurencesJulian')    
end

if ReadContactLocator
    %% Import data from GMAT file and remove unwanted lines
    A = importdata(filename_Contact,' ');
    sz = size(A.textdata,1);
    delims = find(strcmp(A.textdata(:,1),'Number'));
    num_stations = 4;
    start = 4;
    finish = [];
    for i=1:num_stations-1
        start = [start,delims(i)+3];
        finish = [finish,delims(i)-1];
    end
    finish = [finish,delims(end)-1];
    

    for i=1:num_stations
        lengthArray = finish(i)-start(i)+1;
        ContactOccurrences{i} = zeros(lengthArray,2);
        ContactOccurrencesJulian{i} = zeros(lengthArray,2);
    end
    
    for j=1:num_stations
        for i=1:finish(j)-start(j)+1    
            %% Column 1 - Day of the month
            ContactOccurrences{j}(i,1) = str2num(A.textdata{start(j)-1+i,1});
            %% Column 2 - Month name to number
            switch(A.textdata{start(j)-1+i,2})
                case 'Jan'
                case 'Feb'
                    ContactOccurrences{j}(i,1) = ContactOccurrences{j}(i,1)+31;
                case 'Mar'
                    ContactOccurrences{j}(i,1) = ContactOccurrences{j}(i,1)+59;
                case 'Apr'
                    ContactOccurrences{j}(i,1) = ContactOccurrences{j}(i,1)+90;
                case 'May'
                    ContactOccurrences{j}(i,1) = ContactOccurrences{j}(i,1)+120;
                case 'Jun'
                    ContactOccurrences{j}(i,1) = ContactOccurrences{j}(i,1)+151;
                case 'Jul'
                    ContactOccurrences{j}(i,1) = ContactOccurrences{j}(i,1)+181;
                case 'Aug'
                    ContactOccurrences{j}(i,1) = ContactOccurrences{j}(i,1)+212;
                case 'Sep'
                    ContactOccurrences{j}(i,1) = ContactOccurrences{j}(i,1)+243;
                case 'Oct'
                    ContactOccurrences{j}(i,1) = ContactOccurrences{j}(i,1)+273;
                case 'Nov'
                    ContactOccurrences{j}(i,1) = ContactOccurrences{j}(i,1)+304;
                case 'Dec'
                    ContactOccurrences{j}(i,1) = ContactOccurrences{j}(i,1)+334;
            end
            %% Column 4 - Transform hour:min:sec to secs passed in the day
            ContactOccurrences{j}(i,1) = ContactOccurrences{j}(i,1)+HourOfDay2Secs(A.textdata{start(j)-1+i,4})/86400;
            
            %% Column 5 - Day of the month
            ContactOccurrences{j}(i,2) = str2num(A.textdata{start(j)-1+i,5});
            %% Column 6 - Month name to number
            switch(A.textdata{start(j)-1+i,6})
                case 'Jan'
                case 'Feb'
                    ContactOccurrences{j}(i,2) = ContactOccurrences{j}(i,2)+31;
                case 'Mar'
                    ContactOccurrences{j}(i,2) = ContactOccurrences{j}(i,2)+59;
                case 'Apr'
                    ContactOccurrences{j}(i,2) = ContactOccurrences{j}(i,2)+90;
                case 'May'
                    ContactOccurrences{j}(i,2) = ContactOccurrences{j}(i,2)+120;
                case 'Jun'
                    ContactOccurrences{j}(i,2) = ContactOccurrences{j}(i,2)+151;
                case 'Jul'
                    ContactOccurrences{j}(i,2) = ContactOccurrences{j}(i,2)+181;
                case 'Aug'
                    ContactOccurrences{j}(i,2) = ContactOccurrences{j}(i,2)+212;
                case 'Sep'
                    ContactOccurrences{j}(i,2) = ContactOccurrences{j}(i,2)+243;
                case 'Oct'
                    ContactOccurrences{j}(i,2) = ContactOccurrences{j}(i,2)+273;
                case 'Nov'
                    ContactOccurrences{j}(i,2) = ContactOccurrences{j}(i,2)+304;
                case 'Dec'
                    ContactOccurrences{j}(i,2) = ContactOccurrences{j}(i,2)+334;
            end
            %% Column 8 - Transform hour:min:sec to secs passed in the day
            ContactOccurrences{j}(i,2) = ContactOccurrences{j}(i,2)+HourOfDay2Secs(A.textdata{start(j)-1+i,8})/86400;
        end
        
        JD_start = juliandate([startYear,0,0]);
        k=0;
        for i=1:length(ContactOccurrences{j})
            if i > 1
                if ContactOccurrences{j}(i,1) < ContactOccurrences{j}(i-1,2)
                    k=k+1;
                    JD_start = juliandate([startYear+k,0,0]);
                end
            end
            ContactOccurrencesJulian{j}(i,1) = JD_start+ContactOccurrences{j}(i,1);
            if ContactOccurrences{j}(i,2) < ContactOccurrences{j}(i,1)
                k=k+1;
                JD_start = juliandate([startYear+k,0,0]);
            end
            ContactOccurrencesJulian{j}(i,2) = JD_start+ContactOccurrences{j}(i,2);
        end
        
     a=1;   
    end
   save('ContactOccurrencesJulian','ContactOccurrencesJulian')

end

if ReadSatPos
    A = importdata(filename_SatPos);
    startDate=A.data(1,1);
    for i=1:length(A.data)
        A.data(i,1)=(A.data(i,1)-startDate)*86400;
        if A.data(i,1) > MaximumDurationSatPos
            break;
        end
    end
    Pos_Vel_GMAT = A.data(1:i,1:7);
       save('Pos_Vel_GMAT','Pos_Vel_GMAT')
end

function secs = HourOfDay2Secs(hour_str)
    hour_vec = textscan(hour_str,'%f:%f:%f');
    secs = hour_vec{1}*3600+hour_vec{2}*60+hour_vec{3};
end