load('ContactOccurrencesJulian');
for i=1:length(ContactOccurrencesJulian)
    sz = length(ContactOccurrencesJulian{i});
    TimeWithoutContact{i} = zeros(sz-1,1);
    for j=2:sz
        TimeWithoutContact{i}(j-1) = (ContactOccurrencesJulian{i}(j,1)-ContactOccurrencesJulian{i}(j-1,2))*24;
    end
    maxTimeWithoutContact(i)=max(TimeWithoutContact{i});
end
