
beacon_x = ADCSModelComplete.variance_beacon.Data(:, 1);
beacon_y = ADCSModelComplete.variance_beacon.Data(:, 2);
beacon_z = ADCSModelComplete.variance_beacon.Data(:, 3);

beacon_mean_x = sum(beacon_x)/size(beacon_x, 1);
beacon_mean_y = sum(beacon_y)/size(beacon_y, 1);
beacon_mean_z = sum(beacon_z)/size(beacon_z, 1);

beacon_sum_x = 0;
beacon_sum_y = 0;
beacon_sum_z = 0;

for i=1:size(beacon_x, 1) 
    beacon_sum_x = beacon_sum_x + (beacon_x(i)-beacon_mean_x)^2;
    beacon_sum_y = beacon_sum_y + (beacon_y(i)-beacon_mean_y)^2;
    beacon_sum_z = beacon_sum_z + (beacon_z(i)-beacon_mean_z)^2;   
end

beacon_var_x = beacon_sum_x/size(beacon_x ,1);
beacon_var_y = beacon_sum_y/size(beacon_x, 1);
beacon_var_z = beacon_sum_z/size(beacon_x, 1);

Beacon_R = diag([beacon_var_x, beacon_var_y, beacon_var_z])