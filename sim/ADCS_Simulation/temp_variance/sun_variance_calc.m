
sun_x = ADCSModelComplete.variance_sun.Data(:, 1);
sun_y = ADCSModelComplete.variance_sun.Data(:, 2);
sun_z = ADCSModelComplete.variance_sun.Data(:, 3);

sun_mean_x = sum(sun_x)/size(sun_x, 1);
sun_mean_y = sum(sun_y)/size(sun_y, 1);
sun_mean_z = sum(sun_z)/size(sun_z, 1);

sun_sum_x = 0;
sun_sum_y = 0;
sun_sum_z = 0;

for i=1:size(sun_x, 1) 
    sun_sum_x = sun_sum_x + (sun_x(i)-sun_mean_x)^2;
    sun_sum_y = sun_sum_y + (sun_y(i)-sun_mean_y)^2;
    sun_sum_z = sun_sum_z + (sun_z(i)-sun_mean_z)^2;   
end

sun_var_x = sun_sum_x/size(sun_x ,1);
sun_var_y = sun_sum_y/size(sun_x, 1);
sun_var_z = sun_sum_z/size(sun_x, 1);

sun_R = diag([sun_var_x, sun_var_y, sun_var_z])