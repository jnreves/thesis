
mag_x = ADCSModelComplete.variance_mag_nT.Data(:, 1);
mag_y = ADCSModelComplete.variance_mag_nT.Data(:, 2);
mag_z = ADCSModelComplete.variance_mag_nT.Data(:, 3);

mag_mean_x = sum(mag_x)/size(mag_x, 1);
mag_mean_y = sum(mag_y)/size(mag_y, 1);
mag_mean_z = sum(mag_z)/size(mag_z, 1);

mag_sum_x = 0;
mag_sum_y = 0;
mag_sum_z = 0;

for i=1:size(mag_x, 1) 
    mag_sum_x = mag_sum_x + (mag_x(i)-mag_mean_x)^2;
    mag_sum_y = mag_sum_y + (mag_y(i)-mag_mean_y)^2;
    mag_sum_z = mag_sum_z + (mag_z(i)-mag_mean_z)^2;   
end

mag_var_x = mag_sum_x/size(mag_x ,1);
mag_var_y = mag_sum_y/size(mag_x, 1);
mag_var_z = mag_sum_z/size(mag_x, 1);

mag_R = diag([mag_var_x, mag_var_y, mag_var_z])*10^(-9)