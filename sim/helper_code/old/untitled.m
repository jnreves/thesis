b1 = [0.1, 0.5, 0.9]';
b2 = [0.033, 0.055, 0.0987]';

b1_hat = [0.11, 0.52, 0.93]';
b2_hat = [0.032, 0.055, 0.099]';

b1_hat_cross = skew(b1_hat);
b2_hat_cross = skew(b2_hat);

y = [b1; b2];
h = [b1_hat; b2_hat];
% H = [b1_hat_cross, zeros(3,3);...
%     b2_hat_cross, zeros(3,3)];

H = [b1_hat_cross, zeros(3,3)];    
    
c = 0.001;
P_pri = diag([c, c, c, c, c, c]);

b_pri = [0.001,0.001, 0.001];

R = [eye(3)*(0.0055), zeros(3, 3); zeros(3, 3), diag([0.16, 0.16, 0.45])];

% kalman gain calculation
Kalman = P_pri*(H')*pinv(H*P_pri*(H')+R);

% covariance update
P_pos = (eye(6)-Kalman*(H))*P_pri;

% state update
delta_x_pos = Kalman*(y-h); %verify if h and y are the same size
b_pos = b_pri + delta_x_pos(4:6); 

%%

