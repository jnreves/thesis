q_n = ADCSModelComplete.q.signals.values; %sample time = 1 s
w = ADCSModelComplete.wSat.signals.values; %sample time = 0.05 s

we(:,1) = w(:,1);
we(:,2) = w(:,2);
we(:,3) = w(:,3);
we = deg2rad(we);
n_secs = 5;
dt = 0.05;
m = n_secs/0.05;
qe=zeros(m,4);
qe(2, 1) = q_n(2, 1);
qe(2, 2) = q_n(2, 2);
qe(2, 3) = q_n(2, 3);
qe(2, 4) = q_n(2, 4);



for i=2:m
%     % Propagate State
%     w=norm(we(i,:));
%     co=cos(0.5*w*dt);
%     si=sin(0.5*w*dt);
%     n1=we(i,1)/w;n2=we(i,2)/w;n3=we(i,3)/w;
%     qw1=n1*si;qw2=n2*si;qw3=n3*si;qw4=co;
%     om=[qw4  qw3 -qw2 qw1;-qw3  qw4  qw1 qw2;qw2 -qw1  qw4 qw3;-qw1 -qw2 -qw3 qw4];
%     q(i+1,1:4)=(om*q(i,1:4)')';
    omega = [0 -we(i,:);...
        we(i,:)' [0 -we(i,3) we(i,2); we(i,3) 0 -we(i,1); -we(i,2) we(i,1) 0]];
   % omega_prev = [0 -we(i-1,:);...
    %    we(i-1,:)' [0 -we(i-1,3) we(i-1,2); we(i-1,3) 0 -we(i-1,1); -we(i-1,2) we(i-1,1) 0]];
    %matrix_prop = eye(4)+(dt/2)*omega + ((dt*dt)/48)*(omega*omega_prev - omega_prev*omega);
    matrix_prop = eye(4)+(dt/2)*omega;
    qe(i+1,1:4)=(matrix_prop*(qe(i,1:4)'))';
end

propagated_q =  qe(m,1:4)
sim_q = q_n(m, 1:4)



% omega = [0 -w';...
%         w [0 -w(3) w(2); w(3) 0 -w(1); -w(2) w(1) 0]];
% matr1x_prop = eye(4)+(dt/2)*omega;
% q_prop=((matr1x_prop*q_prev_in))';