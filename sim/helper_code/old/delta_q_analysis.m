q_new_local = q_new;
q_new_local(:, 1) = q_new_local(:, 2);
q_new_local(:, 2) = q_new_local(:, 3);
q_new_local(:, 3) = q_new_local(:, 4);
q_new_local(:, 4) = q_new_local(:, 1);
q_old_local = q_old;
q_old_local(:, 1) = q_old_local(:, 2);
q_old_local(:, 2) = q_old_local(:, 3);
q_old_local(:, 3) = q_old_local(:, 4);
q_old_local(:, 4) = q_old_local(:, 1);

conj_q_new = zeros(size(q_new_local));
inv_q_new = zeros(size(q_new_local));
delta_q = zeros(size(q_new_local));
for i =1:size(q_new_local, 1)
    
   conj_q_new(i, :) = [-q_new_local(i, 1:3), q_new_local(i, 4)];
   inv_q_new(i, :) = conj_q_new(i, :)/(norm(conj_q_new(i, :))*norm(conj_q_new(i, :)));
   q_old_local_skew = [0, -q_old_local(i, 3), q_old_local(i, 2); q_old_local(i, 3), 0, -q_old_local(i, 1);-q_old_local(i, 2), q_old_local(i, 1), 0]; 
   q_old_local_cross = [eye(3)*q_old_local(i, 4)-q_old_local_skew, q_old_local(i, 1:3)'; -q_old_local(i, (1:3)), q_old_local(i, 4)];
   delta_q(i, :) = q_old_local_cross*inv_q_new(i, :)';
   mod_delta_q(i) = abs(delta_q(i, 1)) + abs(delta_q(i, 2)) + abs(delta_q(i, 3)) + abs(delta_q(i, 4)) 
end