time = 2200;
timestamp = time/0.025;
%BeaconVECECI = [ADCSModelComplete.BeaconVECECI.Data(timestamp,1), ADCSModelComplete.BeaconVECECI.Data(timestamp,2) ,ADCSModelComplete.BeaconVECECI.Data(timestamp,3)];
%GSECI = [ADCSModelComplete.GSECI.Data(timestamp,1), ADCSModelComplete.GSECI.Data(timestamp,2) ,ADCSModelComplete.GSECI.Data(timestamp,3)];

hold on
%% spehere, GS and Sat position
satPosECEF = [ADCSModelComplete.satPosECEF.Data(timestamp,1), ADCSModelComplete.satPosECEF.Data(timestamp,2) ,ADCSModelComplete.satPosECEF.Data(timestamp,3)];

plot3(ADCSModelComplete.GSECI.Data(1,1), ADCSModelComplete.GSECI.Data(1,2), ADCSModelComplete.GSECI.Data(1,3), 'o')
plot3(satPosECEF(1), satPosECEF(2), satPosECEF(3), 'x')
[X,Y,Z] = sphere;
radius = 6357000;
surf(X*radius,Y*radius,Z*radius,'FaceAlpha',0.5)
a = 10000000;
axis([-a a -a a -a a]); view(3); grid on;
% plot los
seg_x = linspace(ADCSModelComplete.GSECI.Data(1,1), satPosECEF(1), 1000);
seg_y = linspace(ADCSModelComplete.GSECI.Data(1,2), satPosECEF(2), 1000);
seg_z = linspace(ADCSModelComplete.GSECI.Data(1,3), satPosECEF(3), 1000);
plot3(seg_x,seg_y, seg_z);


%% Sat orientation
%pointing_dir_ECI = ADCSModelComplete.pointingDirECI.signals.values(time, :);
%pointing_vec_body = Mission.pointingVecBody;
q = ADCSModelComplete.q.signals.values(time, :);
%pointing_dir_body = quatrotate(q, pointing_dir_ECI);
rotm = quat2rotm(q);
vecdir = [0 0 1]*rotm;
vecECEF = eci2ecef_transform(vecdir',time,Orbit.DCM);


% plot orientation(q)
mult = 2000000;
vecECEF_t = satPosECEF + (vecECEF')*mult;
seg_x = linspace(vecECEF_t(1), satPosECEF(1), 1000);
seg_y = linspace(vecECEF_t(2), satPosECEF(2), 1000);
seg_z = linspace(vecECEF_t(3), satPosECEF(3), 1000);
plot3(seg_x,seg_y, seg_z, '--');
plot3(vecECEF_t(1), vecECEF_t(2), vecECEF_t(3), 'p');

% plot plane dir 1
planeDir1 = eci2ecef_transform(([1 0 0]*rotm)',time,Orbit.DCM);
planeDir1Vec = satPosECEF + (planeDir1')*mult;
seg_x = linspace(planeDir1Vec(1), satPosECEF(1), 1000);
seg_y = linspace(planeDir1Vec(2), satPosECEF(2), 1000);
seg_z = linspace(planeDir1Vec(3), satPosECEF(3), 1000);
plot3(seg_x,seg_y, seg_z, ':');
plot3(planeDir1Vec(1), planeDir1Vec(2), planeDir1Vec(3), '>');

% plot plane dir 2
planeDir2 = eci2ecef_transform(([0 1 0]*rotm)',time,Orbit.DCM);
planeDir2Vec = satPosECEF + (planeDir2')*mult;
seg_x = linspace(planeDir2Vec(1), satPosECEF(1), 1000);
seg_y = linspace(planeDir2Vec(2), satPosECEF(2), 1000);
seg_z = linspace(planeDir2Vec(3), satPosECEF(3), 1000);
plot3(seg_x,seg_y, seg_z, '-.');
plot3(planeDir2Vec(1), planeDir2Vec(2), planeDir2Vec(3), '>');


planeOrg = satPosECEF;
%% plot plane
planeDir1in = planeDir1;
planeDir1 = planeDir1*mult;
planeDir2in = planeDir2;
planeDir2 = planeDir2*mult;
planeSeg = [planeOrg'-planeDir1-planeDir2 ...
  planeOrg'+planeDir1-planeDir2 ...
  planeOrg'+planeDir1+planeDir2 ...
  planeOrg'-planeDir1+planeDir2 ...
  planeOrg'-planeDir1-planeDir2];
patch(planeSeg(1,:),planeSeg(2,:),planeSeg(3,:),[.9 .9 .9]);

dot(planeDir1, vecECEF)
dot(planeDir2, vecECEF)


%% vector to plane
n = (vecECEF')/norm(vecECEF);
ab = ADCSModelComplete.GSECI.Data - satPosECEF;
n_dash = n*(ab*n');
v = ab - n_dash;

v_prior = v;
v = satPosECEF + v;
seg_x = linspace(v(1), satPosECEF(1), 1000);
seg_y = linspace(v(2), satPosECEF(2), 1000);
seg_z = linspace(v(3), satPosECEF(3), 1000);
plot3(seg_x,seg_y, seg_z, '-');
plot3(v(1), v(2), v(3), 'd');



vec = cross(n,[0 0 1]);
ssc = [0 -vec(3) vec(2); vec(3) 0 -vec(1); -vec(2) vec(1) 0];
R = eye(3) + ssc + ssc^2*(1-dot(n,[0 0 1]))/(norm(vec))^2;
new_n = R*n';


new_v = R*v_prior';
plot_vector(new_v, satPosECEF);

% %pointing_dir_ECEF = pointing_dir_ECEF'*mult + satPosECEF;
% plot_vector(pointing_dir_ECEF, satPosECEF);
% plot3(pointing_dir_ECEF(1), pointing_dir_ECEF(2), pointing_dir_ECEF(3), 's');

% % pointing_error_ECEF = rad2deg(acos(dot(vecECEF, (pointing_dir_ECEF))));
% pointing_error_ECI = rad2deg(acos(dot(pointing_dir_body,pointing_vec_body')));
% % pointing_error_sim = ADCSModelComplete.pointingError_total.signals.values(timestamp);


coord = new_v/139800
