
n = [0 0 1];
n = n/norm(n);
origin = [0 0 0];
hold on 
axis([-3 3e+06 -3 3e+06 -3 3e+06]); view(3); grid on;
plot_vector(n, origin);
plot3(n(1), n(2), n(3), 'p')
 
ab =  [1.3980e+05   0   2.2857e+06];
% ab = ab/norm(ab);
plot_vector(ab, origin);
plot3(ab(1), ab(2), ab(3), '>')


n_dash = n*(ab*n');
v = ab - n_dash;
%v = v/norm(v);
plot_vector(v, origin);
plot3(v(1), v(2), v(3), 'o')
dot(v,n)

vec = cross(n,[0 0 1]);
ssc = [0 -vec(3) vec(2); vec(3) 0 -vec(1); -vec(2) vec(1) 0];
R = eye(3) + ssc + ssc^2*(1-dot(n,[0 0 1]))/(norm(vec))^2;

figure;
hold on 
axis([-3 3 -3 3 -3 3]); view(3); grid on;
new_n = R*n';
plot_vector(new_n, origin);
plot3(new_n(1), new_n(2), new_n(3), 'o')

new_v = R*v';
plot_vector(new_v, origin);
plot3(new_v(1), new_v(2), new_v(3), '>')