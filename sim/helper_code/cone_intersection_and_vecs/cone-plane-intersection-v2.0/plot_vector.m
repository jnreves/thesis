function plot_vector(a, b)
seg_x = linspace(a(1), b(1), 1000);
seg_y = linspace(a(2), b(2), 1000);
seg_z = linspace(a(3), b(3), 1000);
plot3(seg_x,seg_y, seg_z);
end