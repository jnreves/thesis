w is created as a rotation around the x-axis then z-axis

q0 (inital quaternion) is created

the quaternion is propagated using the generated w and q0 for all time

the measurements are created based on the quaternion, FOV, threshold (irrelavant) and sigma

the "measurements" are the true and measured body vectors, as well as inertial true vector

the bias and the gyro measurements are simulated on discrete time based on sig v and u (see source
for how it is done)

the initial conditions are simulated (qo = q0, bo = 0, poa (attitude covariance) = 0.1^2 deg, pog
(gyro bias covariance) = 0.2^2 deg/h)

complete initial covariance p  matrix is created 

-- kal attd
process noise covariance is created (Q)

every loop
- The state and covariance are propagated for the next iteration (done at the end)
- the attitude matrix is created
- the inertial measurements are transposed to the body frame and used to create the H(h) matrix 
- the h(z) matrix is created with the body measuremets
- the kalman gain is computed
- the covariance and the error state are updated
- the bias and q-pos are generated


