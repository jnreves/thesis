# Tasks
- ADCS model
- Orbit simulation: find the book from NASA guidelines (athmo drag)
- How to validate results

## 27-09
- [ ] Simulation of the the beacon: how?


## 26-09
- [x] NASA ADCS desgin guidelines - info in separate document: ADCS-Guidelines.pdf
P: flowers, vet, ericeira docs

# State of affairs - mid september
## MEKF

Equations from "Fundamental of Spacecraft Attitude" in NANOSTAR thesis
6-state Kalman filter: 3 for attitude(3 component representation of attitude error), 3 for bias, computed in the error space

Q - process noise covariance matrix - values from Diogo Neves thesis, from datasheet of
gyro
    sigma-v - arw^2
    sigma-u - rrw^2

R - measurement covariance matrix - values from Diogo Neves thesis, calculated with the sensors (ss
and mag) covariance 
delta-t - 0.1

## Simulation changes
### GPS
Simulating SGR-05P, accuracy 10m (95%), frequency 1 Hz used (can't find official or even typical)

### Tensor of inertia 
Tensor of inertia calculated based on uniform mass distribution.
x axis => movement
z axis => pointing

### New SS

### Old SS

### Reaction Wheels Simulation
state: not done

Used as an example the Nano Avionics 4 wheel cluster. The problem when in comes to simulation is
that there aren't enough parameters provided in the datasheet: only maximum torque, RPM and momentum
storage, and dimension.
In my opinion, the best way to model the reaction wheel would be to compare it to a DC motor +
flywheel, but there aren't enough parameters.
Finex sent me some info on flywheels (flywheel-SAE_CEAE_FS_EN_2020i.pdf) and we were able to
extract the moment of Inertia of the wheel through the Torque equation (max Torque = f(max
RPM) and tried to estimate it I by the formula, although the mass of the wheel is unknown
(k=0.5).
This information is clearly not enough, a Transfer function is needed in order to simulate it. No
conclusive information has been found online
It also might be helpful to look on other thesis/papers.

### Aero torque and Gravity Gradient
state: on hold, confused with cd and cp and 1U vs 3U 


how-to-questions
Q: Question to be asked
QA: Question answered
QD: Question deleted


MEETING: wednesday @ 17h00
# 06-08
- [ ] Send email to Rita about integration of gyro
- [ ] Switch for eclipe and gyro integration
- [ ] Change S/C phy
- [ ] Verify sun sensor

# 05-08
- [x] Verify Micius pointing requirements
- [x] Send information to JP and Profs
- [x] Horizon sensor + sun sensor to Rocha
- [ ] Gyro integration techniques
masonMinXSS1CubeSatOnOrbit2017 for ADCS report
# 27-07
- [x] Finish Intro
- [x] Write sensors justification
- [x] Send Planning, INtro and Sensors to the teachers
- [x] Verify Reaction Wheel
- [x] Send stuff to JP 
- [x] Presentation
- [ ] Verify Coarse Pointing Implementation 

# 26-07
- [x] Planning
- [x] Intro/Requirements Text
- [x] Beacon areas

## Planning
Coarse Pointing Implementation: TRIAD + UKF (no horizon sensor), EKF, (...)

Q: JP. Can I assume all faces of the sat will have solar panels? (SS and magnetorquers)  

# 19-07
- [x] Quick Beacon check
- [x] Earth measurement relevance
- [x] Magnetic measurements shortcomings
- [x] Sun vector -> attitude
- [ ] Slides explaining to Rita
- [ ] Horizon Sensor research: positioning, modelling and options

## Notes
Multisensor attitude estimation(...) pg. 261 about estimting attitude based on single-direction
measurements

## Diogo Neves' analyses on CSS + mag 
In eclipe, the EQUEST method was able to determine(propagate) the attitude of the satellite but still had 50
deg error due to the gyro bias
The MEKF and CF was able to sustain much lower propagated errors, but the MEKF is better at bias
estimation, so, eclipse propagation. The MEKF wasn't chosen due to high computational time



# 15-07
- [ ] Beacon research
- [ ] Horizon sensor research
- [x] e-mail to Rita about meetings 

# 14-07
- [x] GPS for attitude determination
- [ ] Ballpark ideia for disturbances

# Distrubances in eclipse 
3-U ECOSat-III:
- TOI = 0.0060, 0.0357, 0.0361
- weight: 3.66
- max area: 3*0.1*0.1 = 0.3 m^2

## Gravity Gradient
# 13-07 
Not too much time today so no real task done
Meeting with Quantum pointing team at 17h in IST

## markleyFundamentalsSpacecraftAttitude2014 - Chp. 4 Sensors and Actuators
### Redundancy: 
Two types of redundant systems are proposed two overcome fails, block redundant and cross strapped

### Star Trackers
#### Overview
CMOS > CCDs since it is more radiation resistant and allows different processinf capabilities
It then goes into describing the focal part and geometric part of the camera

#### Modes of Operation
Tracking mode: The sensor already has an estimate of the stars that are being followed so it searchs
for the stars centroid in nearby areas of the sensor. It is faster
Initial attitude acquisition: pattern matching

#### Field of View, Resolution, Update Rate
Stars around the FOV are usually ignored due to distortions
Error sources in Star Trackers: Shot noise, Dark current, hot pixels
To avoid the errors the acquisition frequency might be lowered and changes in the aperture of the
optics

#### Star Catalog
#### Proper Motion, Parallax, and Aberration
The aberration fenomenon is explained

# 12-07
- [x] Finish Tuesday's meeting presentation
- [x] Read Ineuza's papers
- [x] Wednesday meeting preparation
- [x] Wednestday meeting presentation

## Wednesday meeting: S/S
- [x] Hardware needed
- [x] Finish ST list
- [x] Finish RW list
- [ ] Draft of the overall physical architecture through examples

## Tuesday meeting: 
 possíveis interações entre o sistema de fine pointing do payload e o ADCS da plataforma. Visto que
 precisamos de uma precisão elevada neste processo e queremos tentar ocupar o menos espaço possível
 de modo a podermos construir o satélite em 3U, torna se necessário perceber se é possível utilizar
 recursos do payload quântico, nomeadamente a quad cell, para apontar o satélite.
- [x] ADCS architecture
- [x] Estimation precision ballpark
- [x] Control precision ballpark

# 09-07
- [x] Send email to Rocha
- [x] Send email to P. Rita
- [x] Organise meetings

# 08-07
- [x] Model star tracker in matlab
- [ ] Model reaction wheel in matlab

# 05-07 + 06-07
- [x] Send documents to JP
- [x] Ask if meeting
- [x] Spacecraft dynamics: how does it impact 1U to 2/3U?
- [x] Reaction wheel model
- [x] Write doubt to Rita 
- [x] Read document Rocha sent
- [x] Star Sensors list
- [x] Rocha's doubts

Future work: Reaction Wheel Disturbace Modeling

Q: Ro. How to obtain the Tensor of Inertia of the future Satellite?
Q: Ro. + Ri. Will the Satellite have elements outside the U space (e.g. solar panels)? If so,
should the rigid body model be used or another one?

# 04-07
A lot of research on the Star Tracker Modelling, document on drive, ready to send to Rita
Q: Ri. How can I model the noise in quaternions?

# 03-07
Trying to find some info about STR A) positioning and how many and all that B) Modeling

Q: Ro. How many Us for the satellite?

## gauravStarSensorMounting2020
Paper on how to position star sensors, since they apparentely are very sensitive the Sun and the
Earth's Albedo. May be good but I think to specific

## baeSatelliteAttitudeDetermination2010
They descibre the STR measurement as q-output = delta-q X q-true and delta-q as and independent
zero-mean Gaussian white-noise process

## maHighAccuracyLowCostAttitude2020
They have the reasoning behing choosing a position and a discrete mathematical model.  

# 02-07
* Ines at 18h in Gulbenkian (leave at 17h)
* Dinner with IST bros (20h - ish)

Tasks:
- [x] Review meeting

# 01-07
Today: meeting with P.R.
Taks:
- [x] Review IST thesis about ADCS
- [x] Prep meeting with P.R. 

## Meeting: 
### Questions:
QA: R. I have decided on the type of sensors I will use: gyro (FOGs vs MEMS
still up for discussion), Star tracker (1 or 2, depends on FOV) and a magnetometer. Can I just say
"through the literature, this seems like the reasonable sensor choice for the attitude
requirements"?
R: All good. R. said she was going to try to help to find some literature on MEMS vs FOG

QA: R. I have looked into the thesis and they seems to go into comparing different types of
algorithms and not really more proven/studied arch. Does this make sense?
QA: R. Should I focus on proving that the single-frame, no dynamics methods 
or just try to make the best architecture possible and maybe comparing more
similar filters. So, cut out a lot of things from the start and focus on more "advanced"
architectures. 
R: It is a good ideia to choose less methods and characterize them fully, with different noise and
initial conditions, different architectures and so on

QD: R. Cascaded Complementary Filter - does it make sense to worry that much about the bias in the
gyroscope? Could it be coupled with the Mahony Complementary filter? (I think so)

QA: R. Magnetometer bias: should it be estimated? Most of the papers I have found are either old
(tech advancements since then) or use estimation highly dependent on them (sun + mag)
R: Search it myself but maybe not now

QA: R. Should I worry about MPC? 
R: Can be an afterthough and the paper I found just seems like a double integrator

QA: R. Fault detection?
R: Investigate it myself, interesting but not urgent

Q: R. How to know how non linear a system is? Should I use regular EKF, UKF or CKF, for e. In  
maHighAccuracyLowCostAttitude2020 it is said that the S/C dynamics are not *that* non linear and
that is why they don't apply that well to reality in terms of performance increase and might be too
complicated to simple onboard processors.

### Other topics
I should focus on modelling of the sensors (star tracker)
Reaction whells need to eventually be modelled and could be used in the control (use dynamics
instead of just kynematics
R. expressed that she was worried about the modelling of the satellite

# 29-06

## sokenAttitudeEstimationMagnetometer2020 - TRIAD+UKF
Pre-processing of the attitude estimates through TRIAD that are fed to UKF.
TRIAD vs rest: no explanation, could be tried
UKF vs rest: better coping with poor initialization. They claim to have very poor attitude
estimation in the beginning due to magnetometer error terms
I'm not sure that the magnetometer bias is as relevant as they make it to be - going to check. I
haven't found a recent study where bias estimation and non bias estimation are compared but seems
like general practice to do it. In this paper it is not compared, for example. 
It has a very good architecture to take in mind where the full UKF for bias prediction is only run
when needed in order to reduce computational load.


## hajiyevReviewGyrolessAttitude2017 - Gyroless attitude determination
It may be worth looking into. Good paper with a few different alternatives explored and explained,
but without the deduction of equations - in bib

## baeSatelliteAttitudeDetermination2010 - FEKT
Uses a Federated KF, composed by local filter per sensor, a merging algorithm and a master filter,
to mantain roubstness when a sensor is malfunctioning

## Possible architectures notes
For the architecture let's assume the following sensors: gyro, 1/2 star tracker and magnetometer
- Something like the implementation of maHighAccuracyLowCostAttitude2020 could be tested, maybe
  with the integration of the magnetometer readings. It uses MEKF and quaternion averaging to merge
  two different sensors. Implementations for 2 star tracker may be used
- Atleast one gyroless system could be tried. Either changing one particular architecture to use the
  difference between to star tracker to measure displacement or build one from the ground up. Etiher
  way, hajiyevReviewGyrolessAttitude2017 
- MPC?
- Magnetometer online calibration could be used in spite of the method chosen, maybe something like
  sokenAttitudeEstimationMagnetometer2020 where a KF that includes bias fitting if there is a sensor
  fault detected
- Sensor fault detection? Maybe I should investigate how to do it. Or just define two controller
  (one for fault, one for normal use) and see the difference. Fault in mag - ref above. Fault in
  star tracker - baeSatelliteAttitudeDetermination2010
- Complementary Filter as proposed in narkhedeCascadedComplementaryFilter2021 ? It is designed to be
  in euler angles but maybe can be adapted to quaternions


Rita scheduled meeting for thursday
# 28-06
Looking for what Star-sensors typically output in order to verify if the measurement model should be
studied or assumed to be a vector. Some Star Trackers have an output that is the rotation in regards
to the inertial body frame. 

Sent an email to Rita

## liSatelliteAttitudeEstimation2006 - UKF Star-Sensor
This algorithm introduces an error in the angular velocity, the state being the quaternion and
angular velocity estimates. It is mostly used in a gyroless system so I am going to skip it for now.

## tisseraOnorbitGyroscopeBias2021 - EKF for bias estimation in a gyro
Uses an EKF for attitude estimation  (provides bibliography for the equations) and uses Model
Predictive Control for the bias estimate

## maHighAccuracyLowCostAttitude2020 - High acc - low cost
MEKT tested in different architectures and with one and two star sensors. Even if this filter isn't
used the architecture might be re-used? 
Good references, going to explore them

## guoHIGHPRECISIONATTITUDEESTIMATION2017 - CF + UKF
The output of a CF is used as the measurement of the UKF. It isn't explained very well though

## yuanCubatureKalmanFilter - CKF
Similar to EKF and to UKF in how it is applied but used the spherical radial Cubature criteria,
making it the closest aproximation to Bayesian Filtering. As a comparision to EKF and UKF

## okenAttitudeEstimationMagnetometer2020 - TRIAD + UKF
Magnetometer calibration - which isn't very interesting I think - but has a novel architecure



# 24-06
Literature review table

Future work: fault detection
# 23-06
- Research of Algotrithms

The main Complementary filters utilizes transfer functions in the frequency domanin to limit
influence of certain sensors. The filtered signals are then feed through to the estimator, where
they might suffer addional TF.

The Kalman filter uses the system's dynamics, defines an error between the estimate and the measured
value and calculates a gain to compensate this descrepansy.

Cascadade Complementary Filter has two steps: bias correction and CF. It uses a KF to estimate the
bias, using a PD with the input of the accelerometer as measure and the last CF's output as
estimate. Then, the unbiased gyro measure and the acceloremeter are combined in a complementary
filter.

Complementary Kalman filter?



# 22-06
- Fixed bibtex referencing. When changing bib must run bibtex Thesis; pdflatex Thesis before
  continuing make
- Added vim commands F5 and F6 to compile and view pdfs
- Reviewed sensors text and added references
- Sent email with planning
# 21-06
- Reviewed planning and thesis outline drafts before sending it to Professors
