# Title:
Attitude Determination and Control System (ADCS) for QSat Project

# Keywords:
ADCS; Quantum Communication; Cubesat 

# Abstract:
In the information age, it is crucial to create and maintain secure communications. One way of
significantly improving our current security standards is to use Quantum Key Distribution (QKD)
methods, which allow two parties to exchange encryption keys with absolute confidence that any
eavesdropping by a third party will be detected [1]. Since QKD is based on a photonic
communication link, satellite-based QKD is a promising approach to establishing a global quantum
network.  Implementing this technology in a CubeSat is a further step in the deployment of QKD
technology, allowing for the low-cost deployment of large constellations.
This work purposes a conceptual design of a Cubesat for a downlink quantum communication,
QuantumSat-PT, highlighting the study of the  Attitude Determination and Control System (ADCS). This
subsystem is a vital component of every satellite since it is responsible for estimating accurate
attitude information as well as pointing the satellite and its payload to the correct orientation,
together with performing all other necessary maneuvers. In this particular application, due to the
Size, Weight, and Power consumption (SWaP) limitations inherent to designing a 3U CubeSat [2], the
quantum payload components need to be miniaturized reviling difficult challenges especially
regarding the study of the onboard alignment system [3]. These rigorous constraints concerning the
pointing accuracy can be grouped into coarse body pointing and fine alignment.

Common architectures for high-accurate ADCSs usually include star-trackers, which provide arc-second
accuracy [4]. These sensors are usually expensive and require redundancy, which hinders their
usefulness in this application due to the volume restrictions. This paper studies the incorporation
of the ground tracking beacon as part of the coarse ADCS, as well as the hardware that should be
coupled with it to ensure the target accuracy is met.
The coarse pointing is divided into two modes that are
characterized by which attitude reference is used. Firstly, the beacon is
detected with positioning and inertial sensors; the attitude error generated
by the optical processing of the beacon is then used to align the satellite
with the laser beacon.
A comparison of different sensor suites is provided as well as different methods for attitude
estimation. The control approach is also designed to meet not only the pointing requirements but
also additional maneuvers such as detumbling (spinning rate reduction).
# References:
 - [1]: Renner, R. (2008). Security of quantum key distribution. International Journal of Quantum
   Information, 6(01), 1-127.
 - [2] "CubeSats Overview", NASA, 2021. Available:
   https://www.nasa.gov/mission_pages/cubesats/overview. 
 -[3]S. Neumann et al., "Q3Sat: quantum communications uplink to a 3U CubeSat—feasibility &
   design", EPJ Quantum Technology, vol. 5, no. 1, 2018. doi: 10.1140/epjqt/s40507-018-0068-1.
 - [4] C. C. Liebe, "Accuracy performance of star trackers - a tutorial," in IEEE Transactions on
   Aerospace and Electronic Systems, vol. 38, no. 2, pp. 587-599, April 2002, doi:
   10.1109/TAES.2002.1008988. 

# Figure:


![alt text for screen readers](adcs-function-symposium.drawio.png "Text to show on mouseover")
